//
//  VideoButton.swift
//
//  Created by Paul Manson on 3/04/18.
//  Copyright © 2018 com.equinox. All rights reserved. 
//

import UIKit
import PalaceSoftwareCore

///
/// A VideoButton informs its owner that the button has been pressed through the VideoButtonDelegate methods.
///
public protocol VideoButtonDelegate {
    
    func videoButtonWasTapped()
    
}

///
/// Implements a "Record Video" button. When recording, the button pulses red.
///
@IBDesignable public class VideoButton: UIButton {

    @IBOutlet      var contentView         : UIView!
    @IBOutlet weak var button              : UIButton!
    @IBOutlet weak var recordVideoImage    : UIImageView!
    @IBOutlet weak var recordingVideoImage : UIImageView!
    
    
    ///
    /// Assignment of the **delegate** property is required in order to detect Taps on the VideoButton
    ///
    public var delegate : VideoButtonDelegate? = nil

    ///
    /// Disable or enable the NextButton using this property.
    ///
    public override var isEnabled: Bool {
        didSet {
            self.applyIsEnabled()
        }
    }
    
    ///
    /// The button's default state is !isRecording. When isRecording is set to true, the overlying **recordingVideoImage** will begin to pulse.
    ///
    public var isRecording: Bool = false {
        didSet {
            Async.onMainThread {
                if self.isRecording {
                    self.startRecording()
                } else {
                    self.stopRecording()
                }
            }
        }
    }
    
    internal var animator : UIViewPropertyAnimator? = nil
    
    override init(frame: CGRect) {                  //  For use in code
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {       //  For use in IB
        super.init(coder: aDecoder)
        commonInit()
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
         self.delegate?.videoButtonWasTapped()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("VideoButton", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame            = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        //
        //  Apply the configurable properties to the UI elements.
        //
        self.applyIsEnabled()
    }
    
    private func applyIsEnabled() {
        self.button.isEnabled        = self.isEnabled
        self.recordVideoImage?.alpha = self.isEnabled ? 1.0 : 0.6
    }

    internal func stopRecording() {
        if self.animator != nil {
            self.animator?.stopAnimation(true)
            self.animator = nil
            
            self.recordingVideoImage.alpha = 0.0
        }
    }
    
    internal func startRecording() {
        stopRecording()
        
        //
        //  Make the recordingVideoImage visible and begin to pulse it ...
        //
        self.recordingVideoImage.alpha = 0.0
        
        animateUp()
    }
    
    private func animateUp() {
        self.animator?.stopAnimation(true)
        self.animator = UIViewPropertyAnimator(duration: 1.5, curve: .easeOut)
        self.animator?.addAnimations {
            self.recordingVideoImage.alpha = 1.0
        }
        self.animator?.addCompletion({ (_) in
            self.animateDown()
        })
        
        self.animator?.startAnimation()
    }

    private func animateDown() {
        self.animator?.stopAnimation(true)
        self.animator = UIViewPropertyAnimator(duration: 1.5, curve: .easeIn)
        self.animator?.addAnimations {
            self.recordingVideoImage.alpha = 0.2
        }
        self.animator?.addCompletion({ (_) in
            self.animateUp()
        })
        
        self.animator?.startAnimation()
    }
}
