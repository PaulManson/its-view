//
//  App.swift
//  ITS View
//
//  Created by Paul Manson on 6/05/15.
//  Copyright (c) 2015 e.quinox KG. All rights reserved.
//
//  The App class provides some global settings for the App as a whole. 
//
//
import PalaceSoftwareCore

public class App {
    //
    //  The App can be built either for "Production" (end-user) operation, or for "Testing”. Various classes behave differently, depending on this setting.
    //
    public enum AppExecutionParadigm {
        case testing, production
    }

    static public let appExecutionParadigm : AppExecutionParadigm = .production // .testing    //   .production //
    
    static var developerNames : [String] = [
        "Erik Schuetz", "Paul Manson", "Matthias Leonhardt"
    ]
    
    static var translatorNames : [String] = [
        "Anke Xylander", "Jan Sawicki", "Niels Rasmussen", "Paolo di Giusto",  "Rajak from Geokorp Technologies", "Tanya Slavova"
    ]
    
    static let applicationName        = "ITS View"
    static let organizationName       = "ITS Geo Solutions GmbH"
    static let websiteForAccountLogin = "www.its-geosolutions.com"
    static let supportEmailAddress    = "info@its-geo.de"
    
    static var legalLinkForAppleMaps : String = "https://gspe21-ssl.ls.apple.com/html/attribution-98.html"
    
    static let isRegisteredTrademark = false
    
    static var applicationLegalName : String {
        get {
            return isRegisteredTrademark ? "\(applicationName)®" : "\(applicationName)™"
        }
    }
    
    static var localizedAboutTitle : String {
        get {
            return LM.localizedString("menu_about", comment: "Sidebar menu entry for the About screen.").withApplicationNameInjected
        }
    }
    
    class var copyrightMessage : String {
        get {
            var message = LM.localizedString("text_copyright_statement", comment: "Copyright statement for the App. The current year replaces @1 in the localized string, while the (organization) name of the copyright holder replaces @2.")
            message.replace("@1", withString: "\(Date.now.year)")
            message.replace("@2", withString: organizationName)
            return message
        }
    }
    
    class var applicationVersionAndBuildString : String {
        get {
            return "\(applicationVersionString) (\(applicationBuildString))"
        }
    }
    
    class var applicationBuildString : String {
        get {
            if let buildString = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String {
                return buildString
            } else {
                return "<Build # Unknown>"
            }
        }
    }
    
    class var applicationVersionString : String {
        get {
            if let versionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
                return versionString
            } else {
                return "<Version # Unknown>"
            }
        }
    }
    
    class var isErikOrPaul : Bool {
        get {
            return isErik || isPaul
        }
    }
    
    class var isErik : Bool {
        get {
            guard let emailAddress = Settings.accountEmailAddress?.description else {
                return false
            }
            
            switch emailAddress.lowercased() {
            case "erikschuetz@icloud.com":              return true
            default:                                    return false
            }
        }
    }
    
    class var isPaul : Bool {
        get {
            guard let emailAddress = Settings.accountEmailAddress?.description else {
                return false
            }
            
            switch emailAddress.lowercased() {
            case "paul.robin.manson@icloud.com":        return true
            case "paul.robin.manson@gmail.com":         return true
            default:                                    return false
            }
        }
    }
 
    class var isEnergyAGTester : Bool {
        get {
            guard let emailAddress = Settings.accountEmailAddress?.description else {
                return false
            }
            
            switch emailAddress.lowercased() {
            case "markus.aitzetmueller@netzgmbh.at",
                 "michael.mittendorfer@netzooe.at",
                 "andreas.felbauer@netzooe.at",
                 "thomas.hofer@netzgmbh.at",
                 "daniel.weitmann@energieag.at",
                 "walter.obermair@liwest.at":           return true
            default:                                    return false
            }
            
        }
    }
    
    class var signatureColor : Color {
        get {
            return UIColor(red:   70.0/255.0, green: 107.0/255.0, blue: 176.0/255.0, alpha: 1.0)     //  IBM Blue
        }
    }

    ///
    /// We want the ARKitVersionChecker to encourage the user to update their iOS device if the version of iOS on which we are running isn't at least the
    /// following .desiredMinimumiOSVersionForARKit.
    ///
    class var desiredMinimumiOSVersionForARKit : String {
        get {
            return "12.0"
        }
    }
}

extension String {
    
    //
    //  Given a string, replace all occurrences of '@1' with the application's name and return the resulting string.
    //
    var withApplicationNameInjected : String {
        get {
            return inject(App.applicationName, inPlaceOf: "@1")
        }
    }
    
    //
    //  Given a string, replace all occurrences of '@1' with the application's legal name and return the resulting string.
    //
    var withApplicationLegalNameInjected : String {
        get {
            return inject(App.applicationLegalName, inPlaceOf: "@1")
        }
    }
    
    //
    //  Given a string, replace all occurrences of the specified target string with the injectedString and return the resulting string.
    //
    func inject(_ injectedString: String, inPlaceOf substring: String) -> String {
        var string = self
        
        while string.contains(substring) {
            string.replace(substring, withString: injectedString)
        }
        
        return string
    }
    
}

#if !swift(>=4.1)
extension Array {

    func compactMap<T>(_ transform: (Element) throws -> T?) rethrows -> [T] {
        return try flatMap(transform)
    }

}
#endif

//
//  SketchMapScaleFactor declared here to avoid the need for lots of #if !CYGNUS exceptions. This is unused in ITS View, but exists in (shared) Settings.
//

///
/// Our SketchMapView allows for "super-zooming" through the specification of a "scale factor". When this scaleFactor is 1, the SketchMapView behaves identically to any other
/// MKMapView with respect to scaling and the number of accessible zoom levels. But when a larger scaleFactor is applied, the content added to the SketchMapView is scaled up
/// by the appropriate amount, while any computed dimensions are scaled down by the same amount. The net effect is that the SketchMapView appears to allow data to be zoomed
/// closer than before.
///
/// IMPORTANT: THIS IMPLEMENTATION RELIES ON THE FACT THAT WE ARE NOT DISPLAYING STANDARD BACKGROUND MAPS OR OVERLAY MAP TILES (BOTH OF WHICH WOULD BREAK). IT IS ALSO
/// IMPORTANT THAT ANY EXTERNAL MATH DONE BY THE SketchMapView CLIENT TAKES INTO ACCOUNT THE scaleFactor PROPERTY.
///
enum SketchMapScaleFactor : Int {
    
    case lifeSized =  1
    case scale2X   =  2
    case scale4X   =  4
    case scale8X   =  8
    case scale16X  = 16
    
    var scaleFactor : Double {
        get {
            return Double(self.rawValue)
        }
    }
    
    static var all : [SketchMapScaleFactor] = [.lifeSized, .scale2X, .scale4X, .scale8X, .scale16X]
    static var allDescriptions : [String] = all.map { (sketchMapScaleFactor) -> String in
        return sketchMapScaleFactor.description
    }
    
    var description : String {
        switch self {
        case .lifeSized:    return "Life Sized (1:1)"
        case .scale2X:      return "Twice Life Sized (2:1)"
        case .scale4X:      return "4x Life Sized (4:1)"
        case .scale8X:      return "8x Life Sized (8:1)"
        case .scale16X:     return "16x Life Sized (16:1)"
        }
    }
    
    init?(description: String) {
        for possibleScaleFactor in SketchMapScaleFactor.all {
            if possibleScaleFactor.description.lowercased() == description.lowercased() {
                self = possibleScaleFactor
                return
            }
        }
        
        return nil
    }
}


