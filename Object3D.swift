//
//  Object3D.swift
//  ITS View
//
//  Created by Paul Manson on 23/05/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import Foundation
import PalaceSoftwareSpatial
import CoreLocation

///
/// An **Object3D** represents the template for a 3D Object. In other words, each concrete **Object3D** is a **type** of 3D Object, as opposed to an instance of such an object
/// in the Augmented Reality view.
///
/// The Object3DManager therefore manages the gallery of available 3D Object templates, and these are stored as P2N "Places", synchronized to Perseids, etc.
/// The ARObjectManager manages instances of these **Object3D** objects in Augmented Reality. So while there can only be one copy of an **Object3D** template, it might have
/// several instances of that object type/template in a given Augmented Reality session.
///
/// When "designing" a 3D Object, using the 3D Object Manager utility, each 3D Object belongs to a Customer and by convention the User Tags for the 3D Object include a
/// "CustomerID=XXXXXXXXXXX" tag, where XXXXXXXXXX is the UUID of the Customer record to which the 3D Object belongs.
///
/// Each 3D Object must have 3 associated Media files:
/// 1. A "Parameters" file which is a JSON encoding of an Object3DParameters object
/// 2. A Thumbnail file which is an image in PNG or JPG format. Ideally square 300x300 pixels with a transparent background.
/// 3. A Model file which is a Zipped 3D Model in Apple's .SCN format.
///

class Object3D : Equatable, Hashable {
    
    static let kPinNameFor3DObjects = "Pin_SportOthers.png"
    
    var objectID       : SwiftUUID
    var category       : String
    var customerID     : SwiftUUID?     //  Relevant only to the 3D Object Manager App. Elsewhere, this will be nil.
    var name           : String
    var description    : String?
    var thumbnail      : PlaceMedia?
    var model          : PlaceMedia?
    
    //
    //  The 3D Object's parameters can only be updated via the updateParameters() method.
    //  This ensures that any existing Parameters Media File is deleted and a new one added to the Place associated with this 3D Object.
    //
    private(set) var parameters : Object3DParameters
    
    ///
    /// Construct a 3D Object. This method does NOT create the associated Media files (which must be managed by the caller).
    ///
    init(objectID: SwiftUUID, name: String, category: String, customerID: SwiftUUID? = nil, description: String?, thumbnail: PlaceMedia?, model: PlaceMedia?, parameters: Object3DParameters = Object3DParameters.kDefaultParameters) {
        self.objectID       = objectID
        self.name           = name
        self.category       = category
        self.customerID     = customerID                    //  Non-nil if created using 3D Object Manager.
        self.description    = description
        self.thumbnail      = thumbnail
        self.model          = model
        self.parameters     = parameters
    }
    
    static func ==(lhs: Object3D, rhs: Object3D) -> Bool {
        return lhs.objectID == rhs.objectID
    }
    
    public var hashValue: Int {
        get {
            return self.objectID.hashValue
        }
    }
    
    ///
    /// - Returns: The existing Place corresponding to this 3D Object. CALL ONLY FOR OBJECTS WHICH HAVE BEEN PREVIOUSLY SAVED TO THE DATABASE.
    ///
    var place : Place? {
        get {
            return Places2NoteDB.place(withGUID: self.objectID)
        }
    }
    
    ///
    /// - Returns: true if the model's Y axis points upwards (as is standard for SceneKit/ARKit models), or false otherwise (as is usually the case for imported models)
    ///
    var isModelYAxisUp : Bool {
        get {
            return self.parameters.isModelYAxisUp
        }
    }

    ///
    /// - Returns: the scale of the Model
    ///
    var modelScale : Object3DParameters.Object3DScale {
        get {
            return self.parameters.modelScale
        }
    }
    
    ///
    /// Update the parameters associated with this 3D Object. If other Parameters media files are still associated with the Place representing this Object, delete them.
    ///
    func updateParameters(parametersMediaFile: PlaceMedia) {
        if let updatedParameters = Object3DParameters(parametersMediaFile) {
            self.parameters = updatedParameters
        } else {
            Log.error{ "Downloaded and received an Object3DParametersMediaFile for 3D Object with Name = \(self.name) but we can't extract an encoded 3D Object Parameters object from the Media file!" }
            return
        }

        //
        //  There should be precisely one parametersMedia file for this 3D Object. But if any previous parametersMedia file remains attached to the Place representing
        //  this 3D Object, we must remove it now.
        //
        for media in self.place?.parametersMedia ?? [] {
            if media.mediaID != parametersMediaFile.mediaID {
                Places2NoteDB.deleteMedia(media)
            }
        }
    }
    
    /***
    ///
    /// - Returns: The Object3DParametersMedia object which currently stores the Parameters for this object.
    ///
    var parametersMedia : PlaceMedia? {
        get {
            guard let placeForSelf = self.place else {
                Log.error{ "Unable to return parametersMedia for this 3D Object because the Place representing this object is nil!" }
                return nil
            }
 
            //
            //  Issue an error if there's more or less than one parametersMedia file associated with this 3D Object.
            //
            let mediaFiles = placeForSelf.parametersMedia
            
            if mediaFiles.count == 1 {
                return mediaFiles.first
            } else {
                Log.error{ "There should be precisely 1 Object3DParametersMedia file associated with the 3D Object called \(self.name), but there is \(mediaFiles.count)!" }
                return nil
            }
        }
    }
    ***/
}

extension Place {
    
    ///
    /// - Returns: A 3D Object Template equivalent to a given (pre-existing) Place.
    ///
    /// Note: This does NOT download the thumbnail or model for this object. That is the responsibility of the Application.
    ///       This does, however, open the Parameters media file if it's stored locally; if not, it assigns the default Parameters and requests that the
    ///       Parameters media file be downloaded.
    ///
    var object3D : Object3D? {
        get {
            guard self.isObject3D else {
                return nil
            }
            
            let thumbnail = self.photoMedia.first
            let model     = self.modelMedia.first

            //
            //  While we will tolerate a (temporarily) nil thumbnail or model, the parameters must ALWAYS be present otherwise this Place will be rejected (and auto-
            //  deleted)
            //
            guard let parametersMediaFile = self.parametersMedia.first else {
                //
                //  As we transition from the original userTags[]-based implementation, we want to discard all old 3D Objects to avoid polluting the database with a mix
                //  of old and new objects.
                //
                Log.debug{ "*** FOUND A 3D OBJECT Place WITH NO PARAMETERS JSON FILE ATTACHMENT. TREATING THIS AS AN OBSOLETE PLACE AND DISCARDING IT! PLACE NAME = \(self.name), CATEGORY = \(self.categoryName)" }
                
                //
                //  We can't physically delete any "3D Object" Place that doesn't actually have a "Parameters" attachment, as we risk deleting a Place that's already
                //  in the process of being deleted. So we silently discard it instead ...
                //  Places2NoteDB.deletePlace(self)
                //
                
                return nil
            }
            
            //
            //  If the parametersMedia file is stored locally on this device, we can open it and extract the parameters immediately. If not, we request it, but for now
            //  we go with the default parameters. Only when the actual parametersMedia file is downloaded will we be able to update the Object with its actual
            //  parameters.
            //
            var parameters : Object3DParameters? = nil
            if parametersMediaFile.isLocalFileOnThisDevice {
                parameters = Object3DParameters(parametersMediaFile)
            }
            
            if parameters == nil {
                //
                //  We have come across a 3D Object with a Parameters Media file that isn't held locally on this device. For now, we'll go with the default Parameters, but
                //  really we should never use this Object until the Parameters Media file has been downloaded and the parameters in memory updated. Request that now.
                //
                Object3DParametersMedia.download(parametersMediaFile, for: self.GUID)
            }
            
            let customerID = self.customerIDFromUserTags()
            
            return Object3D(objectID: self.GUID, name: self.name, category: self.categoryName, customerID: customerID, description: self.comments, thumbnail: thumbnail, model: model, parameters: parameters ?? Object3DParameters.kDefaultParameters)
        }
    }
    
    ///
    /// - Returns: true if the Place is a "3D Object Template".
    ///
    var isObject3D : Bool {
        get {
            return (self.categoryGroup == Places2NoteDB.kTemplateCategoryGroupName)
        }
    }
    
    ///
    /// Create a Place based on a locally-created 3D Object WHICH HAS NOT YET BEEN SAVED TO THE DATABASE.
    ///
    /// Note: This method does NOT associate the Object's thumbnail and model with the Place. You should make a subsequent call to add these as PlaceMedia objects
    ///       for this Place. This method also does NOT associate the Parameters media file with the Place; this is also a responsibility of the caller.
    ///
    convenience init(object: Object3D) {
        let fakeLocation = Location(coordinate: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
        
        self.init(name: object.name, location: fakeLocation, categoryGroup: Places2NoteDB.kTemplateCategoryGroupName, categoryName: object.category, pin: Object3D.kPinNameFor3DObjects, placeID: object.objectID, userID: Places2NoteDB.currentUser.userID)
        
        self.comments = object.description ?? ""
        
        if let customerID = object.customerID {
            self.userTags.append(self.userTag(for: customerID))
        }
    }
    
    ///
    /// - Returns: The potential thumbnail image(s) for a given Place.
    ///
    var photoMedia : [PlaceMedia] {
        get {
            let mediaForPlace = Places2NoteDB.mediaForPlace(self)
            
            if mediaForPlace.count == 0 {
                Log.debug{ "Attempt to retrieve mediaForPlace for place named \(self.name) but there are NO media files associated with this place." }
            }
            
            return mediaForPlace.filter({ (placeMedia) -> Bool in
                if let mediaType = placeMedia.mediaType, !placeMedia.deleted {
                    return mediaType == .photo
                } else {
                    return false
                }
            })
        }
    }
    
    ///
    /// - Returns: The potential 3D model(s) for a given Place. These are attachments with the ZIP extension.
    ///
    var modelMedia : [PlaceMedia] {
        get {
            let mediaForPlace = Places2NoteDB.mediaForPlace(self)
            
            if mediaForPlace.count == 0 {
                Log.debug{ "Attempt to retrieve mediaForPlace for place named \(self.name) but there are NO media files associated with this place." }
            }

            return mediaForPlace.filter({ (placeMedia) -> Bool in
                if placeMedia.deleted || placeMedia.mediaType != .custom {
                    return false
                } else {
                    return placeMedia.fileName.uppercased().hasSuffix(".\(ZIPMedia.kFileExtension)")
                }
            })
        }
    }
    
    ///
    /// - Returns: All potential (non-deleted) Parameter media files associated a given Place. These have a JSON extension. There should only be one of these "current"
    ///            at any point in time; all others need to be deleted to ensure data integrity.
    ///
    var parametersMedia : [PlaceMedia] {
        get {
            let mediaForPlace = Places2NoteDB.mediaForPlace(self)
            
            if mediaForPlace.count == 0 {
                Log.debug{ "Attempt to retrieve mediaForPlace for place named \(self.name) but there are NO media files associated with this place." }
            }
            
            let uppercaseFileExtension = Object3DParametersMedia.shared.fileExtension.uppercased()
            
            return mediaForPlace.filter({ (placeMedia) -> Bool in
                if placeMedia.deleted || placeMedia.mediaType != .custom {
                    return false
                } else {
                    return placeMedia.fileName.uppercased().hasSuffix(".\(uppercaseFileExtension)")
                }
            })
        }
    }
    
    private func userTag(for customerID: SwiftUUID) -> String {
        return "\(kCustomerIDUserTagPrefix)\(customerID.UUIDString)"
    }
    
    private func customerIDFromUserTags() -> SwiftUUID? {
        for tag in self.userTags {
            if tag.hasPrefix(kCustomerIDUserTagPrefix) {
                var customerIDString = tag
                customerIDString.replace(kCustomerIDUserTagPrefix, withString: "")
                
                if let customerID = SwiftUUID(string: customerIDString) {
                    return customerID
                }
            }
        }
        
        return nil
    }
    
    private var kCustomerIDUserTagPrefix : String {
        get {
            return "CustomerID="
        }
    }

}
