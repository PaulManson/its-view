//
//  AppDelegate.swift
//  Places2Note
//
//  Created by Paul Manson on 6/05/15.
//  Copyright (c) 2015 e.quinox KG. All rights reserved.
//

import UIKit
import StoreKit
import HealthKit

import PalaceSoftwareCore


//
//  Global Typealias for iOS
//
public typealias Font             = UIFont
public typealias Image            = UIImage
public typealias Color            = UIColor
public typealias BezierPath       = UIBezierPath
public typealias Storyboard       = UIStoryboard
public typealias StoryboardSegue  = UIStoryboardSegue
public typealias Application      = UIApplication
public typealias Window           = UIWindow
public typealias ViewController   = UIViewController
public typealias View             = UIView
public typealias ImageView        = UIImageView

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        Log.trace{ "AppDelegate.application:didFinishLaunchingWithOptions() called." }

        //
        //  Per Apple Guidelines, add an observer for iTunes App Store transactions at the earliest possible opportunity in AppDelegate:didFinishLoadingWithOptions:
        //  to ensure that any store transaction events are handled at startup.
        //
        SKPaymentQueue.default().add(ProductManager.sharedProductManager)
        
        //
        //  It is important that the App does not allow the /Tmp folder to become clogged with files. In general, the App cleans up after itself, but particularly when debugging
        //  there is a tendency for temporary files (e.g. media files) to be left lurking after the App has been interrupted. We clean up those files here, SYNCHRONOUSLY. This
        //  means the App MAY take a little longer to start up, but this will be rare.
        //
        //  While the FileManager will automatically clean the /Tmp folder on first access, we don't want to rely on that approach, because the /Tmp folder is now used by iOS as the
        //  location for the "InBox" containing URLs shared by other Apps. Hence we explicitly delete the contents of /Tmp at App startup.
        //
        _ = FileManager.emptyTmpFolder()
        
        //
        //  Ask the App's NavigationBar to respect our signature tint color
        //
        UINavigationBar.appearance().tintColor = App.signatureColor
        
        //
        //  Register all CustomMedia types used in this Application. This has the side-effect of adding these SpatialMedia subclasses to the SpatialMediaManager's list of
        //  managed SpatialMedia types.
        //
        CustomMediaManager.registerCustomMediaType(GPXMedia.shared.customMediaType)
        CustomMediaManager.registerCustomMediaType(OSMMedia.shared.customMediaType)
        CustomMediaManager.registerCustomMediaType(ZIPMedia.shared.customMediaType)
        
        //
        //  Also register the CustomMedia type used to hold the Parameters for a 3D Object Place.
        //
        CustomMediaManager.registerCustomMediaType(Object3DParametersMedia.shared.customMediaType)
        
        //
        //  Set up a reachability object to query for network status and to receive notifications whenever the network status changes ...
        //
        AppReachability.initialize()
        
        //
        //  Initialize global objects
        //

        // Settings.initializeSettings()        //  No longer necessary as the Settings class auto-initializes on first reference.
        // Places2NoteDB.initializeDatabase()   //  No longer necessary as the Places2NoteDB class auto-initializes on first reference.

        Places2NoteDefinitions.initializeDefinitions()
        Cloud.initializeCloud()
        
        //
        //  This app always wants to be informed if the device orientation changes
        //
        if !UIDevice.current.isGeneratingDeviceOrientationNotifications {
            UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        }
        
        /*
        //
        //  Initialize Apple Watch connectivity (ignored if the platform does not support Apple Watch connectivity)
        //
        WatchConnectivityManager.setupWatchConnectivity()
        */
        
        //
        //  We don't want this App interrupted by the compass calibration screen, at any time.
        //
        AppLocationManager.allowsCompassCalibrationToDisplay = false
        
        //
        //  Force initialization of the 3D Object Manager (which sits atop our Places2NoteDB, providing an extra layer of abstraction for the ITS View App).
        //
        Object3DManager.initialize()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        Log.debug{ "*** AppDelegate.applicationWillResignActive() called." }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        Log.debug{ "*** AppDelegate.applicationDidEnterBackground() called." }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        Log.debug{ "*** AppDelegate.applicationWillEnterForeground() called." }
    }

    // 
    //  Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //
    func applicationDidBecomeActive(_ application: UIApplication) {
        //
        //  Whenever we are woken up, we check to see if offline map tiles have expired, as it may be a long time since we last awoke.
        //
        OfflineMapManager.purgeExpiredMapTiles()
        
        //
        //  Tell the Cloud() module that we're signed out now (as our credentials should be expired); then fire off the Cloud synchronization task after a brief delay.
        //
        Cloud.userIsSignedIn = false
        Async.main(after: 10.0) {
            Cloud.startSynchronizationTask()
        }

        //
        //  Ensure we are (still) checking the user's subscription expiry periodically.
        //
        AccountManager.restartSubscriptionExpiryCheckTimer()
        
        /*
        //
        //  If there is a paired Apple Watch, ensure that it has the latest relevant Settings.
        //
        Settings.sendSettingsToAppleWatch()
        */
        
        //
        //  A maximum of once per day, warn the user if they are running a version of iOS that's sub-optimal for ARKit functionality
        //
        ARKitVersionChecker.checkARKitVersionOncePerDay()
    }

    //
    //  Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    //
    func applicationWillTerminate(_ application: UIApplication) {
        //
        //  Per Apple Guidelines, remove the iTunes App Store transaction observer.
        //
        SKPaymentQueue.default().remove(ProductManager.sharedProductManager)
        
        Log.debug{ "*** AppDelegate.applicationWillTerminate() called." }
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        #if !CYGNUS
        //
        //  We are a registered handler for a variety of SpatialMedia files. If we have received one of these, we should offer the user a chance to either create a new Task
        //  with this file attached, or to attach this file to an existing Task.
        //
        if SpatialMediaManager.isSupportedSpatialMediaFileExtension(url: url) {
            Async.main {
                if let rootNavigationController = self.window!.rootViewController as? UINavigationController {
                    SpatialMediaImporter.importSpatialMediaFile(from: url, on: rootNavigationController)
                } else {
                    Log.severe{ "AppDelegate.importGPXfile(). Failed to instantiate the RootViewController!" }
                }
            }
            return true
        }
        #endif
        
        //
        //  Default fallthrough ... we didn't recognize the URL ...
        //
        Log.trace { "AddDelegate(): Fell through to end. The URL is \(url.path)" }
        return false
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        #if GEMINI
            NewsfeedManager.notificationSettingsChanged(notificationSettings)
        #endif
    }
    
    //
    //  -------------------------------------  P r i v a t e    M e t h o d s --------------------------------------------
    //
    
    
}

//
//  The following exception is thrown in iOS9 (seen first on 21 Jan 2016). For a start, I trapped it here in an effort to try and find out why it occurs. But the mere
//  existence of this function caused the iOS SDK to background the App whenever an orientation change event occurred. This took a while to track down. Do NOT
//  reinstate this function unless temporarily required to debug a "nonLaunchSpecificAction" issue.
//
/**
extension UIApplication {
    func _handleNonLaunchSpecificActions(arg1: AnyObject, forScene arg2: AnyObject, withTransitionContext arg3: AnyObject, completion completionHandler: () -> Void) {
        //
        //  Do we care? Apparently this is an Apple bug and harmless ...
        //
        //  print("UIApplication.handleNonLaunchSpecificActions: caught")
        //
    }
}
**/
