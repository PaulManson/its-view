//
//  Places2NoteDB_ITSView.swift
//  ITS View
//
//  Created by Paul Manson on 28/05/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import Foundation

///
/// Extensions to the Places2NoteDB database to filter out categories of Place which are not of relevance to the ITS View Application.
///

extension Places2NoteDB {
    
    ///
    /// The ITS View App deals internally with a Place category of "". Places belonging to this category are never displayed on the Map or in the Projects list. These are
    /// instead used as "templates" from which instances of 3D Objects can be positioned in Augmented Reality.
    ///
    static let kTemplateCategoryName = "category_3d_object_template"
    
    ///
    /// The 3D Object Templates belong to the following category group.
    ///
    static let kTemplateCategoryGroupName = "category_group_3d_object_templates"
    
    ///
    /// Augmented Reality sessions will generate a Place with a category of: "category_itview_project". These do appear on the Map and in the Projects list, and can be
    /// manipulated in similar ways to the Projects of the ITS Geo Solutions App, or the Tasks of the VisualMapper App.
    ///
    static let kProjectCategoryName = "category_itsview_project"
    
    ///
    /// Sessions/Projects belong to the following category group.
    ///
    static let kProjectCategoryGroupName = "category_group_personal"
    
    ///
    /// A PlacesFilter returning only those Places which are of relevance to the "Places" list in this App.
    ///
    static func placesRelevantToPlacesList(place: Place) -> Bool {
        return place.isRelevantToPlacesList
    }
    
    ///
    /// A PlacesFilter returning only those Places which are of relevance to the "Map" in this App.
    ///
    static func placesRelevantToMap(place: Place) -> Bool {
        return place.isRelevantToPlacesList
    }
}

extension Place {
    
    ///
    /// - Returns: true if this Place is of relevance to the "Places" list in this App.
    ///
    var isRelevantToPlacesList : Bool {
        get {
            return self.categoryName.lowercased() == Places2NoteDB.kProjectCategoryName
        }
    }
    
    ///
    /// - Returns: true if this Place is of relevance to the "Map" in this App.
    ///
    var isRelevantToMap : Bool {
        get {
            return self.categoryName.lowercased() == Places2NoteDB.kProjectCategoryName
        }
    }
    
    ///
    /// - Returns: true if this Place is of relevance to the App as a whole. This will be the case if it represents either a Project or a 3D Object Template.
    ///
    var isRelevantToApp : Bool {
        get {
            return self.categoryGroup.lowercased() == Places2NoteDB.kTemplateCategoryGroupName.lowercased() ||
                   self.categoryGroup.lowercased() == Places2NoteDB.kProjectCategoryGroupName.lowercased()
        }
    }
}
