//
//  Object3DManager.swift
//  ITS View
//
//  Created by Paul Manson on 23/05/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import Foundation
import PalaceSoftwareCore

///
/// This class bridges between the ITS View application, which wants to deal with **Object3D** templates, and the underlying Places2NoteDB, Cloud sync, etc mechanisms which
/// manage database storage, Perseids cloud synchronization, etc.
///
class Object3DManager {
    //
    //  ------------------------------------------------    P r i v a t e    P r o p e r t i e s    ------------------------------------------------
    //
    
    //
    //  The ITS View App will concern itself solely with the "Places" that belong to the "category_group_3d_object_templates" category group.
    //
    private static let shared = Object3DManager()
    
    static let kDemoObjectCategoryName = "Demo"
    
    private var objects   : [Object3D] = []
    
    //
    //  Clients can register an observer for the loading of all 3D Objects as a whole, or for subsequent incremental update to an individual 3D Object.
    //
    private var objectsObservers     : [String : ObjectsObserver] = [:]
    private var objectEventObservers : [String : ObjectObserver ] = [:]
    
    //
    //  We take care to ensure that we detect the precise moment when both the Places and Media database tables are available ...
    //
    private var isPlacesTableAvailable = false
    private var isMediaTableAvailable  = false
    private var isDefinitionsAvailable = false
    private var isLoading3DObjects     = false
    private var isLoaded3DObjects      = false

    //
    //  ------------------------------------------------    P r i v a t e    M e t h o d s    ------------------------------------------------
    //
    private init() {
        //
        //  Wait until both the Places and Media tables are available, as well as the P2NDefinitions, before assembling a local record of the 3D Object Template "Places".
        //
        Places2NoteDB.addObserver(forTable: Places2NoteDB.kPlacesTable) {
            guard !self.isLoading3DObjects else { return }
            
            self.isPlacesTableAvailable = true
                
            self.checkIfWeCanLoad3DObjectsNow()
        }
        
        Places2NoteDB.addObserver(forTable: Places2NoteDB.kMediaTable) {
            guard !self.isLoading3DObjects else { return }
            
            self.isMediaTableAvailable = true

            self.checkIfWeCanLoad3DObjectsNow()
        }
        
        Places2NoteDefinitions.addObserver {
            guard !self.isLoading3DObjects else { return }
            
            self.isDefinitionsAvailable = true
            
            self.checkIfWeCanLoad3DObjectsNow()
        }
    }
  
    private func checkIfWeCanLoad3DObjectsNow() {
        guard !self.isLoading3DObjects else { return }
        
        synched(self) {
            guard !self.isLoading3DObjects else { return }

            if self.isMediaTableAvailable && self.isPlacesTableAvailable && self.isDefinitionsAvailable {
                self.isLoading3DObjects = true
                
                Async.utility(after: 0.1) {
                    self.loadPlacesWhichAre3DObjects()
                    
                    //
                    //  After the initial load, we add our own observer for any subsequent incremental changes which may happen due to Sync.
                    //
                    Places2NoteDB.addObserver { (place: Place, event) in
                        guard place.isRelevantToApp else { return }
                        
                        guard place.isObject3D      else { return }
                        
                        #if !OBJECTMANAGER3D
                        //
                        //  If this place has been shared with us, we need to auto-accept it at this point, otherwise it won't show in our list of 3D Objects.
                        //
                        if place.sharedBy != "" {
                            self.automaticallyAccept(sharedPlace: place)
                            
                            //
                            //  We return at this point as the acceptance of this place will trigger an update event which will call this same observer in just a moment.
                            //  We process the update event instead of the acceptance event.
                            //
                            return
                        }
                        #endif
                        
                        //
                        //  We used to try to generate the 3D Object represented by this Place, but that requires it to be fully-formed with all the necessary Media
                        //  attachments (which might have been deleted already). We still try that approach (first), but if we fail to find a fully-formed Object we
                        //  instead scan our [Objects] array, as if we find a matching 3D Object there, we can still remove it if that's the event in question.
                        //
                        if let objectFromPlace = place.object3D {
                            //
                            //  The database will already have been updated, so all we need to do is ensure that our [Objects] array is in sync.
                            //
                            switch event {
                            case .insertion:
                                self.objects.append(objectFromPlace)
                            case .update:
                                self.objects.remove(objectFromPlace)
                                self.objects.append(objectFromPlace)
                            case .deletion:
                                self.objects.remove(objectFromPlace)
                            }

                            //
                            //  Call observers for an object event
                            //
                            self.callObjectEventObservers(object: objectFromPlace, event: event)
                        } else {
                            for object in self.objects {
                                if object.objectID == place.GUID {
                                    //
                                    //  When we've had to scan [Objects] for a matching object, the only event we can implement is .deletion. Anything else will result in
                                    //  us merely disregarding this Place and not treating it as a 3D Object.
                                    //
                                    switch event {
                                    case .insertion, .update:
                                        Log.error{ "--- PlaceEventHandler is Disregarding a Place which appears to be a 3D Object, but which isn't properly formed (we can't generate a .object3D). We've found a matching Object in our [Objects] list, but given that it appears to be compromised, the only safe thing to do is delete it! Place name = \(place.name)" }
                                        self.objects.remove(object)
                                    case .deletion:
                                        self.objects.remove(object)
                                    }
                                    
                                    //
                                    //  Call observers for an object event
                                    //
                                    self.callObjectEventObservers(object: object, event: event)
                                    return
                                }
                            }
                            
                            Log.debug{ "--- PlaceEventHandler is Disregarding a Place which appears to be a 3D Object, but which isn't properly formed (we can't generate a .object3D) and which doesn't match any of our existing [Objects] (hopefully because it's been deleted already). Place name = \(place.name)" }
                            }
                    }
                }
            }
        }
    }
    
    #if !OBJECTMANAGER3D
    private func automaticallyAccept(sharedPlace: Place) {
        sharedPlace.sharedBy    = ""
        Places2NoteDB.updatePlace(sharedPlace, mediaForPlace: [])
    }
    #endif
    
    private func loadPlacesWhichAre3DObjects() {
        #if !OBJECTMANAGER3D
        //
        //  Immediately before we load the filtered list of Places which happen to be 3D Objects, we want to automatically "accept" any sharedPlaces which might be
        //  sitting in our newsfeed.
        //
        var isSyncRequired = false
        if NewsfeedManager.newsfeedPlaces.count > 0 {
            for sharedPlace in NewsfeedManager.newsfeedPlaces {
                if sharedPlace.isObject3D {
                    self.automaticallyAccept(sharedPlace: sharedPlace)
                    isSyncRequired = true
                }
            }
        }
        #endif
        
        //
        //
        //
        let placeObjects = Places2NoteDB.places.filter({ (place) -> Bool in
            return place.categoryGroup == Places2NoteDB.kTemplateCategoryGroupName
        })
        
        let objects = placeObjects.compactMap { (place) -> Object3D? in
            return place.object3D
        }
        
        synched(self) {
            self.objects = objects
        }
        
        #if !OBJECTMANAGER3D
        //
        //  If there are NO 3D Objects in the database, and if this is the first time we have executed the App, then we want to insert the default
        //  3D Objects into the database (after which they will be synchronized to this user's account, once it's been created or signed in).
        //
        //  Note: Throughout this phase, the .isLoaded3DObjects property will still be false, which prevents any observers being notified as we add the demo
        //        objects to the ObjectManager and to the database that backs it.
        //
        if objects.count == 0 {
            if !Settings.isDemoDataAddedToDatabase {
                
                self.addDefault3DObjectsToDatabase()
                
                Settings.isDemoDataAddedToDatabase = true
            }
        }
        #endif

        //
        //  Call any registered observers.
        //
        self.isLoaded3DObjects = true
        self.callObjectsObservers()
        
        #if !OBJECTMANAGER3D
        //
        //  If we began this function by automatically accepting any "Shared" 3D Objects, we should now Sync with Perseids to cement those changes.
        //
        if isSyncRequired {
            Cloud.startSynchronizationTask()
        }
        #endif
    }
    
    private func registerObserverForObjects(_ key: String, observer: ObjectsObserver?) {
        objectsObservers[key] = observer
        
        if let observer = observer, Object3DManager.areObjectsAvailable {
            observer()
        }
    }
    
    private func registerObserverForObject(_ key: String, observer: ObjectObserver?) {
        objectEventObservers[key] = observer
    }
    
    private func callObjectsObservers() {
        for (_, observer) in self.objectsObservers {
            observer()
        }
    }
    
    private func callObjectEventObservers(object: Object3D, event: Object3DEvent) {
        for (_, observer) in self.objectEventObservers {
            observer(object, event)
        }
    }
    
    //
    //  Add the Demo data that is contained in the App bundle.
    //
    private func addDefault3DObjectsToDatabase() {
        
        struct DemoData {
            var objectName    : String
            var description   : String
            var thumbnailName : String
            var modelName     : String
            var modelScale    : Object3DParameters.Object3DScale
            var isYAxisUp     : Bool
        }
        
        let demoObjects : [DemoData] = [
            DemoData(objectName: "itsview_demo_object_telecom_box_name",            description: "itsview_demo_object_telecom_box_description",            thumbnailName: "TelecomBox", modelName: "utilitybox_smallTelecomBox", modelScale: .meters, isYAxisUp: false),
            DemoData(objectName: "itsview_demo_object_electricity_box_small_name",  description: "itsview_demo_object_electricity_box_small_description",  thumbnailName: "SmallElectricityBox", modelName: "utilitybox_smallElectricityBox", modelScale: .meters, isYAxisUp: false),
            DemoData(objectName: "itsview_demo_object_electricity_box_medium_name", description: "itsview_demo_object_electricity_box_medium_description", thumbnailName: "ElectricityStation", modelName: "ElectricityStation", modelScale: .inches, isYAxisUp: false),
            DemoData(objectName: "itsview_demo_object_electricity_box_large_name",  description: "itsview_demo_object_electricity_box_large_description",  thumbnailName: "BigElectricityBox", modelName: "utilitybox_bigElectricityBox", modelScale: .meters, isYAxisUp: false),
            DemoData(objectName: "itsview_demo_object_streetlamp_name",             description: "itsview_demo_object_streetlamp_description",             thumbnailName: "LampPost", modelName: "LampPost", modelScale: .inches, isYAxisUp: false),
            DemoData(objectName: "itsview_demo_object_lamppost_name",               description: "itsview_demo_object_lamppost_description",               thumbnailName: "LampPost_4m", modelName: "LampPost_4m", modelScale: .inches, isYAxisUp: false),
            DemoData(objectName: "itsview_demo_object_electricity_pylon_name",      description: "itsview_demo_object_electricity_pylon_description",      thumbnailName: "PowerPole", modelName: "PowerPole", modelScale: .inches, isYAxisUp: false),
            DemoData(objectName: "itsview_demo_object_electricity_pylon_50m_name",  description: "itsview_demo_object_electricity_pylon_50m_description",  thumbnailName: "Pylon_50m", modelName: "Pylon_50m", modelScale: .inches, isYAxisUp: false),
            DemoData(objectName: "itsview_demo_object_tree_name",                   description: "itsview_demo_object_tree_description",                   thumbnailName: "Tree", modelName: "Tree", modelScale: .inches, isYAxisUp: false),
            DemoData(objectName: "itsview_demo_object_mailbox_name",                description: "itsview_demo_object_mailbox_description",                thumbnailName: "MailBox", modelName: "MailBox_Ger", modelScale: .inches, isYAxisUp: false),
            DemoData(objectName: "itsview_demo_object_building_name",               description: "itsview_demo_object_building_description",               thumbnailName: "DataCenter", modelName: "DataCenter_80_200", modelScale: .inches, isYAxisUp: false)
        ]
        
        let kDemoDataFolder    = "DemoData"
        let kDemoDataSuffix    = ".scnassets"
        let kDemoDataExtension = "zip"
        
        if !Places2NoteDefinitions.categoryOrGroupExists(withName: Object3DManager.kDemoObjectCategoryName) {
            Places2NoteDefinitions.addCategory(Object3DManager.kDemoObjectCategoryName, toGroup: Places2NoteDB.kTemplateCategoryGroupName, withPin: Object3D.kPinNameFor3DObjects)
        }
        
        for demoObject in demoObjects {
            
            guard let thumbnailImage = Image(named: demoObject.thumbnailName) else {
                Log.error{ "DEMO OBJECT NAMED \(demoObject.thumbnailName) HAS NO THUMBNAIL IN THE APP BUNDLE! DISCARDING!" }
                continue
            }

            let modelName = demoObject.modelName + kDemoDataSuffix
            guard let modelURL = Bundle.main.url(forResource: modelName, withExtension: kDemoDataExtension, subdirectory: kDemoDataFolder) else {
                Log.error{ "DEMO OBJECT NAMED \(demoObject.thumbnailName) HAS NO MODEL IN THE APP BUNDLE! DISCARDING!" }
                continue
            }

            let localizedObjectName  = LM.localizedString(demoObject.objectName,  comment: "")
            let localizedDescription = LM.localizedString(demoObject.description, comment: "")
            
            self.addObject(name: localizedObjectName, category: Object3DManager.kDemoObjectCategoryName, description: localizedDescription, thumbnail: thumbnailImage, modelURL: modelURL, parameters: Object3DParameters(modelScale: demoObject.modelScale, isModelYAxisUp: demoObject.isYAxisUp))
        }
    }
    
    //
    //  This method adds a new, locally-created 3D Object to the database and to the [objects] list. It does so ONLY if it can successfully create
    //  the thumbnail and model as PlaceMedia attachments. This method itself constructs the Parameters attachment.
    //
    private func addObject(name: String, category: String, description: String?, thumbnail: Image, modelURL: URL, parameters: Object3DParameters) {
        let newObject = Object3D(objectID: SwiftUUID(), name: name, category: category, description: description, thumbnail: nil, model: nil, parameters: parameters)
        
        let placeForObject = Place(object: newObject)
        
        //
        //  Create PlaceMedia objects for the thumbnail and model
        //
        var mediaForPlace : [PlaceMedia] = []
        
        let thumbnailMedia = PlaceMedia(photoImage: thumbnail, withSuffix: "png", forPlace: placeForObject, withQuality: Media.MediaPhotoQuality.original)
        if let thumbnail = thumbnailMedia {
            mediaForPlace.append(thumbnail)
        } else {
            Log.error{ "FAILED TO CREATE A THUMBNAIL FOR THE 3D OBJECT NAMED \(name)" }
            return
        }
        
        let modelMedia = PlaceMedia(copyingMediaFromURL: modelURL, forPlace: placeForObject)
        if let model = modelMedia {
            mediaForPlace.append(model)
        } else {
            Log.error{ "FAILED TO CREATE A MODEL FOR THE 3D OBJECT NAMED \(name)" }
            return
        }
        
        //
        //  Create a PlaceMedia object for the Parameters.
        //
        if let parametersMedia = Object3DParametersMedia.CreateObject3DParameterMedia(parameters: parameters, for: placeForObject) {
            mediaForPlace.append(parametersMedia)
        } else {
            Log.error{ "FAILED TO CREATE A PARAMETERS MEDIA FILE FOR THE 3D OBJECT NAMED \(name)" }
            return
        }
        
        //
        //  Store the newly-created 3D Object to the database, along with its two media attachments.
        //
        Places2NoteDB.storePlace(placeForObject, mediaForPlace: mediaForPlace)
        
        //
        //  Add the completed new Object3D to our [objects] list.
        //
        newObject.thumbnail = thumbnailMedia
        newObject.model     = modelMedia
        
        self.objects.append(newObject)
    }
    
    //
    //  This method adds a new, locally-created 3D Object to the database and to the [objects] list. This method expects the caller to create the thumbnail and model
    //  Media attachments (if these are non-nil). This method also REQUIRES that the caller has previously constructed a Object3DParametersMedia file and stored it to the
    //  database.
    //
    private func add(object: Object3D) {
        let placeForObject = Place(object: object)
        
        //
        //  Create PlaceMedia objects for the thumbnail and model
        //
        var mediaForPlace : [PlaceMedia] = []
        if let thumbnail = object.thumbnail {
            mediaForPlace.append(thumbnail)
        }
        if let zipFile = object.model {
            mediaForPlace.append(zipFile)
        }
        if let parametersMediaFile = placeForObject.parametersMedia.first {
            mediaForPlace.append(parametersMediaFile)
        }
        
        //
        //  Store the newly-created 3D Object to the database, along with its three media attachments.
        //
        Places2NoteDB.storePlace(placeForObject, mediaForPlace: mediaForPlace)
        
        //
        //  We don't need to explicitly call object event observers as the Place observer will do that on our behalf.
        //
        /*
        //
        //  Call incremental Object event observers, provided we have completed loading.
        //
        if Object3DManager.areObjectsAvailable {
            callObjectEventObservers(object: object, event: .insertion)
        }
        */
    }
    
    //
    //  Delete the specified 3D object.
    //
    private func delete(object: Object3D) {
        guard self.objects.contains(object) else {
            Log.error{ "Asked to delete the 3D Object named \(object.name) but it DOESN'T EXIST in the [objects] list!" }
            return
        }
        
        #if !OBJECTMANAGER3D
            //
            //  In case we have previously loaded this model into Augmented Reality, we will have Unzipped its modelMedia into the /Models folder. Delete it from there.
            //
            Object3DModelManager.deleteModelURL(for: object)
        #endif
        
        //
        //  Delete the corresponding Place and its associated thumbnail and model Media attachments, if held locally.
        //
        guard let placeForObject = object.place else {
            Log.error{ "Asked to delete the 3D Object named \(object.name), which we have done. But the corresponding Place DOESN'T EXIST in the Places Database table!" }
            return
        }
        
        Places2NoteDB.deletePlace(placeForObject)
        
        //
        //  We don't need to explicitly call object event observers as the Place observer will do that on our behalf.
        //
        /*
        //
        //  Call incremental Object event observers, provided we have completed loading.
        //
        if Object3DManager.areObjectsAvailable {
            callObjectEventObservers(object: object, event: .deletion)
        }
        */
    }
    
    //
    //  The specified 3D object (and/or one of its Media attachments) has been updated. Persist those update(s) to the database.
    //
    private func update(object: Object3D) {
        guard self.objects.contains(object) else {
            Log.error{ "Asked to update a 3D Object named \(object.name) but it DOESN'T EXIST in the [objects] list! THIS OBJECT WILL BE DISCARDED!!!" }
            return
        }
        
        #if !OBJECTMANAGER3D
            //
            //  In case we have previously loaded this model into Augmented Reality, we will have Unzipped its modelMedia into the /Models folder. Delete it from there (forcing
            //  it to be reloaded on next use).
            //
            Object3DModelManager.deleteModelURL(for: object)
        #endif
        
        //
        //  Retrieve the corresponding Place, to ensure it's been previously written to the database.
        //
        guard let placeForObject = object.place else {
            Log.error{ "Asked to update a 3D Object named \(object.name), but the corresponding Place DOESN'T EXIST in the Places Database table! UPDATES DISCARDED!" }
            return
        }
        
        //
        //  Now generate a new Place from object, using the existing place's ID (the two MUST be identical). Update any of the media files which may have been modified.
        //
        var mediaForPlace = [object.thumbnail, object.model].compactMap { (placeMedia) -> PlaceMedia? in
            return placeMedia
        }
        if let parametersMedia = placeForObject.parametersMedia.first {
            mediaForPlace.append(parametersMedia)
        }
        
        let updatedPlace = Place(object: object)
        updatedPlace.ID  = placeForObject.ID
        
        Places2NoteDB.updatePlace(updatedPlace, mediaForPlace: mediaForPlace)
        
        //
        //  We don't need to explicitly call object event observers as the Place observer will do that on our behalf.
        //
        /*
        //
        //  Call incremental Object event observers, provided we have completed loading.
        //
        if Object3DManager.areObjectsAvailable {
            callObjectEventObservers(object: object, event: .update)
        }
        */
    }
    
    //
    //  Map the flat list of [objects] to a 2-dimensional "gallery", grouping objects by their category and sorting them alphabetically by name. The categories are
    //  also sorted alphabetically by name.
    //
    private func objects3DGallery() -> [Object3DGalleryCategory] {
        var galleryAsDictionary : [String : [Object3D]] = [:]
        
        for object in Object3DManager.objects {
            galleryAsDictionary[object.category] = (galleryAsDictionary[object.category] ?? []) + [object]
        }
        
        return galleryAsDictionary.map({ (categoryName, objectsInCategory) -> Object3DGalleryCategory in
            let sortedObjects = objectsInCategory.sorted(by: { (object1, object2) -> Bool in
                object1.name < object2.name
            })
            return Object3DGalleryCategory(categoryName: categoryName, objectsInCategory: sortedObjects)
        }).sorted(by: { (category1, category2) -> Bool in
            return category1.categoryName < category2.categoryName
        })
        
    }
    
    //
    //  ------------------------------------------------    P u b l i c    T y p e s    ------------------------------------------------
    //
    
    //
    //  Clients can register an observer for initial loading (or total reloading) of the 3D Objects as a whole, or for subsequent incremental modification to an
    //  individual object.
    //
    typealias ObjectsObserver = () -> ()
    
    typealias Object3DEvent   = Places2NoteDB.Places2NoteDBEvent
    typealias ObjectObserver  = (Object3D, Object3DEvent) -> ()
    
    ///
    /// 3D Objects are presented in groups, each of which corresponds to a specific Category Name.
    ///
    struct Object3DGalleryCategory {
        var categoryName      : String
        var objectsInCategory : [Object3D]
    }
    
    //
    //  ------------------------------------------------    P u b l i c    P r o p e r t i e s    ------------------------------------------------
    //

    ///
    /// - Returns: The flat list of available [objects].
    ///
    class var objects : [Object3D] {
        get {
            return shared.objects
        }
    }
    
    ///
    /// - Returns: True if the 3D Objects have been loaded from the database; otherwise False.
    ///
    class var areObjectsAvailable : Bool {
        get {
            return shared.isLoaded3DObjects
        }
    }
    
    ///
    /// - Returns: The flat list of [objects], mapped to a 2-dimensional "gallery", grouping objects by their category and sorting them alphabetically by name.
    ///            The categories are also sorted alphabetically by name.
    ///
    class var objectsGallery : [Object3DGalleryCategory] {
        get {
            return shared.objects3DGallery()
        }
    }
    
    //
    //  ------------------------------------------------    P u b l i c    M e t h o d s    ------------------------------------------------
    //
    
    ///
    /// The Object3DManager singleton will be initialized lazily, on first access, but can be forced to initialize explicitly via a call to the .initialize() method.
    ///
    class func initialize() {
        let _ = shared
    }

    class func registerObserverForObjects(_ key: String, observer: @escaping ObjectsObserver) {
        shared.registerObserverForObjects(key, observer: observer)
    }
    
    class func deregisterObserverForObjects(_ key: String) {
        shared.registerObserverForObjects(key, observer: nil)
    }
    
    class func registerObserverForObject(_ key: String, observer: @escaping ObjectObserver) {
        shared.registerObserverForObject(key, observer: observer)
    }

    class func deregisterObserverForObject(_ key: String) {
        shared.registerObserverForObject(key, observer: nil)
    }

    class func update(object: Object3D) {
        shared.update(object: object)
    }
    
    class func delete(object: Object3D) {
        shared.delete(object: object)
    }
    
    class func add(object: Object3D) {
        shared.add(object: object)
    }
    
    class func object(withID objectID: SwiftUUID) -> Object3D? {
        return self.objects.first(where: { (object) -> Bool in
            return object.objectID == objectID
        })
    }
}
