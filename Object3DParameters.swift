//
//  Object3DParameters.swift
//  ITS View
//
//  Created by Paul Manson on 3/07/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import Foundation
import PalaceSoftwareCore

///
/// In addition to the Thumbnail and 3D Model, each 3D Object also has a "Parameters" attachment which is a JSON representation of the Object3DParameters structure.
///
struct Object3DParameters : Codable, Equatable {
    
    ///
    /// An Object3DParameters Media file has a .json extension
    ///
    static let kMediaFileExtension = "json"
    
    enum Object3DScale : String, Codable {
        case meters
        case inches
    }
    
    var modelScale     : Object3DScale
    var isModelYAxisUp : Bool
    
    init(modelScale: Object3DScale, isModelYAxisUp: Bool) {
        self.modelScale     = modelScale
        self.isModelYAxisUp = isModelYAxisUp
    }
    
    ///
    /// There is a designated "default" set of Parameters for convenient initial creation of 3D Objects.
    ///
    static let kDefaultParameters = Object3DParameters(modelScale: .meters, isModelYAxisUp: false)
    
    ///
    /// Populate an Object3DParameters struct from the contents of an Object3DParametersMedia file, which will be a JSON representation generated using Codable.
    /// If the media file is not stored locally, the constructor fails.
    ///
    init?(_ parametersMedia: PlaceMedia) {
        guard parametersMedia.isLocalFileOnThisDevice else {
            Log.error{ "Rejected a Object3dParametersMedia file because it is NOT stored locally on this device." }
            return nil
        }
        
        guard parametersMedia.fileName.uppercased().hasSuffix(Object3DParametersMedia.shared.fileExtension.uppercased()) else {
            Log.error{ "Rejected a Object3dParametersMedia file because it does NOT have a \(Object3DParametersMedia.shared.fileExtension) suffix." }
            return nil
        }
    
        guard let parametersMediaURL = parametersMedia.getLocalMediaURL() else {
            Log.error{ "Rejected a Object3dParametersMedia file because it does NOT have a local URL on this device." }
            return nil
        }
        
        let jsonData: Data
        do {
            jsonData = try Data(contentsOf: parametersMediaURL)
        } catch let error {
            Log.severe{ "Failed to read the contents of \(parametersMediaURL.path) as JSON data! Error = \(error.localizedDescription)" }
            return nil
        }
        
        do {
            let parameters = try JSONDecoder().decode(Object3DParameters.self, from: jsonData)
            
            //
            //  Populate our parameters
            //
            self.modelScale     = parameters.modelScale
            self.isModelYAxisUp = parameters.isModelYAxisUp
            
        } catch let error {
            Log.severe{ "Failed to decode the Object3DParameters from JSON data! Error = \(error.localizedDescription)" }
            return nil
        }
    }
    
    ///
    /// Given a set of 3D Object Parameters, encode and write these to the supplied URL.
    /// - Returns: true for success, false for failure.
    ///
    func encodeAndWrite(to url: URL) -> Bool {
        let jsonData : Data
        
        //
        //  Encode the featureDefinitions
        //
        do {
            jsonData = try JSONEncoder().encode(self)
        } catch let error {
            Log.severe{ "Failed to generate JSON data encoding of a Object3DParameters object. Error = \(error.localizedDescription)" }
            return false
        }
        
        //
        //  Write the encoded featureDefinitions to the supplied URL.
        //
        do {
            try jsonData.write(to: url, options: Data.WritingOptions.atomic)
            return true
        } catch let error {
            Log.severe{ "Failed to write encoded Object3DParameters to a supplied temporary URL. Error = \(error.localizedDescription)" }
            return false
        }
    }
    
    //
    //  --------------------------------    C o d a b l e    P r o t o c o l    --------------------------------
    //
    
    private enum Object3DParametersKeys: String, CodingKey {
        case modelScale     = "ModelScale"
        case isModelYAxisUp = "IsModelYAxisUp"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Object3DParametersKeys.self)
        try container.encode(self.modelScale,     forKey: .modelScale)
        try container.encode(self.isModelYAxisUp, forKey: .isModelYAxisUp)
    }
    
    init(from decoder: Decoder) throws {
        let values     = try decoder.container(keyedBy: Object3DParametersKeys.self)
        
        self.modelScale     = try values.decode(Object3DScale.self, forKey: .modelScale)
        self.isModelYAxisUp = try values.decode(Bool.self,          forKey: .isModelYAxisUp)
    }
    
    //
    //  --------------------------------    E q u a t a b l e    P r o t o c o l    --------------------------------
    //
    static func ==(lhs: Object3DParameters, rhs: Object3DParameters) -> Bool {
        return (lhs.isModelYAxisUp == rhs.isModelYAxisUp) && (lhs.modelScale == rhs.modelScale)
    }
}
