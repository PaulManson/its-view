//
//  AboutViewController.swift
//  ITS View
//
//  Created by Paul Manson on 28/05/15.
//  Copyright (c) 2015 e.quinox KG. All rights reserved.
//

import UIKit
import PalaceSoftwareCore
import Eureka


class AboutViewController: FormViewController {
    
    @IBOutlet weak var imageViewWithRoundedCorners : UIImageView!
    @IBOutlet weak var lblPlaces2Note              : UILabel!
    @IBOutlet weak var lblVersion                  : UILabel!
    @IBOutlet weak var lblCopyright                : UILabel!
    
    //
    //  We assign the various docs for the ITS Geo Solutions App in viewDidLoad().
    //
    var webDocumentsToOpen : [WebDocumentToOpen] = []
    
    @IBOutlet weak var constrainedTableView: UITableView!
    
    override func viewDidLoad() {
        //
        //  Replace the default Eureka tableView with our constrained tableView.
        //
        super.tableView = self.constrainedTableView
        super.tableView.autoresizingMask = UIView.AutoresizingMask.flexibleWidth.union(.flexibleHeight)
        if #available(iOS 9.0, *) {
            super.tableView.cellLayoutMarginsFollowReadableWidth = false
        }
        
        super.viewDidLoad()

        self.navigationItem.title = App.localizedAboutTitle
        
        //
        //  Create and add the custom IU elements that weren't handled in IB.
        //
        self.navigationItem.leftBarButtonItem = SidebarButton(target: self, selector: #selector(HelpViewController.sidebarButtonPressed(_:)))
        
        let doneTitle = LM.localizedString("button_done", comment: "Standard Done button. Accepts the fields entered on a form. Equivalent to OK.")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: doneTitle, style: UIBarButtonItem.Style.plain, target: self, action: #selector(HelpViewController.btnDonePressed(_:)))
        
        //
        //  Initialize content
        //
        let isRunningInGerman = ((LM.applicationLanguageLocale.languageCode ?? "").lowercased() == "de")
        let termsOfService    = isRunningInGerman ? "https://its-service.de/richtlinien/agb.php"       : "https://its-service.eu/legal/terms-conditions.php"
        let privacyPolicy     = isRunningInGerman ? "https://its-service.de/richtlinien/impressum.php" : "https://its-service.eu/legal/imprint.php"
        let website           = isRunningInGerman ? "https://its-service.de/"                          : "https://its-service.eu/"
        
        let termsOfServicePrompt = LM.localizedString("title_terms_of_service", comment: "Title for the Terms of Service for Place2Note.")
        let privacyPolicyPrompt  = LM.localizedString("title_privacy_policy",   comment: "Title for the Privacy Policy for Place2Note.")
        
        self.webDocumentsToOpen = [
            WebDocumentToOpen(prompt: termsOfServicePrompt, isLocal: false, documentName: termsOfService, documentExtension: ""),
            WebDocumentToOpen(prompt: privacyPolicyPrompt,  isLocal: false, documentName: privacyPolicy,  documentExtension: ""),
            WebDocumentToOpen(prompt: "ITS Group",          isLocal: false, documentName: website,        documentExtension: "")
        ]
        
        //
        //  Build the form, starting with an icon.
        //
        
        self.imageViewWithRoundedCorners.image = Image(named: "AppIcon_128x128")!.maskWithRoundedRect(cornerRadius: 24.0)
        
        self.lblPlaces2Note.text = App.applicationLegalName
        self.lblVersion.text     = App.applicationVersionAndBuildString
        self.lblCopyright.text   = App.copyrightMessage
        
        //
        //  Now a separate section for each of the content document buttons
        //
        for doc in self.webDocumentsToOpen {
            
            form +++ Section()
                
                <<< ButtonRow() {
                    $0.title = doc.prompt
                }.onCellSelection { cell, row in
                    
                    if doc.isLocal {
                        guard AppReachability.checkNetworkReachabilityAndWarnIfNotReachable() else { return }
                    }
                    
                    if let webDocumentViewController = Storyboard.mainStoryboard().instantiateViewController(withIdentifier: "WebDocumentViewController") as? WebDocumentViewController {
                        webDocumentViewController.webDocument = doc
                        self.navigationController?.pushViewController(webDocumentViewController, animated: true)
                    } else {
                        Log.severe{ "AboutViewController() could not instantiate a webDocumentViewController!" }
                    }
                    
                }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        _ = SafeAsynchronousEventManager.checkForEvents(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func btnDonePressed(_ sender: AnyObject) {
        showMap()
    }
    
    @objc func sidebarButtonPressed(_ sender:UIBarButtonItem) {
        toggleSideMenuView()
    }
    
    private func showMap() {
        if MapViewController.singletonMapViewController == nil {
            Log.severe{ "PlacesList.showPlaceOnMap() tried to call MapViewController but it doesn't (yet) exist!" }
            return
        }
        
        let destViewController = MapViewController.singletonMapViewController!
        
        sideMenuController()?.setContentViewController(destViewController)
        sideMenuController()?.sideMenu?.toggleMenu()
        
        CustomToolbarManager.resetDefaultButtons()
    }
}

