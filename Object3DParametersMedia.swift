//
//  Object3DParametersMedia.swift
//  ITS View
//
//  Created by Paul Manson on 3/07/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import Foundation
import PalaceSoftwareCore

class Object3DParametersMedia {
    
    //
    //  ------------------------------------    P r i v a t e    P r o p e r t i e s   ------------------------------------
    //
    private var isDownloadingObject3DParameterMediaFile : Bool = false
    
    private var downloadRequests : [(SwiftUUID, PlaceMedia)] = []
    
    private var downloader : FileDownloader?
    
    //
    //  ------------------------------------    P r i v a t e    M e t h o d s   ------------------------------------
    //
    
    //
    //  Queue this request. If there's no download operation under way, initiate download of this request immediately; if not, the queued request will be
    //  actioned in its turn.
    //
    private func download(_ parameterMediaFile: PlaceMedia, for objectID: SwiftUUID) {
        synched(self) {
            self.downloadRequests.append((objectID, parameterMediaFile))
        
            //
            //  Unless there's already a download in progress, initiate downloading of the "next" file.
            //
            if !self.isDownloadingObject3DParameterMediaFile {
                self.isDownloadingObject3DParameterMediaFile = true
                
                Async.background {
                    self.downloadNextRequestedObject3DParameterMediaFile()
                }
            }
        }
    }
    
    private func downloadNextRequestedObject3DParameterMediaFile() {
        guard self.isDownloadingObject3DParameterMediaFile  else { return }
        guard self.downloader == nil else {
            Log.error{ "Resetting the Object3DParameterMediaFileDownloader because there is still a downloader running, apparently." }
            abandonDownload(isRemovingFirstRequest: false)
            return
        }
        guard let nextRequest = self.downloadRequests.first, let requestedMediaURL = nextRequest.1.mediaURL else {
            Log.error{ "Resetting the Object3DParameterMediaFileDownloader because there is no request pending, or that request has a nil mediaURL" }
            abandonDownload(isRemovingFirstRequest: true)
            return
        }
        
        //
        //  It's possible a request may be queued more than once. In that case, we might find that by the time we come to service this request the media file is
        //  already downloaded and available locally. If that's the case, just skip it and move on to the next request.
        //
        guard !nextRequest.1.isLocalFileOnThisDevice else {
            abandonDownload(isRemovingFirstRequest: true)
            return
        }
        
        //
        //  Create a new FileDownloader and commence downloading. The success or failure completion handler is called upon completion and tidies up before the next
        //  request is handled.
        //
        self.downloader = FileDownloader(requestedMediaURL, destinationFolder: Media.defaultMediaFolder, giveDownloadedFileUniqueName: false, progressHandler: { (_) in
            //
            //  We disply no progress indicator for these downloads ... they need to be 'silent'.
            //
        }, successHandler: { (_, mediaURLDownloaded) in
            //
            //  Update the localMedialURL for the downloaded PlaceMedia. Set the synchronized timestamp to the media file's created time; this ensures that we don't try
            //  to upload a media file that we've just downloaded from the server!
            //
            let requestedMedia = nextRequest.1
            requestedMedia.updateLocalMediaURL(newLocalMediaFileURL: mediaURLDownloaded)
            requestedMedia.synchronized = requestedMedia.created
            
            //
            //  Successful download means we should update the parameters for the 3D Object to which the downloaded Object3DParametersMediaFile belongs.
            //
            if let existingObject = Object3DManager.object(withID: nextRequest.0) {
                existingObject.updateParameters(parametersMediaFile: requestedMedia)
            } else {
                Log.debug{ "Downloaded and received an Object3DParametersMediaFile for 3D Object with ID = \(nextRequest.0) but there's no longer a matching Object according to the 3D Object Manager! Discarding the downloaded Media file ..." }
                _ = FileManager.deleteFile(mediaURLDownloaded)
            }
            
            //
            //  Now remove this request from the list and move on to the next ...
            //
            synched(self) {
                if self.downloadRequests.count > 0 {
                    self.downloadRequests.removeFirst()
                }
                
                self.downloader                              = nil
                self.isDownloadingObject3DParameterMediaFile = false
                self.considerNextDownloadRequest()
            }
        }, failureHandler: { (_, error) in
            //
            //  Failure to download removes the request from the list and moves on to try the next ...
            //
            Log.error{ "Failed to download a requested Object3DParametersMediaFile from Perseids! Error = \(error.localizedDescription). Media = \(requestedMediaURL.path)" }
            self.abandonDownload(isRemovingFirstRequest: true)
        })
        
        guard let downloader = self.downloader else {
            Log.severe{ "Failed to create a FileDownloader for the Object3DParametersMediaFile for ObjectID = \(nextRequest.0.UUIDString). Aborting Download of URL = \(requestedMediaURL.path)!" }
            self.abandonDownload(isRemovingFirstRequest: true)
            return
        }
        
        downloader.commenceDownload()
    }

    private func abandonDownload(isRemovingFirstRequest: Bool) {
        synched(self) {
            if isRemovingFirstRequest && self.downloadRequests.count > 0 {
                self.downloadRequests.removeFirst()
            }
            
            self.isDownloadingObject3DParameterMediaFile = false
            self.downloader                              = nil
            
            Async.background(after: 0.1) {
                self.considerNextDownloadRequest()
            }
        }
    }
    
    //
    //  Unless there's already a download in progress, initiate downloading of the "next" file.
    //
    private func considerNextDownloadRequest() {
        Async.background {
            synched(self) {
                guard self.downloadRequests.count > 0 else { return }
                
                if !self.isDownloadingObject3DParameterMediaFile {
                    self.isDownloadingObject3DParameterMediaFile = true
                    
                    Async.background {
                        self.downloadNextRequestedObject3DParameterMediaFile()
                    }
                }
            }
        }
    }
    
    //
    //  ------------------------------------    P u b l i c   P r o p e r t i e s   ------------------------------------
    //
    
    //
    //  Subclasses of SpatialMedia should override the .fileExtension and .mimeType properties (though the latter defaults to XML which may be useful in some cases).
    //
    var fileExtension : String {
        get {
            return "json"
        }
    }
    
    var mimeType : String {
        get {
            return "application/text"
        }
    }
    
    var customMediaType : CustomMediaManager.CustomMedia {
        get {
            return CustomMediaManager.CustomMedia(fileExtension: self.fileExtension, MIMEType: self.mimeType, getThumbnail: nil, getImage: nil, playButtonLabel: nil, playAction: nil, deleteAction: nil, tapAction: nil)
        }
    }
    
    static let shared = Object3DParametersMedia()
    
    //
    //  ------------------------------------    P u b l i c     M e t h o d s   ------------------------------------
    //
    
    ///
    /// Given an Object3DParameters object, create a matching Object3DParametersMedia file in the /Media folder, associated with the specified Place.
    /// - Returns: The PlaceMedia file, or nil in the event of failure (in which case no file will have been created).
    ///
    static func CreateObject3DParameterMedia(parameters: Object3DParameters, for place: Place) -> PlaceMedia? {
        //
        //  Create a temporary file, write the parameters to it in JSON format, and then create a PlaceMedia reference for the supplied Place, copying the file
        //  to the /Media folder in the process.
        //
        let temporaryFileName = SwiftUUID.uniqueFileName(withExtension: shared.fileExtension)
        let temporaryURL = FileManager.tmpFolderURL.appendingPathComponent(temporaryFileName)
        
        guard parameters.encodeAndWrite(to: temporaryURL) else {
            Log.error{ "Failed to encode and write Object3DParameters to a temporaryURL at: \(temporaryURL.path)" }
            _ = FileManager.deleteFile(temporaryURL)
            return nil
        }
        
        if let parametersMedia = PlaceMedia(copyingMediaFromURL: temporaryURL, forPlace: place) {
            return parametersMedia
        } else {
            Log.error{ "FAILED TO CREATE A PARAMETERS MEDIA FILE FOR THE 3D OBJECT WITH PLACE NAME = \(place.name)" }
            
            _ = FileManager.deleteFile(temporaryURL)
            
            return nil
        }
    }

    ///
    /// Request download of a specified ParametersMedia file, to be associated with the 3D Object having the specified ObjectID.
    ///
    static func download(_ parameterMediaFile: PlaceMedia, for objectID: SwiftUUID) {
        shared.download(parameterMediaFile, for: objectID)
    }
}
