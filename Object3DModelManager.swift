//
//  Object3DModelManager.swift
//  ITS View
//
//  Created by Paul Manson on 25/05/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import Foundation
import PalaceSoftwareCore
import Zip

///
/// The 3D models associated with 3D Objects are generally stored as Zipped-up Media files.
/// However, when a 3D model is first loaded, it is unzipped into a subfolder of the //Documents/Models folder, with the name of that subfolder being the unique objectID
/// with the .scnassets suffix.
///
/// All instances of a VirtualObject are created using the URL of that subfolder. This avoids the need to Unzip repeatedly, and allows multiple instances of a 3D
/// Object to be created using just a single URL.
///
/// When a 3D Object is deleted, the associated folder is also deleted, recovering the space.
///

class Object3DModelManager {

    //
    //  ------------------------------------------------    P r i v a t e    P r o p e r t i e s    ------------------------------------------------
    //
    static  let shared             = Object3DModelManager()
    private let kModelsFolderName  = "Models"
    private let kModelFolderSuffix = "scnassets"
    private let kModelExtension    = "scn"
    
    private var modelURLs : [SwiftUUID : URL] = [:]
    
    private var modelsFolder : URL {
        get {
            return FileManager.documentsFolderURL.appendingPathComponent(kModelsFolderName)
        }
    }
    
    //
    //  ------------------------------------------------    P r i v a t e    M e t h o d s    ------------------------------------------------
    //
    private init() {
        loadModelURLs()
    }
    
    private func loadModelURLs() {
        //
        //  If the modelsFolder doesn't already exist, create it.
        //
        if !FileManager.folderExists(self.modelsFolder) {
            if !FileManager.createFolder(self.modelsFolder) {
                Log.severe{ "FAILED TO CREATE THE /Documents/\(kModelsFolderName) FOLDER!" }
            }
        }
        
        //
        //  Load a dictionary of the current unzipped modelURLs
        //
        var modelURLs : [SwiftUUID : URL] = [:]
        
        for fileOrFolder in FileManager.contentsOfFolder(self.modelsFolder) {
            if fileOrFolder.isFolder {
                var folderName   = fileOrFolder.lastPathComponent
                let folderSuffix = ".\(kModelFolderSuffix)"
                if folderName.hasSuffix(folderSuffix) {
                    folderName.replace(folderSuffix, withString: "")
                }
                
                guard let modelObjectID = SwiftUUID(string: folderName) else {
                    Log.error{ "Failed to include Model folder with name = \(folderName) because we couldn't make a UUID out of it!" }
                    continue
                }
                
                modelURLs[modelObjectID] = fileOrFolder
            }
        }
        
        self.modelURLs = modelURLs
    }
    
    //
    //  Returns: The model URL for the specified 3D Object. If this model has previously been loaded, the URL will be returned immediately. If not, the model will be
    //           unzipped (if possible) and then returned. A nil response indicates failure (e.g. the model is not stored locally on this device)
    //
    private func modelURL(for object: Object3D) -> URL? {
        if let modelFolder = self.modelURLs[object.objectID] {
            if let modelURL = self.modelURL(in: modelFolder) {
                return modelURL
            } else {
                Log.error{ "Failed to find a .scn file in the Unzipped Model folder for 3D Object named: \(object.name)!" }
                return nil
            }
        }

        //
        //  This 3D Object hasn't been loaded before. So let's Unzip it and make a note of it so we can serve it up rapidly in future.
        //
        //  But first, make sure we have a local URL for this 3D Object's model
        //
        guard let modelMedia = object.model, modelMedia.isLocalFileOnThisDevice else {
            Log.error{ "Called with a nil model Media object, or the modelMedia's URL is not local to this device. 3D Object Model name: \(object.name)" }
            return nil
        }
        
        guard let modelURL = modelMedia.mediaURL else {
            Log.error{ "Called with a nil modelMedia URL for 3D Object Model name: \(object.name)" }
            return nil
        }
        
        //
        //  Unzip the 3D Model into a folder named XXXXXXXXXXXXX.scnassets where XXXXXXXXXXXX is the Object's unique ID.
        //
        let modelFolderName = object.objectID.UUIDString + ".\(kModelFolderSuffix)"
        let modelFolder     = self.modelsFolder.appendingPathComponent(modelFolderName)
        
        var isExitingWithError = false
        
        defer {
            //
            //  If we exit with an error after this point, we need to delete the folder we just (tried to) create. We use a defer{} block because there are multiple exits.
            //
            if isExitingWithError {
                if !FileManager.deleteFolder(modelFolder) {
                    Log.severe{ "Failed to delete the temporary folder at \(modelFolder.path)!" }
                }
            }
        }
        
        //
        //  Unzip the ZIP file into this model folder
        //
        do {
            try Zip.unzipFile(modelURL, destination: modelFolder, overwrite: true, password: nil, progress: { (progress) -> () in
                //  No need for any progress ... this should be fast!
            })
        } catch {
            Log.severe{ "Failed to UNZIP the file \(modelURL.path) into the folder \(modelFolder.path)" }
            isExitingWithError = true
            return nil
        }
        
        //
        //  Confirm that there actually is a .SCN model in the model folder; return the URL of that model.
        //
        guard let modelFileURL = self.modelURL(in: modelFolder) else {
            Log.error{ "Failed to find a .scn file in the Unzipped folder for 3D Object named: \(object.name)!" }
            
            return nil
        }
        
        return modelFileURL
    }
    
    //
    //  In response to deletion of the specified 3D Object (or at least, deletion of its model), we delete our Unzipped record of that model.
    //
    private func deleteModelURL(for object: Object3D) {
        guard let modelURL = self.modelURLs[object.objectID] else {
            // Log.error{ "ATTEMPTED TO DELETE 3D MODEL FOLDER FROM /DOCUMENTS/MODELS WHEN IT DOESN'T ACTUALLY EXIST THERE!" }
            return
        }
        
        self.modelURLs[object.objectID] = nil
        
        //
        //  Delete the folder
        //
        if !FileManager.deleteFolder(modelURL) {
            Log.error{ "FAILED TO DELETE 3D MODEL FOLDER FROM /DOCUMENTS/MODELS FOR 3D OBJECT NAMED: \(object.name)!" }
        }
    }
    
    //
    //  Clean up by deleting all unzipped /Models. But don't actually delete the /Models folder.
    //
    private func deleteAllModels() {
        var isRequiredDeletionOfModelsFolder = false
        
        for objectModelFolder in FileManager.contentsOfFolder(self.modelsFolder) {
            if objectModelFolder.isFolder {
                if !FileManager.deleteFolder(objectModelFolder) {
                    Log.error{ "Failed to delete \(objectModelFolder.path) [as a file]!" }
                    isRequiredDeletionOfModelsFolder = true
                }
            } else if objectModelFolder.isFile {
                if !FileManager.deleteFile(objectModelFolder) {
                    Log.error{ "Failed to delete \(objectModelFolder.path) [as a file]!" }
                    isRequiredDeletionOfModelsFolder = true
                }
            }
        }

        //
        //  If we ran into any problems at all, try deleting and re-creating the /Models folder.
        //
        if isRequiredDeletionOfModelsFolder {
            _ = FileManager.deleteFolder(self.modelsFolder)
        }
        
        //
        //  If the modelsFolder doesn't still exist, create it.
        //
        if !FileManager.folderExists(self.modelsFolder) {
            if !FileManager.createFolder(self.modelsFolder) {
                Log.severe{ "FAILED TO CREATE THE /Documents/\(kModelsFolderName) FOLDER!" }
            }
        }

        //
        //  We now have no models available ...
        //
        self.modelURLs = [:]        
    }
    
    //
    //  Given an recently-unzipped Model folder, try to locate a .scn file within it. If the folder has a .scn file at its top level, return it.
    //  If the folder contains a subfolder with the .scnassets suffix, look there instead.
    //
    private func modelURL(in folder: URL) -> URL? {
        if let sceneFile = FileManager.contentsOfFolder(folder, forFilesWithExtension: kModelExtension).first {
            return sceneFile
        }
        
        for fileOrSubfolder in FileManager.contentsOfFolder(folder) {
            if fileOrSubfolder.isFile {
                continue
            }
            
            if fileOrSubfolder.pathExtension != kModelFolderSuffix {
                continue
            }
            
            if let sceneFile = FileManager.contentsOfFolder(fileOrSubfolder, forFilesWithExtension: kModelExtension).first {
                return sceneFile
            }
        }
        
        return nil
    }
    
    //
    //  ------------------------------------------------    P u b l i c    T y p e s    ------------------------------------------------
    //
    
    //
    //  ------------------------------------------------    P u b l i c    P r o p e r t i e s    ------------------------------------------------
    //
    
    //
    //  ------------------------------------------------    P u b l i c    M e t h o d s    ------------------------------------------------
    //
    
    ///
    /// - Returns: The model URL for the specified 3D Object. If this model has previously been loaded, the URL will be returned immediately. If not, the model will be
    ///            unzipped (if possible) and then returned. A nil response indicates failure (e.g. the model is not stored locally on this device)
    ///
    class func modelURL(for object: Object3D) -> URL? {
        return shared.modelURL(for: object)
    }
    
    ///
    /// In response to deletion of the specified 3D Object (or at least, deletion of its model), we delete our Unzipped record of that model.
    ///
    class func deleteModelURL(for object: Object3D) {
        shared.deleteModelURL(for: object)
    }
    
    ///
    /// By way of cleaning up, delete all unzipped 3D Object models from the /Models folder
    ///
    class func deleteAllModels() {
        shared.deleteAllModels()
    }
}
