//
//  Object3DGalleryViewController.swift
//  ITS View
//
//  Created by Paul Manson on 23/05/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import UIKit
import PalaceSoftwareCore

class Object3DGalleryViewController: UIViewController, Object3DGalleryCellDelegate {
    
    private let kUniqueKey = String(describing: Object3DGalleryViewController.self)

    @IBOutlet weak var tableView: UITableView!
    
    private var gallery : [Object3DManager.Object3DGalleryCategory] = []
    
    private var isAtLeastOneCustomObject : Bool {
        get {
            for category in self.gallery {
                if category.categoryName.lowercased() != Object3DManager.kDemoObjectCategoryName.lowercased() {
                    return true
                }
            }
            
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = LM.localizedString("title_3d_object_gallery", comment: "Title for a form which displays each of the available 3D Objects")
        
        //
        //  Create and add the custom IU elements that weren't handled in IB.
        //
        self.navigationItem.leftBarButtonItem = SidebarButton(target: self, selector: #selector(Object3DGalleryViewController.sidebarButtonPressed(_:)))
        
        let doneTitle = LM.localizedString("button_done", comment: "Standard Done button. Accepts the fields entered on a form. Equivalent to OK.")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: doneTitle, style: UIBarButtonItem.Style.plain, target: self, action: #selector(Object3DGalleryViewController.btnDonePressed(_:)))
        
        self.tableView.dataSource = self
        self.tableView.delegate   = self
        
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetSource   = self
        
        self.tableView.tableFooterView = UIView(frame: .zero)   //  Ensure a lot of blank lines aren't displayed.
        
        if Object3DManager.areObjectsAvailable {
            self.reload3DObjectGallery()
        } else {
            Object3DManager.registerObserverForObjects(kUniqueKey) {
                self.reload3DObjectGallery()
            }
        }
        
        Object3DManager.registerObserverForObject(kUniqueKey) { (object, event) in
            self.reload3DObjectGallery()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        Object3DManager.deregisterObserverForObjects(kUniqueKey)
        Object3DManager.deregisterObserverForObject (kUniqueKey)
    }
    
    @objc func btnDonePressed(_ sender: AnyObject) {
        showMap()
    }
    
    @objc func sidebarButtonPressed(_ sender:UIBarButtonItem) {
        toggleSideMenuView()
    }
    
    private func showMap() {
        if MapViewController.singletonMapViewController == nil {
            Log.severe{ "PlacesList.showPlaceOnMap() tried to call MapViewController but it doesn't (yet) exist!" }
            return
        }
        
        let destViewController = MapViewController.singletonMapViewController!
        
        sideMenuController()?.setContentViewController(destViewController)
        sideMenuController()?.sideMenu?.toggleMenu()
        
        CustomToolbarManager.resetDefaultButtons()
    }
    
    private func reload3DObjectGallery() {
        Async.onMainThread {
            self.gallery = Object3DManager.objectsGallery
            
            self.tableView.reloadData()
        }
    }
    
    //
    //  Ask the user what they want to do with the selected place: Move it? Edit it? Or Delete it? etc
    //
    fileprivate func show3DObjectActionMenu(object: Object3D) {
        var actions : [Alert.Action] = []
        
        //
        //  If the 3D Model is not stored locally, provide the user with an option to download it.
        //
        if let model = object.model, !model.isLocalFileOnThisDevice {
            let downloadMenu = LM.localizedString("button_download_3d_model", comment: "Button to initiate download of a 3D model for display in the Augmented Reality view.")
            let downloadAction = Alert.Action(actionTitle: downloadMenu) {
                self.downloadModelButtonWasPressed(object: object)
            }
            actions.append(downloadAction)
        }
        
        //
        //  Share Object With My Team
        //
        let shareMenu   = LM.localizedString("menu_share_task_with_my_team", comment: "Title of the Form where the user can share a Task with their Team.")
        let shareAction = Alert.Action(actionTitle: shareMenu) {
            guard let placeForObject = object.place else {
                Log.error{ "Could not Share a 3D Object called \(object.name) because there is no corresponding Place!" }
                return
            }
            
            self.shareObjectWithTeam(placeForObject)
        }
        actions.append(shareAction)
        
        //
        //  Delete 3D Object
        //
        let deleteMenu   = LM.localizedString("menu_delete_3d_object", comment: "Menu entry allowing the user to delete a 3D object.")
        let deleteAction = Alert.Action(actionTitle: deleteMenu, isDestructive: true) {
           self.confirmDeletion(of: object)
        }
        actions.append(deleteAction)
        
        Alert.alertWithActions(object.name, message: nil, iconName: nil, actions: actions, lastOptionIsCancel: true, presentedOn: self)
    }
    
    private func shareObjectWithTeam(_ placeForObject: Place) {
        //
        //  Check that there is a network connection. If there isn't, sharing is off the menu for the moment ...
        //
        guard AppReachability.checkNetworkReachabilityAndWarnIfNotReachable() else { return }
        
        //
        //  Allow the user to select the Friend(s) with whom they wish to share this Place
        //
        guard let shareWithTeamViewController = Storyboard.mainStoryboard().instantiateViewController(withIdentifier: "ShareWithTeamViewController") as? ShareWithTeamViewController else {
            Log.error{ "Error: Could not make a ShareWithTeamViewController via the storyboard" }
            return
        }
        
        shareWithTeamViewController.setTaskToShare(placeForObject, completion: { (friendsWithWhomIShared: [Friend]) -> () in
            let title   = LM.localizedString("title_success", comment: "Title for the successful completion of a task.")
            let message = "The object named '\(placeForObject.name)' was shared with \(friendsWithWhomIShared.count) team members"

            Alert.simpleAlertWithOK(title, message: message, presentedOn: self, completion: nil)
        })
        
        self.navigationController?.pushViewController(shareWithTeamViewController, animated: true)
    }

    private func confirmDeletion(of object: Object3D) {
        let title   = LM.localizedString("title_please_confirm",    comment: "Title for a question asking the user to confirm an action (e.g. a deletion).")
        var message = LM.localizedString("text_delete_3d_object_x", comment: "Ask the user to confirm that they wish to delete the specified 3D Object. The object's name replaces @1 in the localized string.")
        message.replace("@1", withString: object.name)
        
        let deleteAction = Alert.Action(actionTitle: LM.localizedString("button_delete", comment: "The standard Delete button for dialog boxes."), isDestructive: true) {
            Object3DManager.delete(object: object)
            
            self.reload3DObjectGallery()
        }
        
        Alert.alertWithActions(title, message: message, iconName: nil, actions: [deleteAction], lastOptionIsCancel: true, presentedOn: self)
    }
    
    //
    //  Ask the user to confirm their wish to delete this specific 3D Object.
    //
    private func deleteObject(at indexPath: IndexPath) {
        guard let object = self.object(at: indexPath) else { return }
        
        self.confirmDeletion(of: object)
    }
    
    //
    //  Share the 3D Object at the row specified by the indexPath.
    //
    private func shareObject(at indexPath: IndexPath) {
        guard let object = self.object(at: indexPath) else { return }
        
        guard let placeForObject = object.place else {
            Log.error{ "Unable to retrieve a non-nil Place for the 3D Object named: \(object.name)" }
            return
        }
        
        self.shareObjectWithTeam(placeForObject)
    }
    
    ///
    /// - Returns: the 3D Object at the row specified by the indexPath, or nil if this isn't a genuine 3D Object (i.e. it's the advertisement for custom objects).
    ///
    private func object(at indexPath: IndexPath) -> Object3D? {
        if !self.isAtLeastOneCustomObject && indexPath.section >= self.gallery.count {
            return nil
        }
        
        let category       = self.gallery[indexPath.section.clamp(0 ..< self.gallery.count)]
        let row            = indexPath.row.clamp(0 ..< category.objectsInCategory.count)
        return category.objectsInCategory[row]
    }
    
    //
    //  ------------------------------------   O b j e c t 3 D G a l l e r y C e l l D e l e g a t e   ------------------------------------
    //
    func downloadModelButtonWasPressed(object: Object3D) {
        guard let modelMedia = object.model else { return }
        
        //
        //  Use the CloudFileTransfer mechanism because we want to display a HUD during the download.
        //
        CloudFileTransfer.downloadMediaFiles([modelMedia], hostView: self.view) { (downloadedModelMedia) in
            object.model = downloadedModelMedia.first
            //
            //  Persist the updated object to the database.
            //
            Object3DManager.update(object: object)

            self.reload3DObjectGallery()
        }
    }
}

extension Object3DGalleryViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if !self.isAtLeastOneCustomObject {
            return self.gallery.count + 1
        } else {
            return self.gallery.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !self.isAtLeastOneCustomObject && section >= self.gallery.count {
            return 1
        } else {
            return self.gallery[section].objectsInCategory.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //
        //  Is this the section promoting Custom 3D Objects?
        //
        if !self.isAtLeastOneCustomObject && indexPath.section >= self.gallery.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: PromoteCustom3DObjectsCell.kReuseIdentifier, for: indexPath) as! PromoteCustom3DObjectsCell
            cell.configure()
            return cell
        }
        
        //
        //  This is a regular cell.
        //
        let cell = tableView.dequeueReusableCell(withIdentifier: Object3DGalleryCell.kReuseIdentifier, for: indexPath) as! Object3DGalleryCell
        
        let object = self.object(at: indexPath)!
        
        cell.configure(object: object, delegate: self)
        
        return cell
    }
}

extension Object3DGalleryViewController : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !self.isAtLeastOneCustomObject && indexPath.section >= self.gallery.count {
            return PromoteCustom3DObjectsCell.kRowHeight
        } else {
            return Object3DGalleryCell.kRowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if !self.isAtLeastOneCustomObject && indexPath.section >= self.gallery.count {
            return false
        } else {
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !self.isAtLeastOneCustomObject && section >= self.gallery.count {
            return LM.localizedString("title_custom_3d_objects", comment: "Title for the section of the 3D Object Gallery which describes how to obtain custom 3D Objects.")
        } else {
            return self.gallery[section.clamp(0 ..< self.gallery.count)].categoryName
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var actions : [UITableViewRowAction] = []
        
        let deleteTitle = LM.localizedString("button_delete", comment: "The standard Delete button for dialog boxes.")
        let deleteAction = UITableViewRowAction(style: .normal, title: deleteTitle) { (rowAction, indexPath) in
            self.deleteObject(at: indexPath)
        }
        deleteAction.backgroundColor = .red
        actions.append(deleteAction)
        
        
        let shareTitle  = LM.localizedString("button_share", comment: "Title of the Share Button")
        let shareAction = UITableViewRowAction(style: .normal, title: shareTitle) { (rowAction, indexPath) in
            self.shareObject(at: indexPath)
        }
        shareAction.backgroundColor = Color.purple
        actions.append(shareAction)
        
        if let object = self.object(at: indexPath), let model = object.model, !model.isLocalFileOnThisDevice {
            let downloadTitle  = LM.localizedString("button_download", comment: "Download button. Starts an offline map download operation.")
            let downloadAction = UITableViewRowAction(style: .normal, title: downloadTitle) { (rowAction, indexPath) in
                self.downloadModelButtonWasPressed(object: object)
            }
            downloadAction.backgroundColor = .blue
            actions.append(downloadAction)
        }
        
        return actions
    }
    
    /*
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if !self.isAtLeastOneCustomObject && indexPath.section >= self.gallery.count { return }
            
        if editingStyle == UITableViewCellEditingStyle.delete {
            self.deleteObject(at: indexPath)
        }
    }
    */
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        //
        //  Is this the section promoting Custom 3D Objects?
        //
        if !self.isAtLeastOneCustomObject && indexPath.section >= self.gallery.count {
            return nil
        }

        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let selectedObject = self.object(at: indexPath) else { return }
        
        self.show3DObjectActionMenu(object: selectedObject)
    }
}

extension Object3DGalleryViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //
    //  -----------------------------------------   D N Z E m p t y D a t a S e t   -----------------------------------------
    //
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let localizedTitle = LM.localizedString("title_no_3d_objects", comment: "Title for the message informing the user that their database of 3D objects is empty.")
        let attrs = [
            NSAttributedString.Key.font            : UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline),
            NSAttributedString.Key.foregroundColor : Color.gray
        ]
        return NSAttributedString(string: localizedTitle, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let localizedDescription = LM.localizedString("text_no_3d_objects", comment: "Explain to the user that their library of 3D Object templates is currently empty, and that they should speak with their supervisor as it's him/her who will provide them with the appropriate 3D Objects for their project.")
        let attrs = [
            NSAttributedString.Key.font            : UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body),
            NSAttributedString.Key.foregroundColor : Color.darkGray
        ]
        return NSAttributedString(string: localizedDescription, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        let minimumDimension = min(400.0, min(Device.screenWidth, Device.screenHeight) * 0.6)
        let imageSize        = CGSize(width: minimumDimension, height: minimumDimension)
        return Image(named: "AppIcon_512x512")?.resize(imageSize, fitMode: ImageFitMode.scale).roundCorners(10.0)
    }
}

class PromoteCustom3DObjectsCell : UITableViewCell {
    
    static let kReuseIdentifier = "PromoteCustom3DObjectsCell"
    static let kRowHeight       = 150 as CGFloat
    
    @IBOutlet weak var lblPromotionalText: UILabel!
    
    func configure() {
        var promotionalText = LM.localizedString("text_explain_custom_3d_object_gallery_process", comment: "Explain to the user the process for obtaining customized 3G Objects. The App's name replaces @1 in the localized string, while the contact email address replaces @2.")
        promotionalText.replace("@1", withString: App.applicationName)
        promotionalText.replace("@2", withString: App.supportEmailAddress)

        self.lblPromotionalText.text      = promotionalText
        self.lblPromotionalText.textColor = App.signatureColor
    }
}


protocol Object3DGalleryCellDelegate : class {
    
    func downloadModelButtonWasPressed(object: Object3D)
    
}

class Object3DGalleryCell : UITableViewCell {
    
    static let kReuseIdentifier = "Object3DGalleryCell"
    static let kRowHeight       = 116 as CGFloat
    
    @IBOutlet weak var thumbnailImage       : UIImageView!
    @IBOutlet weak var lblName              : UILabel!
    @IBOutlet weak var lblDescription       : UILabel!
    @IBOutlet weak var lblIsModelDownloaded : UILabel!
    @IBOutlet weak var btnDownloadModel     : UIButton!
    @IBOutlet weak var pleaseWait           : UIActivityIndicatorView!
    
    private weak var delegate : Object3DGalleryCellDelegate? = nil
    private weak var object   : Object3D?                    = nil
    
    private var thumbnailDownloader : FileDownloader? = nil
    
    ///
    /// Configure this cell, through the provision of a 3D Object and a delegate.
    ///
    func configure(object: Object3D, delegate: Object3DGalleryCellDelegate) {
        self.delegate = delegate
        self.object   = object
        
        self.lblName.text        = object.name
        self.lblDescription.text = object.description
        
        //
        //  If the thumbnail for this Object is stored locally, display it. If not, display a "Media Download" image with a pleaseWait spinner. Once the thumbnail
        //  has been downloaded, store it with the Object and update the display.
        //
        let thumbnailImage : Image?
        if let thumbnail = object.thumbnail, thumbnail.isLocalFileOnThisDevice {
            thumbnailImage = thumbnail.getImage()
        } else {
            thumbnailImage = nil
        }
        if let thumbnail = thumbnailImage {
            self.thumbnailImage.image = thumbnail
        } else {
            self.thumbnailImage.image = Image(named: "Media_Image_Download")
            self.downloadThumbnail(for: object)
        }
        
        //
        //  If the 3D Model is stored locally, let the user know that. If not, provide the user with a button to download it.
        //
        if let model = object.model, model.isLocalFileOnThisDevice {
            self.lblIsModelDownloaded.isHidden = false
            self.lblIsModelDownloaded.text     = LM.localizedString("text_3d_model_stored_on_this_device", comment: "Inform the user that this 3D model is available for use on this device (having been downloaded previously).")
            self.btnDownloadModel.isHidden     = true
            self.btnDownloadModel.isEnabled    = false
        } else {
            self.lblIsModelDownloaded.isHidden = true
            self.lblIsModelDownloaded.text     = ""
            self.btnDownloadModel.isHidden     = false
            self.btnDownloadModel.isEnabled    = true
            self.btnDownloadModel.setTitle(LM.localizedString("button_download_3d_model", comment: "Button to initiate download of a 3D model for display in the Augmented Reality view."), for: .normal)
        }
    }
    
    ///
    /// The thumbnail image for this 3D Object is not available locally. Download it while displaying a downloading image and spinner.
    ///
    private func downloadThumbnail(for object: Object3D) {
        guard let thumbnailMedia = object.thumbnail else { return }
        
        guard let thumbnailMediaURL = thumbnailMedia.mediaURL else {
            Log.error{ "Asked to download a thumbnail but the .mediaURL is nil! We don't know where to find this thumbnail!" }
            return
        }
        
        Async.onMainThread {
            self.pleaseWait.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
            self.pleaseWait?.startAnimating()
        }
        
        Async.background {
            self.thumbnailDownloader = FileDownloader(thumbnailMediaURL, destinationFolder: Media.defaultMediaFolder, giveDownloadedFileUniqueName: false, progressHandler: { (_) in
                //
                //  This should be brief and we're displaying a spinner. Don't bother with explicit progress indication.
                //
            }, successHandler: { (_, downloadedThumbnailURL) in
                //
                //  We successfully downloaded the thumbnail image. Update the object's database record to confirm this fact, and update the display.
                //
                //  Update the localMediaURL for this thumbnail Media object. Set the synchronized timestamp to the media file's created time; this ensures
                //  that we don't try to upload a media file that we've just downloaded from the server!
                //
                thumbnailMedia.updateLocalMediaURL(newLocalMediaFileURL: downloadedThumbnailURL)
                thumbnailMedia.synchronized = thumbnailMedia.created
                
                //
                //  Persist the updated object to the database.
                //
                Object3DManager.update(object: object)
                
                //
                //  Update the display
                //
                Async.onMainThread {
                    self.pleaseWait?.stopAnimating()
                    self.thumbnailImage.image = thumbnailMedia.getImage()
                }
                
                self.thumbnailDownloader = nil
                
            }, failureHandler: { (_, error) in
                //
                //  We failed to downloaded the thumbnail image. Update the display?
                //
                Async.onMainThread {
                    self.pleaseWait?.stopAnimating()
                }
                Log.error{ "Failed to download the thumbnail image for 3D Object named: \(object.name). Error = \(error.localizedDescription)" }
                self.thumbnailDownloader = nil
            })
            
            self.thumbnailDownloader?.commenceDownload()
        }
        
    }
    
    ///
    /// We call our delegate when the "Download 3D Model" button is tapped, because we need to put up a HUD during download.
    ///
    @IBAction func btnDownloadModelPressed(_ sender: Any) {
        guard let object = self.object else { return }
        
        Async.onMainThread {
            self.delegate?.downloadModelButtonWasPressed(object: object)
        }
    }
    
}
