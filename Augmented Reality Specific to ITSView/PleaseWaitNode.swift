//
//  PleaseWaitNode.swift
//  ITS View
//
//  Created by Paul Manson on 1/06/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import Foundation
import SceneKit
import ARKit

///
/// A UIActivityIndicator for SceneKit. Displays in 3D and is ideally positioned just in front of the camera, at something of an angle, to ensure that it has a nice 3D
/// appearance.
///

class PleaseWaitNode : SCNNode {
    
    private var currentRotation : Float = 0.0
        
    override init() {
        let kLifeSize = 0.5 as CGFloat    //  Half a meter across
            
        super.init()
            
        //
        //  Create a plane of the specified size and add the "PleaseWait" bitmap as texture.
        //
        // let plane = SCNSphere(radius: kLifeSize)
        let plane = SCNPlane(width: kLifeSize, height: kLifeSize)
        
        let material                       = SCNMaterial()
        material.diffuse.contents          = Image(named: "PleaseWait")
        material.isDoubleSided             = true
        material.diffuse.contentsTransform = SCNMatrix4MakeScale(1, 1, 0)
        /*
        material.diffuse.contents = Color.red
        material.isDoubleSided = true
        */
        //
        //  Apply material
        //
        plane.materials = [material]
        
        //
        //  Add the Place as a child of ourselves, tilted to 45 degrees.
        //
        let planeNode = SCNNode(geometry: plane)
        //planeNode.eulerAngles.x = .pi / 2
        
        self.addChildNode(planeNode)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func updateRotation() {
        let kIterationsPerCircle = 32.0 as Float
        
        var newRotation = self.currentRotation + ((.pi * 2.0) / kIterationsPerCircle)
        
        if newRotation > (.pi * 2.0) {
            newRotation = newRotation - (.pi * 2.0)
        }
        
        self.currentRotation = newRotation
    }
    
    func update(camera: ARCamera) {
        self.updateRotation()
        
        self.simdTransform      = matrix_identity_float4x4
        self.eulerAngles.z      = self.currentRotation
        
        var translation         = self.simdTransform
        translation.columns.3.z = -2.0 // Translate 2m in front of the camera
        self.simdTransform      = matrix_multiply(camera.transform, translation)
    }
}
