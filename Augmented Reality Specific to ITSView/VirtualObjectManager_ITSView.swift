//
//  VirtualObjectManager.swift
//  ITS View
//
//  Created by Paul Manson on 23/05/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import Foundation
import ARKit
import SceneKit
import PalaceSoftwareCore
import PalaceSoftwareSpatial
import Zip

// MARK: - Delegate

protocol VirtualObjectManagerDelegate: class {
    func virtualObjectManager(_ manager: VirtualObjectManager, willLoad                     object: VirtualObject)
    func virtualObjectManager(_ manager: VirtualObjectManager, didLoad                      object: VirtualObject)
    func virtualObjectManager(_ manager: VirtualObjectManager, transformDidChangeFor        object: VirtualObject)
    func virtualObjectManager(_ manager: VirtualObjectManager, didMoveObjectOntoNearbyPlane object: VirtualObject, adjustingHeightBy distance: Distance)
    func virtualObjectManager(_ manager: VirtualObjectManager, couldNotPlace                object: VirtualObject)
    func virtualObjectManager(_ manager: VirtualObjectManager, didMove                      object: VirtualObject, to objectPosition: SCNVector3)
    func virtualObjectManager(_ manager: VirtualObjectManager, didCreate                    object: VirtualObject)
    func virtualObjectManager(_ manager: VirtualObjectManager, didDelete                    object: VirtualObject)

    func virtualObjectManager(_ manager: VirtualObjectManager, didSelect                    object: VirtualObject)
    func virtualObjectManager(_ manager: VirtualObjectManager, didDeselect                  object: VirtualObject)
}

// Optional protocol methods
extension VirtualObjectManagerDelegate {
    func virtualObjectManager(_ manager: VirtualObjectManager, transformDidChangeFor        object: VirtualObject) {}
    func virtualObjectManager(_ manager: VirtualObjectManager, didMoveObjectOntoNearbyPlane object: VirtualObject, adjustingHeightBy distance: Distance) {}
}

/***
///
/// There can be a high cost to loading a Virtual Object from a .scn file. We don't want to incur that cost repeatedly. So the first time we load a specific .scn file, we
/// keep a reference to the loaded VirtualObject (subclass of SCNReferenceNode).
///
/// If we subsequently receive a request to load another instance of the same 3D Object (i.e. the same .scn file), we instead *clone* the existing VirtualObject,
/// update its objectID and position and go ahead with that instead.
///
fileprivate class LoadedVirtualObject {
    
    var numberOfObjectsLoaded : Int
    var virtualObjectToClone  : VirtualObject
    
    init?(virtualObject: VirtualObject, numberOfObjectsLoaded: Int = 1) {
        self.numberOfObjectsLoaded = numberOfObjectsLoaded
        self.virtualObjectToClone  = virtualObject
    }
}
***/

///
/// The VirtualObjectManager manages instances of Object3D Templates in Augmented Reality.
///
class VirtualObjectManager {
    
    //
    //  Hold a record of the currently-selected 3D Object's physical state (position and rotation). This state is updated whenever a (translation or rotation) gesture
    //  ends. While such a gesture is in progress, the state is used to compute the delta from that state, and apply it to the selected Virtual Object.
    //
    private struct VirtualObjectWithState {
        var object   : VirtualObject
        
        //
        //  When we initially create a 3D Virtual Object, and whenever we subsequently select an existing 3D Virtual Object, we need a way to show that it is
        //  "Selected" in the AR View.
        //  We achieve this by displaying a flat rectangular grid (SelectedObjectGridNode) beneath the object, and slightly larger than the object's BoundingBox, to
        //  ensure that the grid is visible.
        //
        var gridNode : SelectedObjectGridNode?
        
        var rotation : Float
        var position : SCNVector3
        
        init(object: VirtualObject) {
            //
            //  Create and attach a SelectedObjectGridNode
            //
            let gridNode = SelectedObjectGridNode(for: object)
        
            self.gridNode = gridNode
            self.object   = object
            self.rotation = object.eulerAngles.y
            self.position = object.position
        
            Log.debug{ "*** Created VirtualObjectWithState: Object name is \(object.object.name), object is at \(self.position) with rotation = \(self.rotation). GridNode is at \(gridNode.position.description)" }
        }
    }
    
    //
    //  ------------------------------------------------    P r i v a t e    P r o p e r t i e s    ------------------------------------------------
    //
    
    //
    //  We maintain a list of all existing VirtualObjects
    //
    private var virtualObjects : [VirtualObject.VirtualObjectID : VirtualObject] = [:]
    
    /***
    //
    //  We also maintain a LoadedVirtualObjects manager which ensures that we only "load" (from .scn file) once per 3D Object Template.
    //  All duplicate .load requests are handled through ... [TO BE IMPLEMENTED!]
    //
    private var loadedVirtualObjects : [Object3D : LoadedVirtualObject] = [:]
    ***/
    
    //
    //  Whenever a 3D Virtual Object is selected, we maintain a current VirtualObjectWithState, which encapsulates all we need to know about this object, including
    //  its associated SelectedObjectGridNode
    //
    private var currentSelectedObject : VirtualObjectWithState? = nil {
        willSet {
            //
            //  Automatically remove and destroy the associated SelectedObjectGridNode if we have just de-selected a 3D Object.
            //
            if newValue == nil {
                currentSelectedObject?.gridNode?.removeFromParentNode()
                currentSelectedObject?.gridNode = nil
            }
        }
    }
    
    ///
    /// The queue with updates to the virtual objects are made on.
    ///
    private var updateQueue: DispatchQueue
    
    private let kTiny = (1.0 / 10000.0) as Float
    private var kTinyScale : SCNVector3 {
        get {
            return SCNVector3(kTiny, kTiny, kTiny)
        }
    }
    
    ///
    /// Objects which are "near enough" to a new or updated Plane are automatically moved onto it. We set a "near enough" tolerance of 0.5m.
    ///
    /// This helps to avoid objects jumping around if they are situated on another plane.
    ///
    static let kVerticalObjectToPlaneTolerance = 0.5 as Float    //  0.5m
    
    //
    //  ------------------------------------------------    P r i v a t e    M e t h o d s    ------------------------------------------------
    //
    init(updateQueue: DispatchQueue) {
        self.updateQueue = updateQueue
    }
    
    // MARK: - Resetting objects
    
    func removeAllVirtualObjects() {
        let cloneOfVirtualObjects = self.virtualObjects
        
        for (_, virtualObject) in cloneOfVirtualObjects {       //  Can't enumerate and mutate ...
            self.delete(virtualObject: virtualObject)
        }
        virtualObjects.removeAll()
    }
    
    func unload(virtualObject object: VirtualObject) {
        updateQueue.async {
            object.unload()
            object.removeFromParentNode()
        }
    }
    
    // MARK: - Loading object
    
    func loadAndSelect(virtualObject: VirtualObject, at position: SCNVector3, in sceneView: ARSCNView) {
        self.virtualObjects[virtualObject.virtualObjectID] = virtualObject
        
        self.delegate?.virtualObjectManager(self, willLoad: virtualObject)
        
        //
        //  Load the content asynchronously. The delegate can potentially display a Please Wait spinner between .willLoad() and .didLoad().
        //
        DispatchQueue.global(qos: .userInitiated).async {
            
            virtualObject.load()
            
            self.delegate?.virtualObjectManager(self, didLoad: virtualObject)

            //
            //  Immediately place the object in 3D space.
            //
            self.updateQueue.async {
                self.setPosition(of: virtualObject, to: position)

                self.delegate?.virtualObjectManager(self, didCreate: virtualObject)
                
                //
                //  Select the newly-added object
                //
                self.select(virtualObject, in: sceneView)
            }
        }
    }
    
    //
    //  Similar to the other loadAndSelect method, but this one places the virtualObject's near edge (as determined from its maximum boundingBox) at the
    //  latest detected planePosition.
    //
    func loadAndSelect(virtualObject: VirtualObject, atOrBehind crosshairsPosition: SCNVector3, camera cameraTransform: SCNMatrix4, in sceneView: ARSCNView) {
        
        self.virtualObjects[virtualObject.virtualObjectID] = virtualObject
        
        self.delegate?.virtualObjectManager(self, willLoad: virtualObject)
        
        //
        //  Load the content asynchronously. The delegate can potentially display a Please Wait spinner between .willLoad() and .didLoad().
        //
        DispatchQueue.global(qos: .userInitiated).async {
            
            virtualObject.load()
            
            self.delegate?.virtualObjectManager(self, didLoad: virtualObject)
            
            //
            //  Having loaded the object's model, we can check its footprint size and work out how far away from us to push it.
            //  Start by placing the object on the ground, directly beneath the camera. Then push it away from us by the distance from the camera to the
            //  crosshairs, plus the object's footprint radius.
            //
            let cameraPosition             = SCNVector3(cameraTransform.m41, crosshairsPosition.y, cameraTransform.m43)
            let cameraToCrosshairsDistance = Distance(meters: Double(cameraPosition.distance(from: crosshairsPosition)))
            let footprintRadius            = virtualObject.footprintRadius
            let distanceToPushObject       = cameraToCrosshairsDistance + footprintRadius
            //
            //  Direction is the orientation of the camera in world space, but pointing directly ahead (0.0 in the y dimension)
            //
            var finalPosition   = self.pointInFront(ofCameraWith: cameraTransform, at: distanceToPushObject)
            finalPosition.y     = crosshairsPosition.y
            
            Log.isVerbose = false
            Log.debug{ "+----------------------------------------+" }
            Log.debug{ "|      Object Placement Calculations     |" }
            Log.debug{ "+----------------------------------------+" }
            Log.debug{ "| Camera is            at \(cameraPosition.description)" }
            Log.debug{ "| Footprint Radius     is \(footprintRadius.meters)" }
            Log.debug{ "| Pushing Distance     is \(distanceToPushObject.meters)" }
            Log.debug{ "| Object final pos     is \(finalPosition.description)" }
            let distance = finalPosition.distance(from: cameraPosition)
            Log.debug{ "| Distance             is \(distance)" }
            Log.debug{ "+----------------------------------------+" }
            Log.isVerbose = true
            
            //
            //  Make a note of the correct scale for the object, then set the scale to something really, really tiny. We then animate up to the correct scale as we pan the
            //  object to the correct position
            //
            let correctObjectScale = virtualObject.scale
            virtualObject.scale    = self.kTinyScale
            
            //
            //  Immediately place the object in 3D space.
            //
            self.updateQueue.async {
                //
                //  Instantaneously move the (miniscule) object to the current cameraPosition
                //
                self.setPosition(of: virtualObject, to: cameraPosition)
                
                self.delegate?.virtualObjectManager(self, didCreate: virtualObject)
                
                //
                //  Animate the object into the desired final position
                //
                let moveToFinalPosition = SCNAction.move (to: finalPosition,                 duration: 1.5)
                let scaleToFinalSize    = SCNAction.scale(to: CGFloat(correctObjectScale.x), duration: 1.5)
                let moveAndScaleGroup   = SCNAction.group([moveToFinalPosition, scaleToFinalSize])
                let completion          = SCNAction.run({ (object) in
                    //
                    //  Ensure the scale is correct
                    //
                    virtualObject.scale = correctObjectScale
                    
                    //
                    //  Update the position to which we've moved the object.
                    //
                    self.delegate?.virtualObjectManager(self, didMove: virtualObject, to: finalPosition)
                    
                    //
                    //  Once it has finished moving, visibly select the newly-added object
                    //
                    self.select(virtualObject, in: sceneView)
                })
                
                virtualObject.runAction(SCNAction.sequence([moveAndScaleGroup, completion]))
            }
        }
    }
    
    //
    //  Similar to the other loadAndSelect method, but this one takes the virtualObject's maximum boundingBox dimension into account and animates the object's appearance
    //  so that it's at least the specified distance in front of the camera.
    //
    func loadAndSelect(virtualObject: VirtualObject, atAnAppropriateDistance appropriateDistanceClosure: @escaping AppropriateDistanceClosure, inFrontOfCamera cameraTransform: SCNMatrix4, onPlaneAt planeHeight: Float, in sceneView: ARSCNView) {
        
        self.virtualObjects[virtualObject.virtualObjectID] = virtualObject
        
        self.delegate?.virtualObjectManager(self, willLoad: virtualObject)
        
        //
        //  Load the content asynchronously. The delegate can potentially display a Please Wait spinner between .willLoad() and .didLoad().
        //
        DispatchQueue.global(qos: .userInitiated).async {
            
            virtualObject.load()
            
            self.delegate?.virtualObjectManager(self, didLoad: virtualObject)
            
            //
            //  Having loaded the object's model, we can check its footprint size and work out how far away from us to push it.
            //  Start by placing the object on the ground, directly beneath the camera.
            //
            let cameraPosition = SCNVector3(cameraTransform.m41, planeHeight, cameraTransform.m43)
            
            //
            //  Compute the scaled footprint radius for the 3D Object, adding this to the supplied distance to yield a minimum distance between the camera and the object.
            //
            let footprintRadius     = virtualObject.footprintRadius
            let appropriateDistance = appropriateDistanceClosure(footprintRadius)
            
            //
            //  Direction is the orientation of the camera in world space, but pointing directly ahead (0.0 in the y dimension)
            //
            var finalPosition   = self.pointInFront(ofCameraWith: cameraTransform, at: appropriateDistance + footprintRadius)
            finalPosition.y     = cameraPosition.y
            
            Log.isVerbose = false
            Log.debug{ "+----------------------------------------+" }
            Log.debug{ "|      Object Placement Calculations     |" }
            Log.debug{ "+----------------------------------------+" }
            Log.debug{ "| Camera is            at \(cameraPosition.description)" }
            Log.debug{ "| Footprint Radius     is \(footprintRadius.meters)" }
            Log.debug{ "| Appropriate Distance is \(appropriateDistance.meters)" }
            Log.debug{ "| Object final pos     is \(finalPosition.description)" }
            let distance = finalPosition.distance(from: cameraPosition)
            Log.debug{ "| Distance             is \(distance)" }
            Log.debug{ "+----------------------------------------+" }
            Log.isVerbose = true
            
            //
            //  Make a note of the correct scale for the object, then set the scale to something really, really tiny. We then animate up to the correct scale as we pan the
            //  object to the correct position
            //
            let correctObjectScale = virtualObject.scale
            virtualObject.scale    = self.kTinyScale
            
            //
            //  Immediately place the object in 3D space.
            //
            self.updateQueue.async {
                //
                //  Instantaneously move the (miniscule) object to the current cameraPosition
                //
                self.setPosition(of: virtualObject, to: cameraPosition)
                
                self.delegate?.virtualObjectManager(self, didCreate: virtualObject)
                
                //
                //  Animate the object into the desired final position
                //
                let moveToFinalPosition = SCNAction.move (to: finalPosition,                 duration: 1.5)
                let scaleToFinalSize    = SCNAction.scale(to: CGFloat(correctObjectScale.x), duration: 1.5)
                let moveAndScaleGroup   = SCNAction.group([moveToFinalPosition, scaleToFinalSize])
                let completion          = SCNAction.run({ (object) in
                    //
                    //  Ensure the scale is correct
                    //
                    virtualObject.scale = correctObjectScale
                    
                    //
                    //  Update the position to which we've moved the object.
                    //
                    self.delegate?.virtualObjectManager(self, didMove: virtualObject, to: finalPosition)
                    
                    //
                    //  Once it has finished moving, visibly select the newly-added object
                    //
                    self.select(virtualObject, in: sceneView)
                })
                
                virtualObject.runAction(SCNAction.sequence([moveAndScaleGroup, completion]))
            }
        }
    }
    
    /***
    private func pointInFront(of point: SCNVector3, direction: SCNVector3, distance: Distance) -> SCNVector3 {
        let distanceInMeters = Float(distance.meters)
        
        let x = point.x + distanceInMeters * direction.x
        let y = point.y + distanceInMeters * direction.y
        let z = point.z + distanceInMeters * direction.z
        
        return SCNVector3(x, y, z)
    }
    ***/
    
    //
    //  Generate the point directly in front of the camera, at the specified distance (ignoring height variation, i.e. flatten the camera's direction).
    //
    private func pointInFront(ofCameraWith cameraTransform: SCNMatrix4, at distance: Distance) -> SCNVector3 {
        
        let normalizedCameraDirection         = SCNVector3(-cameraTransform.m31, 0.0, -cameraTransform.m33).normalized()
        let scaledNormalizedCameraTranslation = normalizedCameraDirection * Float(distance.meters)
        
        return SCNVector3.positionFromTransform(matrix_float4x4(cameraTransform)) + scaledNormalizedCameraTranslation
    }
    
    /***
    private func pointInFront(ofCameraWith cameraTransform: SCNMatrix4, distance: Distance) -> SCNVector3 {
        let referenceNodeTransform = matrix_float4x4(cameraTransform)
        
        //
        //  Setup a translation matrix with the desired position
        //
        var translationMatrix = matrix_identity_float4x4
        translationMatrix.columns.3.x = 0.0
        translationMatrix.columns.3.y = 0.0
        translationMatrix.columns.3.z = Float(distance.meters)      //  "In front by z meters"
        
        //
        //  Combine the configured translation matrix with the camera's transform to get the desired position
        //
        let updatedTransform = matrix_multiply(referenceNodeTransform, translationMatrix)
        return SCNVector3(updatedTransform.translation)
    }
    ***/
    
    // MARK: - Update object position
    
    /**
    func translate(_ object: VirtualObject, in sceneView: ARSCNView, basedOn screenPos: CGPoint, instantly: Bool, infinitePlane: Bool) {
        DispatchQueue.main.async {
            let result = self.worldPositionFromScreenPosition(screenPos, in: sceneView, objectPos: object.simdPosition, infinitePlane: infinitePlane)
            
            guard let newPosition = result.position else {
                self.delegate?.virtualObjectManager(self, couldNotPlace: object)
                return
            }
            
            guard let cameraTransform = sceneView.session.currentFrame?.camera.transform else {
                return
            }
            
            self.updateQueue.async {
                self.setPosition(for: object,
                                 position: newPosition,
                                 instantly: instantly,
                                 filterPosition: !result.hitAPlane,
                                 cameraTransform: cameraTransform)
            }
        }
    }
    
    private func setPosition(for
        object: VirtualObject, position: float3, instantly: Bool, filterPosition: Bool, cameraTransform: matrix_float4x4) {
        if instantly {
            setNewVirtualObjectPosition(object, to: SCNVector3(position), cameraTransform: cameraTransform)
        } else {
            updateVirtualObjectPosition(object, to: position, filterPosition: filterPosition, cameraTransform: cameraTransform)
        }
    }
    
    private func setNewVirtualObjectPosition(_ object: VirtualObject, to position: SCNVector3, cameraTransform: matrix_float4x4) {
        let cameraWorldPos   = cameraTransform.translation
        var cameraToPosition = float3(position) - cameraWorldPos
        
        delegate?.virtualObjectManager(self, didMove: object, to: position)
        
        //
        //  Limit the distance of the object from the camera to a maximum of 10 meters.
        //
        if simd_length(cameraToPosition) > 10 {
            cameraToPosition = simd_normalize(cameraToPosition)
            cameraToPosition *= 10
        }
        
        object.simdPosition = cameraWorldPos + cameraToPosition
        object.recentVirtualObjectDistances.removeAll()
    }
    **/
    
    private func setPosition(of object: VirtualObject, to position: SCNVector3) {
        object.position = position
        delegate?.virtualObjectManager(self, didMove: object, to: position)
    }
    
    /**
    private func updateVirtualObjectPosition(_ object: VirtualObject, to pos: float3, filterPosition: Bool, cameraTransform: matrix_float4x4) {
        let cameraWorldPos = cameraTransform.translation
        var cameraToPosition = pos - cameraWorldPos
        
        // Limit the distance of the object from the camera to a maximum of 10 meters.
        if simd_length(cameraToPosition) > 10 {
            cameraToPosition = simd_normalize(cameraToPosition)
            cameraToPosition *= 10
        }
        
        // Compute the average distance of the object from the camera over the last ten
        // updates. If filterPosition is true, compute a new position for the object
        // with this average. Notice that the distance is applied to the vector from
        // the camera to the content, so it only affects the percieved distance of the
        // object - the averaging does _not_ make the content "lag".
        let hitTestResultDistance = simd_length(cameraToPosition)
        
        object.recentVirtualObjectDistances.append(hitTestResultDistance)
        object.recentVirtualObjectDistances.keepLast(10)
        
        if filterPosition, let averageDistance = object.recentVirtualObjectDistances.average {
            let averagedDistancePos = cameraWorldPos + simd_normalize(cameraToPosition) * averageDistance
            object.simdPosition = averagedDistancePos
        } else {
            object.simdPosition = cameraWorldPos + cameraToPosition
        }
    }
    **/
    
    //
    //  ------------------------------------------------    P u b l i c    M e t h o d s    ------------------------------------------------
    //
    
    func checkIfObjectsShouldMoveOntoPlane(anchor: ARPlaneAnchor, planeAnchorNode: SCNNode) {
        
        for (_, object) in virtualObjects {
            //
            //  Get the object's position in the plane's coordinate system.
            //
            let objectPos = planeAnchorNode.convertPosition(object.position, from: object.parent)
            
            if objectPos.y == 0 {
                //
                //  The object is already on the plane - nothing to do here.
                //
                return
            }
            
            //
            //  Add 10% tolerance to the corners of the plane.
            //
            let tolerance: Float = 0.1
            
            let minX: Float = anchor.center.x - anchor.extent.x / 2 - anchor.extent.x * tolerance
            let maxX: Float = anchor.center.x + anchor.extent.x / 2 + anchor.extent.x * tolerance
            let minZ: Float = anchor.center.z - anchor.extent.z / 2 - anchor.extent.z * tolerance
            let maxZ: Float = anchor.center.z + anchor.extent.z / 2 + anchor.extent.z * tolerance
            
            if objectPos.x < minX || objectPos.x > maxX || objectPos.z < minZ || objectPos.z > maxZ {
                return
            }
            
            //
            //  Move the object onto the plane if it is near it (within kVerticalTolerance centimeters) and at has magnitude of least 1 mm
            //
            let kEpsilonOneMillimeter = 0.001 as Float
            
            let distanceToPlane = abs(objectPos.y)
            if distanceToPlane > kEpsilonOneMillimeter && distanceToPlane < VirtualObjectManager.kVerticalObjectToPlaneTolerance {
                delegate?.virtualObjectManager(self, didMoveObjectOntoNearbyPlane: object, adjustingHeightBy: Distance(meters: Double(objectPos.y)))
                
                SCNTransaction.begin()
                SCNTransaction.animationDuration       = CFTimeInterval(distanceToPlane * 50) // Move 2 cm per second.
                SCNTransaction.animationTimingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                object.position.y                      = anchor.transform.columns.3.y
                
                //
                //  If this object is currently selected, we also need to adjust the height of the associated gridNode.
                //
                if object == self.currentSelectedObject?.object {
                    self.currentSelectedObject?.gridNode?.position.y = anchor.transform.columns.3.y
                }
                
                SCNTransaction.commit()
            }
        }
    }
    
    func transform(for object: VirtualObject, cameraTransform: matrix_float4x4) -> (distance: Float, rotation: Int, scale: Float) {
        let cameraPos = cameraTransform.translation
        let vectorToCamera = cameraPos - object.simdPosition
        
        let distanceToUser = simd_length(vectorToCamera)
        
        var angleDegrees = Int((object.eulerAngles.y * 180) / .pi) % 360
        if angleDegrees < 0 {
            angleDegrees += 360
        }
        
        return (distanceToUser, angleDegrees, object.scale.x)
    }
    
    func worldPositionFromScreenPosition(_ position: CGPoint,
                                         in sceneView: ARSCNView,
                                         objectPos: float3?,
                                         infinitePlane: Bool = false) -> (position: float3?, planeAnchor: ARPlaneAnchor?, hitAPlane: Bool) {
        
        // -------------------------------------------------------------------------------
        // 1. Always do a hit test against exisiting plane anchors first.
        //    (If any such anchors exist & only within their extents.)
        
        let planeHitTestResults = sceneView.hitTest(position, types: .existingPlaneUsingExtent)
        if let result = planeHitTestResults.first {
            
            let planeHitTestPosition = result.worldTransform.translation
            let planeAnchor = result.anchor
            
            // Return immediately - this is the best possible outcome.
            return (planeHitTestPosition, planeAnchor as? ARPlaneAnchor, true)
        }
        
        // -------------------------------------------------------------------------------
        // 2. Collect more information about the environment by hit testing against
        //    the feature point cloud, but do not return the result yet.
        
        var featureHitTestPosition: float3?
        var highQualityFeatureHitTestResult = false
        
        let highQualityfeatureHitTestResults = sceneView.hitTestWithFeatures(position, coneOpeningAngleInDegrees: 18, minDistance: 0.2, maxDistance: 2.0)
        
        if !highQualityfeatureHitTestResults.isEmpty {
            let result = highQualityfeatureHitTestResults[0]
            featureHitTestPosition = result.position
            highQualityFeatureHitTestResult = true
        }
        
        // -------------------------------------------------------------------------------
        // 3. If desired or necessary (no good feature hit test result): Hit test
        //    against an infinite, horizontal plane (ignoring the real world).
        
        if (infinitePlane && Settings.isDragOnInfinitePlanesEnabled) || !highQualityFeatureHitTestResult {
            
            if let pointOnPlane = objectPos {
                let pointOnInfinitePlane = sceneView.hitTestWithInfiniteHorizontalPlane(position, pointOnPlane)
                if pointOnInfinitePlane != nil {
                    return (pointOnInfinitePlane, nil, true)
                }
            }
        }
        
        // -------------------------------------------------------------------------------
        // 4. If available, return the result of the hit test against high quality
        //    features if the hit tests against infinite planes were skipped or no
        //    infinite plane was hit.
        
        if highQualityFeatureHitTestResult {
            return (featureHitTestPosition, nil, false)
        }
        
        // -------------------------------------------------------------------------------
        // 5. As a last resort, perform a second, unfiltered hit test against features.
        //    If there are no features in the scene, the result returned here will be nil.
        
        let unfilteredFeatureHitTestResults = sceneView.hitTestWithFeatures(position)
        if !unfilteredFeatureHitTestResults.isEmpty {
            let result = unfilteredFeatureHitTestResults[0]
            
            return (result.position, nil, false)
        }
        
        return (nil, nil, false)
    }
    
    ///
    /// Create a new 3D Object in the Augmented Reality view, using the supplied 3D Object Template, and select it.
    ///
    /// The object is placed exactly at the specified position.
    ///
    /// If there was a previously-selected 3D Object, it is confirmed and de-selected.
    ///
    func create(newObject: Object3D, loadingAt position: SCNVector3, in sceneView: ARSCNView) {
        if self.isSelectedObject {
            self.deselectSelectedObject()
        }
        
        guard let newVirtualObject = VirtualObject(object: newObject) else {
            Log.error{ "Failed to create a new Virtual Object for 3D Object Template named: \(newObject.name)" }
            return
        }
        
        self.virtualObjects[newVirtualObject.virtualObjectID] = newVirtualObject
        
        self.loadAndSelect(virtualObject: newVirtualObject, at: position, in: sceneView)
    }
    
    ///
    /// Create a new 3D Object in the Augmented Reality view, using the supplied 3D Object Template, and select it.
    ///
    /// The object is positioned in front of the camera by (at least) the specified amount, taking into account its boundingBox.
    ///
    /// If there was a previously-selected 3D Object, it is confirmed and de-selected.
    ///
    func create(newObject: Object3D, atAnAppropriateDistance appropriateDistanceClosure: @escaping AppropriateDistanceClosure, inFrontOfCamera cameraTransform: SCNMatrix4, onPlaneAt planeHeight: Float, in sceneView: ARSCNView) {
        if self.isSelectedObject {
            self.deselectSelectedObject()
        }
        
        guard let newVirtualObject = VirtualObject(object: newObject) else {
            Log.error{ "Failed to create a new Virtual Object for 3D Object Template named: \(newObject.name)" }
            return
        }
        
        self.virtualObjects[newVirtualObject.virtualObjectID] = newVirtualObject
        
        self.loadAndSelect(virtualObject: newVirtualObject, atAnAppropriateDistance: appropriateDistanceClosure, inFrontOfCamera: cameraTransform, onPlaneAt: planeHeight, in: sceneView)
    }
    
    ///
    /// Create a new 3D Object in the Augmented Reality view, using the supplied 3D Object Template, and select it.
    ///
    /// The object is positioned in front of the camera by (at least) the specified amount, taking into account its boundingBox.
    ///
    /// If there was a previously-selected 3D Object, it is confirmed and de-selected.
    ///
    func create(newObject: Object3D, atOrBehind crosshairsPosition: float3, camera cameraTransform: SCNMatrix4, in sceneView: ARSCNView) {
        if self.isSelectedObject {
            self.deselectSelectedObject()
        }
        
        guard let newVirtualObject = VirtualObject(object: newObject) else {
            Log.error{ "Failed to create a new Virtual Object for 3D Object Template named: \(newObject.name)" }
            return
        }
        
        self.virtualObjects[newVirtualObject.virtualObjectID] = newVirtualObject
        
        self.loadAndSelect(virtualObject: newVirtualObject, atOrBehind: SCNVector3(crosshairsPosition), camera: cameraTransform, in: sceneView)
    }

    ///
    /// Select an existing Object in the AR View.
    ///
    /// If there was a previously-selected 3D Object, it is confirmed and de-selected.
    ///
    func select(_ object: VirtualObject, in sceneView: ARSCNView) {
        if self.isSelectedObject {
            self.deselectSelectedObject()
        }
        
        self.currentSelectedObject = VirtualObjectWithState(object: object)
        
        if let gridNode = self.currentSelectedObject?.gridNode {
            sceneView.scene.rootNode.addChildNode(gridNode)
        }
        
        self.delegate?.virtualObjectManager(self, didSelect: object)
    }
    
    ///
    /// Delete the current selected 3D Object. The user has already been asked to confirm deletion.
    ///
    func deleteSelectedObject() {
        guard let selectedObject = self.currentSelectedObject else {
            Log.error{ "Asked to delete the current selected 3D Object when there is none!" }
            return
        }
        
        self.deselectSelectedObject()
        
        self.delete(virtualObject: selectedObject.object)
    }
    
    ///
    /// Delete the specified virtualObject (which may or may not be selected).
    ///
    func delete(virtualObject: VirtualObject) {
        if selectedObject?.virtualObjectID == virtualObject.virtualObjectID {
            self.deselectSelectedObject()
        }
        
        self.unload(virtualObject: virtualObject)
        self.virtualObjects[virtualObject.virtualObjectID] = nil

        self.delegate?.virtualObjectManager(self, didDelete: virtualObject)
        
        /***
        //
        //  Decrement the count of loaded virtual objects of this type. If this is the last such LoadedVirtualObject, unload it.
        //
        guard let existingLoadedVirtualObject = self.loadedVirtualObjects[selectedObject.object.object] else {
            Log.error{ "Should be impossible that we had a VirtualObject with 3D Object that wasn't registered in the loadedVirtualObjects[:] dictionary!" }
            return
        }

        synched(self) {
            existingLoadedVirtualObject.numberOfObjectsLoaded -= 1
        
            if existingLoadedVirtualObject.numberOfObjectsLoaded <= 0 {
                self.loadedVirtualObjects[selectedObject.object.object] = nil
            }
        }
        
        Async.background {
            existingLoadedVirtualObject.virtualObjectToClone.unload()
        }
        ***/
    }
    
    ///
    /// Deselect the current selected 3D Object (this should effectively confirm the object's current position and orientation).
    ///
    func deselectSelectedObject() {
        guard let selectedObject = self.currentSelectedObject else { return }
        
        self.currentSelectedObject = nil
        
        self.delegate?.virtualObjectManager(self, didDeselect: selectedObject.object)
    }
    
    ///
    /// - Returns: A reference to the (first) VirtualObject returned when performing a hit test against the `sceneView` at the specified point.
    ///
    func virtualObject(at point: CGPoint, in sceneView: ARSCNView) -> VirtualObject? {
        let hitTestOptions: [SCNHitTestOption: Any] = [.boundingBoxOnly: true]
        let hitTestResults: [SCNHitTestResult]      = sceneView.hitTest(point, options: hitTestOptions)
        
        return hitTestResults.lazy.compactMap { result in
            VirtualObject.isNodePartOfVirtualObject(result.node)
        }.first
    }
    
    ///
    /// Rotate the currently-selected 3D Object by the specified angle (expressed as a delta from the rotation stored in its current state). The state will be
    /// updated if the isRotationEnding parameter is true.
    ///
    func rotateSelectedObject(rotation: Float, isRotationEnding: Bool) {
        guard var selectedObject = self.currentSelectedObject else { return }
        
        let dampedRotation = rotation / 30.0
        
        //
        //  Apply the incoming rotation to the current selected object and its associated grid.
        //
        selectedObject.object.eulerAngles.y    -= dampedRotation
        selectedObject.gridNode?.eulerAngles.y -= dampedRotation
        
        //
        //  If this is the end of the rotation, we update our VirtualObjectWithState to reflect the "final" (for now) rotation value.
        //
        if isRotationEnding {
            selectedObject.rotation    = selectedObject.object.eulerAngles.y
            self.currentSelectedObject = selectedObject
        }
    }
    
    ///
    /// Move the currently-selected 3D Object by the specified relative distance (expressed as a percentage from its original State position). The direction is
    /// obtained from the sign of the percentage (negative is left or down) and the isMovementVertical flag. The cameraPosition may be changing, if the user is
    /// still moving. So we recompute the new position continuously, using the vector from the current camera position to the current position as our baseline.
    ///
    func moveSelectedObject(percentTranslation: Float, isMovementVertical: Bool, cameraPosition: SCNVector3) {
        guard let selectedObject = self.currentSelectedObject else { return }
        
        //
        //  Compute the current vector from the ground beneath the camera to the 3D object. Normalize this (and - if the movement is horizontal - take its orthogonal)
        //  and then use that normalized/orthogonal vector to generate a new position for the object its selection grid.
        //
        let cameraToObject           = cameraPosition - selectedObject.object.position
        let normalizedCameraToObject = SCNVector3(cameraToObject.x, 0.0, cameraToObject.z).normalized()
        
        let kDampingFactor : Float   = 1.0 / (isMovementVertical ? 30.0 : 40.0)
        
        let scaledDistance           = cameraPosition.distance(from: selectedObject.object.position) * percentTranslation * kDampingFactor
        let scaleVector              = SCNVector3(scaledDistance, 0, scaledDistance)
        
        let translationVector = isMovementVertical ? normalizedCameraToObject.scaled(by: scaleVector) : normalizedCameraToObject.orthogonalVector(around: .y, direction: .counterClockwise).scaled(by: scaleVector)
        
        selectedObject.object.position    = selectedObject.object.position + translationVector
        selectedObject.gridNode?.position = selectedObject.object.position
    }
    
    //
    //  ------------------------------------------------    P u b l i c    T y p e s    ------------------------------------------------
    //
    
    ///
    /// An **AppropriateDistanceClosure** is a function which takes the size of a virtual object, expressed as the object's footprintRadius, and returns an
    /// appopriate minimum distance at which the object should be placed to ensure that it is easily visible to the user.
    ///
    typealias AppropriateDistanceClosure = (/* footprintRadius */ Distance) -> (Distance)
    
    //
    //  ------------------------------------------------    P u b l i c    P r o p e r t i e s    ------------------------------------------------
    //
    
    weak var delegate: VirtualObjectManagerDelegate?
    
    var virtualObjectsArray : [VirtualObject] {
        get {
            return Array(self.virtualObjects.values)
        }
    }
    
    var selectedObject : VirtualObject? {
        get {
            return self.currentSelectedObject?.object
        }
    }
    
    var isSelectedObject : Bool {
        get {
            return currentSelectedObject != nil
        }
    }
    
}

extension SCNVector3 {
    
    init(_ vec: vector_float3) {
        self.init(vec.x, vec.y, vec.z)
    }
    
    func length() -> Float {
        return sqrtf(x * x + y * y + z * z)
    }

    
    mutating func normalize() {
        self = self.normalized()
    }
    
    func normalized() -> SCNVector3 {
        if self.length() == 0 {
            return self
        }
        
        return self / self.length()
    }
    
    static func positionFromTransform(_ transform: matrix_float4x4) -> SCNVector3 {
        return SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
    }
    
    func friendlyString() -> String {
        return "(\(String(format: "%.2f", x)), \(String(format: "%.2f", y)), \(String(format: "%.2f", z)))"
    }
    
    func dot(_ vec: SCNVector3) -> Float {
        return (self.x * vec.x) + (self.y * vec.y) + (self.z * vec.z)
    }
    
    func cross(_ vec: SCNVector3) -> SCNVector3 {
        return SCNVector3(self.y * vec.z - self.z * vec.y, self.z * vec.x - self.x * vec.z, self.x * vec.y - self.y * vec.x)
    }
}

func / (left: SCNVector3, right: Float) -> SCNVector3 {
    return SCNVector3Make(left.x / right, left.y / right, left.z / right)
}

extension SCNVector3 {
    enum Axis { case x, y, z }
    enum Direction { case clockwise, counterClockwise }
    func orthogonalVector(around axis: Axis, direction: Direction) -> SCNVector3 {
        switch axis {
        case .x: return direction == .clockwise ? SCNVector3(self.x, -self.z, self.y) : SCNVector3(self.x, self.z, -self.y)
        case .y: return direction == .clockwise ? SCNVector3(-self.z, self.y, self.x) : SCNVector3(self.z, self.y, -self.x)
        case .z: return direction == .clockwise ? SCNVector3(self.y, -self.x, self.z) : SCNVector3(-self.y, self.x, self.z)
        }
    }
}




