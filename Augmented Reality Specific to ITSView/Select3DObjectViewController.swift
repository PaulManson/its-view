//
//  Select3DObjectViewController.swift
//  ITS View
//
//  Created by Paul Manson on 25/05/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import Foundation
import UIKit

///
/// Communicate user selections back to our caller.
///
protocol Select3DObjectDelegate: class {
    
    func userDidSelect(object: Object3D)

}

class Select3DObjectViewController: UIViewController {
    
    private static let kRowHeight         =  45.0 as CGFloat
    private static let kRowWidth          = 250.0 as CGFloat
    private static let kCellContentHeight =  30.0 as CGFloat
    private static let kLabelWidth        = 200.0 as CGFloat
    
    private var tableView : UITableView!
    private var size      : CGSize!
    private var selectedVirtualObjectRow: Int = -1
    
    private var gallery : [Object3DManager.Object3DGalleryCategory] = []
    
    weak var delegate: Select3DObjectDelegate?
    
    init(size: CGSize) {
        super.init(nibName: nil, bundle: nil)
        self.size = size
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.gallery = Object3DManager.objectsGallery
        
        self.view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        tableView                  = UITableView(frame: CGRect(origin: CGPoint.zero, size: self.size), style: .plain)
        tableView.dataSource       = self
        tableView.delegate         = self
        tableView.backgroundColor  = UIColor.clear
        tableView.separatorEffect  = UIVibrancyEffect(blurEffect: UIBlurEffect(style: .light))
        tableView.separatorInset   = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.bounces          = false
        tableView.autoresizingMask = [UIView.AutoresizingMask.flexibleHeight]
        
        self.preferredContentSize = self.size
        
        self.view.addSubview(tableView)
        
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //
        //  Adjust the preferredContentSize to the ACTUAL contentSize, to ensure that the tableView scrolls to its extremes and doesn't just get clipped by
        //  the popover.
        //
        self.preferredContentSize = self.view.bounds.size
        self.tableView.frame      = CGRect(origin: CGPoint.zero, size: self.view.bounds.size)
    }
    
    private func object(at indexPath: IndexPath) -> Object3D {
        let category = self.gallery[indexPath.section.clamp(0 ..< self.gallery.count)]
        return category.objectsInCategory[indexPath.row.clamp(0 ..< category.objectsInCategory.count)]
    }
    
    class var popoverSize : CGSize {
        get {
            let rowsRequired = Object3DManager.objectsGallery.reduce(0) { (soFar, objectCategory) -> Int in
                return soFar + 1 + objectCategory.objectsInCategory.count
            }
            return CGSize(width: Select3DObjectViewController.kRowWidth, height: Select3DObjectViewController.kRowHeight * CGFloat(rowsRequired))
        }
    }
}


extension Select3DObjectViewController : UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.gallery.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gallery[section].objectsInCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = UITableViewCell()
        let label = UILabel    (frame: CGRect(x: 53, y: 10, width: Select3DObjectViewController.kLabelWidth        , height: Select3DObjectViewController.kCellContentHeight))
        let icon  = UIImageView(frame: CGRect(x: 15, y: 10, width: Select3DObjectViewController.kCellContentHeight,  height: Select3DObjectViewController.kCellContentHeight))
        
        /*
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle  = .none
        let vibrancyEffect   = UIVibrancyEffect(blurEffect: UIBlurEffect(style: .extraLight))
        let vibrancyView     = UIVisualEffectView(effect: vibrancyEffect)
        vibrancyView.frame   = cell.contentView.frame
        cell.contentView.insertSubview(vibrancyView, at: 0)
         vibrancyView.contentView.addSubview(label)
         vibrancyView.contentView.addSubview(icon)

         */
        cell.contentView.addSubview(label)
        cell.contentView.addSubview(icon)

        let object = self.object(at: indexPath)

        label.text = object.name
        icon.image = object.thumbnail?.getImage()

        if object.isSelectable {
            label.textColor     = .black
            icon.alpha          = 1.0
            cell.selectionStyle = .blue
        } else {
            label.textColor     = .lightGray
            icon.alpha          = 0.5
            cell.selectionStyle = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = UIColor.clear
    }
}

extension Select3DObjectViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedObject = self.object(at: indexPath)
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard selectedObject.isSelectable else { return }
        
        delegate?.userDidSelect(object: selectedObject)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let objectAtRow = self.object(at: indexPath)
        
        return objectAtRow.isSelectable
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let objectAtRow = self.object(at: indexPath)
        
        return objectAtRow.isSelectable ? indexPath : nil
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerViewSize     = CGSize(width: Select3DObjectViewController.kRowWidth, height: Select3DObjectViewController.kRowHeight)
        let view               = UIView(frame: CGRect(origin: CGPoint.zero, size: headerViewSize))
        view.backgroundColor   = .lightGray

        let lblTitle           = UILabel(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: Select3DObjectViewController.kLabelWidth, height: Select3DObjectViewController.kCellContentHeight)))
        lblTitle.text          = self.gallery[section.clamp(0 ..< self.gallery.count)].categoryName
        lblTitle.font          = Font.boldSystemFont(ofSize: 20.0)
        lblTitle.textAlignment = .center
        lblTitle.textColor     = .darkGray

        view.addSubview(lblTitle)
        
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lblTitle.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        lblTitle.widthAnchor.constraint  (equalToConstant: Select3DObjectViewController.kLabelWidth).isActive = true
        lblTitle.heightAnchor.constraint (equalToConstant: Select3DObjectViewController.kCellContentHeight).isActive = true
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Select3DObjectViewController.kRowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Select3DObjectViewController.kRowHeight
    }
}


extension Object3D {
    
    ///
    /// - Returns:  True if the object has both a thumbnail and a 3D model located here on this device.
    ///
    var isSelectable : Bool {
        get {
            guard let thumbnail = self.thumbnail, thumbnail.isLocalFileOnThisDevice else { return false }
            guard let model     = self.model,     model.isLocalFileOnThisDevice     else { return false }
            
            return true
        }
    }
    
}


