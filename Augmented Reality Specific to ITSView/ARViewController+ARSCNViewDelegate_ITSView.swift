/*
See LICENSE folder for this sample’s licensing information.

Abstract:
ARSCNViewDelegate interactions for `ViewController`.
*/

import ARKit
import PalaceSoftwareCore

extension ARViewController: ARSCNViewDelegate, ARSessionDelegate {
    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        switch anchor {
        case is ARPlaneAnchor:
            return SCNNode()
        default:
            Log.debug{ "GENERATING AN AROtherAnchorNode() FOR AN UNKNOWN ANCHOR!" }
            return AROtherAnchorNode()
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        if self.isDisplayingGetIntoARAnimation {
            updateGetIntoARAnimation()
            updateFocusSquare(isDisplayingFocusSquare: false)
        } else {
            updateFocusSquare(isDisplayingFocusSquare: self.isDisplayingFocusSquare)
        }
        
        //
        //  If light estimation is enabled, update the intensity of the model's lights and the environment map
        //
        if let lightEstimate = session.currentFrame?.lightEstimate {
            sceneView.scene.enableEnvironmentMapWithIntensity(lightEstimate.ambientIntensity / 40, queue: serialQueue)
        } else {
            sceneView.scene.enableEnvironmentMapWithIntensity(40, queue: serialQueue)
        }
        
        //
        //  If the PleaseWait spinner is visible, rotate it a little more.
        //
        if let pleaseWaitNode = self.pleaseWaitNode, let camera = session.currentFrame?.camera {
            pleaseWaitNode.update(camera: camera)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        switch anchor {
        case let planeAnchor as ARPlaneAnchor:
            serialQueue.async {
                self.planesManager?.addPlane(anchor: planeAnchor, with: node)    //  self.addPlane(node: node, anchor: planeAnchor)
                self.textManager.cancelScheduledMessage(forType: .planeEstimation)
                self.virtualObjectManager.checkIfObjectsShouldMoveOntoPlane(anchor: planeAnchor, planeAnchorNode: node)
            }
            
        default:
            break
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        switch anchor {
        case let planeAnchor as ARPlaneAnchor:
            serialQueue.async {
                self.planesManager?.updatePlane(anchor: planeAnchor, with: node)     //  self.updatePlane(anchor: planeAnchor, node: node)
                self.virtualObjectManager.checkIfObjectsShouldMoveOntoPlane(anchor: planeAnchor, planeAnchorNode: node)
            }

        default:
            break
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        switch anchor {
        case let planeAnchor as ARPlaneAnchor:
            serialQueue.async {
                self.planesManager?.removePlane(anchor: planeAnchor)     // self.removePlane(anchor: planeAnchor)
            }

        default:
            break
        }
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        
        textManager.showTrackingQualityInfo(for: camera.trackingState, autoHide: true)
        
        //
        //  Make a note of the current Tracking State so it can be polled elsewhere in the ARViewController
        //
        self.currentARCameraTrackingState = camera.trackingState
        
        switch camera.trackingState {
        case .notAvailable:
            Log.debug{ "TRACKING STATE WENT TO NOT AVAILABLE!" }
            textManager.escalateFeedback(for: camera.trackingState, inSeconds: 3.0)
        case .limited(let reason):
            Log.debug{ "TRACKING STATE WENT TO LIMITED AT \(Date.now). Reason = \(reason.description)" }
            textManager.escalateFeedback(for: camera.trackingState, inSeconds: 3.0)
        case .normal:
            Log.debug{ "TRACKING STATE WENT TO NORMAL  AT \(Date.now)" }
            textManager.cancelScheduledMessage(forType: .trackingStateEscalation)
        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        guard let arError = error as? ARError else { return }
        
        let nsError = error as NSError
        var sessionErrorMsg = "\(nsError.localizedDescription) \(nsError.localizedFailureReason ?? "")"
        if let recoveryOptions = nsError.localizedRecoveryOptions {
            for option in recoveryOptions {
                sessionErrorMsg.append("\(option).")
            }
        }
        
        let isRecoverable = (arError.code == .worldTrackingFailed)
        if isRecoverable {
            sessionErrorMsg += "\nYou can try resetting the session or quit the application."
        } else {
            sessionErrorMsg += "\nThis is an unrecoverable error that requires to quit the application."
        }
        
        displayErrorMessage(title: "We're sorry!", message: sessionErrorMsg, allowRestart: isRecoverable)
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        textManager.blurBackground()
        textManager.showAlert(title: "Session Interrupted", message: "The session will be reset after the interruption has ended.")
        
        Log.debug{ "SESSION WAS INTERRUPTED - REPORTED BY ARKIT" }
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        textManager.unblurBackground()
        session.run(currentConfiguration, options: [.resetTracking, .removeExistingAnchors])
        self.restartExperience()
        textManager.showMessage("RESETTING SESSION")
        
        Log.debug{ "SESSION INTERRUPTION ENDED - REPORTED BY ARKIT" }
    }
}

//
//  For debugging purposes, we display a yellow "Anchor Node" for each "Other" Anchor position. These might eventually become invisible within the ARScene,
//  but they still serve their purpose as placeholders.
//
class AROtherAnchorNode : SCNNode {
    
    override init() {
        super.init()
        
        let sphere = SCNSphere(radius: 0.1)
        sphere.firstMaterial?.diffuse.contents = Color.yellow
        
        self.addChildNode(SCNNode(geometry: sphere))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension ARCamera.TrackingState.Reason {
    
    var description : String {
        switch self {
        case .excessiveMotion:          return "EXCESSIVE MOTION!"
        case .initializing:             return "INITIALIZING!"
        case .insufficientFeatures:     return "INSUFFICIENT FEATURES!"
            
        #if swift(>=4.1)
            case .relocalizing:         return "RELOCALIZING"
        #endif
        }
    }
}


