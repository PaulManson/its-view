//
//  VirtualObject.swift
//  ITS View
//
//  Created by Paul Manson on 25/05/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import Foundation
import SceneKit
import Zip
import PalaceSoftwareCore
import PalaceSoftwareSpatial

///
/// **VirtualObject** objects appear in Augmented Reality and represent *instances* of a 3D Object template.
///
class VirtualObject : SCNReferenceNode, ReactsToScale {
    static private let kRootNodeName = "Virtual Object Root Node"
    
    public typealias VirtualObjectID = SwiftUUID
    
    private(set) var object          : Object3D
    private(set) var virtualObjectID : VirtualObjectID                      //  Unique ID for the purposes of unique comparison
    
    private      var particleScaleInfo: [String: Float] = [:]
    
    init?(object: Object3D, particleScaleInfo: [String: Float] = [:]) {
        self.object            = object
        self.virtualObjectID   = SwiftUUID()
        self.particleScaleInfo = particleScaleInfo
        
        guard let modelURL = Object3DModelManager.modelURL(for: object) else {
            Log.error{ "Failed to create a VirtualObject because the modelURL for the 3D Object named \(object.name) is NOT available!" }
            return nil
        }
        
        super.init(url: modelURL)
        
        self.name = VirtualObject.kRootNodeName
    }
    
    //
    //  Clone the existing VirtualObject and give it a new unique ID and set is position to (0,0,0)
    //
    static func clone(virtualObject: VirtualObject) -> VirtualObject {
        let clone             = virtualObject.clone()
        clone.virtualObjectID = SwiftUUID()
        clone.position        = SCNVector3(0,0,0)
        
        return clone
    }
    
    /**
    //
    //  Use average of recent virtual object distances to avoid rapid changes in object scale.
    //
    var recentVirtualObjectDistances = [Float]()
    **/
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public static func ==(lhs: VirtualObject, rhs: VirtualObject) -> Bool {
        return lhs.virtualObjectID == rhs.virtualObjectID
    }
    
    /*
    public override var hashValue: Int {
        get {
            return self.virtualObjectID.hashValue
        }
    }
    */
    
    override var hash: Int {
        var hasher = Hasher()
        hasher.combine(self.virtualObjectID)
        return hasher.finalize()
    }
    
    func reactToScale() {
        for (nodeName, particleSize) in self.particleScaleInfo {
            guard let node = self.childNode(withName: nodeName, recursively: true), let particleSystem = node.particleSystems?.first
                else { continue }
            particleSystem.reset()
            particleSystem.particleSize = CGFloat(scale.x * particleSize)
        }
    }
    
    ///
    /// Some virtualObjects have the Z axis pointing up; others have the Y Axis pointing up (as required by ARKit/SceneKit). Some virtualObjects are to metric
    /// scale (i.e. 1.0 means 1m); others are to inch scale. If this object isn't exactly as required by ARKit/SceneKit, we adjust it upon loading.
    ///
    override func load() {
        //
        //  Grab the memory consumed by the App before and after the call to .load(). This will help us determine the space required by this 3D Object.
        //
        let memoryBeforeLoad = Memory.megabytesUsed
        
        super.load()
        
        let memoryAfterLoad = Memory.megabytesUsed
        
        //
        //  Diagnostics ...
        //
        self.memoryDiagnostics(object: self.object, before: memoryBeforeLoad, after: memoryAfterLoad)
        
        //
        //  Adjust the orientation, if necessary
        //
        if !self.object.isModelYAxisUp {
            self.rotation = SCNVector4(1, 0, 0, -Float.pi / 2.0)
        }
        
        //
        //  Adjust the scale, if necessary
        //
        switch self.object.modelScale {
        case .inches:
            //
            //  Scale the model
            //
            let kInchesPerMeter = 25.4 / 1000
            self.scale = SCNVector3(kInchesPerMeter, kInchesPerMeter, kInchesPerMeter)
            
        case .meters:
            //
            //  Do nothing ... the model is already 1:1
            //
            break
        }
        
        //
        //  Adjust the .pivot property for the virtualObject, so it's centered on the centroid of its boundingBox. This makes it much easier to manipulate and - in
        //  particular - to rotate.
        //
        self.adjustPivotToCentroidOfBoundingBox()
    }
    
    //
    //  Adjust the .pivot property for the virtualObject, so it's centered on the centroid of its boundingBox. This makes it much easier to manipulate
    //  (and, in particular, to rotate).
    //
    private func adjustPivotToCentroidOfBoundingBox() {
        let (bbMin, bbMax) = self.boundingBox
        
        let centerX = (bbMax.x + bbMin.x) / 2.0
        let centerY = (bbMax.y + bbMin.y) / 2.0
        let centerZ = (bbMax.z + bbMin.z) / 2.0
        
        if self.object.isModelYAxisUp {
            self.pivot = SCNMatrix4MakeTranslation(centerX, 0, centerZ)
        } else {
            self.pivot = SCNMatrix4MakeTranslation(centerX, centerY, 0)
        }
    }
    
    private func memoryDiagnostics(object: Object3D, before: Float?, after : Float?) {
        Log.isVerbose = false
        Log.debug{ "+----------------------------------------------------------+" }
        Log.debug{ "| Loading Object named: \(object.name) " }
        
        defer {
            Log.debug{ "+----------------------------------------------------------+" }
            Log.isVerbose = true
        }
        
        guard let megabytesBeforeLoad = before, let megabytesAfterLoad = after else {
            Log.debug{ "| Either the before or after memory usage was unavailable! |" }
            return
        }

        Log.debug{ "| Memory before .load()          = \(megabytesBeforeLoad) MB |" }
        Log.debug{ "| Memory before .load()          = \(megabytesAfterLoad) MB |" }
        Log.debug{ "| Memory consumed by this Object = \(megabytesAfterLoad - megabytesBeforeLoad) MB |" }
    }
    
    //
    //  Return the virtualObject's footprintRadius, being the larger of the two horizontal boundingBox dimensions, adjusted to meter scale.
    //
    var footprintRadius : Distance {
        get {
            let (bbMin, bbMax) = self.boundingBox
            
            let radiusX = (bbMax.x - bbMin.x) / 2.0

            let radiusZ : Float
            if self.object.isModelYAxisUp {
                radiusZ = (bbMax.z - bbMin.z) / 2.0
            } else {
                radiusZ = (bbMax.y - bbMin.y) / 2.0
            }
            
            return Distance(meters: Double(max(radiusX, radiusZ) * self.scale.x))
        }
    }
    
    /*
    func loadModel() {
        /*
        //
        //  Make sure we have a local URL for this 3D Object's model
        //
        guard let modelMedia = self.object.model, modelMedia.isLocalFileOnThisDevice else {
            Log.error{ "Called with a nil model Media object, or the modelMedia's URL is not local to this device. 3D Object Model name: \(self.object.name)" }
            return
        }
        
        guard let modelURL = modelMedia.mediaURL else {
            Log.error{ "Called with a nil modelMedia URL for 3D Object Model name: \(self.object.name)" }
            return
        }
        
        //
        //  Unzip the 3D Model into a temporary folder.
        //
        let temporaryFolder = FileManager.tmpFolderURL.appendingPathComponent(SwiftUUID().UUIDString).appendingPathExtension("scnassets")
        guard FileManager.createFolder(temporaryFolder) else {
            Log.severe{ "Failed to create a temporary folder at \(temporaryFolder.path) for UNZIPping the 3D Object Model named: \(self.object.name)" }
            return
        }
        
        defer {
            //
            //  Regardless of how we exit from this point onwards, we need to delete that folder from /Tmp. We use a defer{} block because there are multiple exits.
            //
            if !FileManager.deleteFolder(temporaryFolder) {
                Log.severe{ "Failed to delete the temporary folder at \(temporaryFolder.path)!" }
            }
        }
        
        //
        //  Unzip the ZIP file into this temporary folder
        //
        do {
            try Zip.unzipFile(modelURL, destination: temporaryFolder, overwrite: true, password: nil, progress: { (progress) -> () in
                //  No need for any progress ... this should be fast!
            })
        } catch {
            Log.severe{ "Failed to UNZIP the file \(modelURL.path) into the folder \(temporaryFolder.path)" }
            return
        }
        
        //
        //  Identify the actual .SCN model in the temporary folder, and load it.
        //
        guard let sceneFile = self.sceneFile(in: temporaryFolder) else {
            Log.error{ "Failed to find a .scn file in the Unzipped folder for 3D Object named: \(self.object.name)!" }
            return
        }
        */
        
        let objectScene : SCNScene
        do {
            objectScene   = try SCNScene(url: super.referenceURL, options: nil)
        } catch let error {
            Log.severe{ "Failed to load the 3D Object Scene from \(sceneFile.path) for 3D Object named: \(self.object.name)! Error = \(error.localizedDescription)" }
            return
        }
        
        /*
         guard let virtualObjectScene = SCNScene(named: "\(modelName).\(fileExtension)", inDirectory: "Models.scnassets/\(modelName).scnassets") else {
         return
         }
         */
        
        let wrapperNode = SCNNode()
        
        for child in objectScene.rootNode.childNodes {
            child.geometry?.firstMaterial?.lightingModel = .physicallyBased
            child.movabilityHint = .movable
            wrapperNode.addChildNode(child)
        }
        
        //
        //  Because I have not been able to resolve the Z-Up (.dae) vs Y-Up (SceneKit) rotation issue with the imported .dae files, I have to rotate the model object
        //  unless it's specifically tagged as being "YAxisUp".
        //
        if !self.object.isModelYAxisUp {
            wrapperNode.rotation = SCNVector4(1,0,0,-Float.pi/2.0)
        }
        
        self.addChildNode(wrapperNode)
        
        self.isModelLoaded = true
    }
    
    func unloadModel() {
        guard self.isModelLoaded else { return }
        
        for child in self.childNodes {
            child.removeFromParentNode()
        }
        
        self.isModelLoaded = false
    }
    **/
    
    /*
     func translateBasedOnScreenPos(_ pos: CGPoint, instantly: Bool, infinitePlane: Bool) {
     
     guard let controller = viewController else {
     return
     }
     
     let result = controller.worldPositionFromScreenPosition(pos, objectPos: self.position, infinitePlane: infinitePlane)
     
     controller.moveVirtualObjectToPosition(result.position, instantly, !result.hitAPlane)
     }
     */
    
    
    static func isNodePartOfVirtualObject(_ node: SCNNode) -> VirtualObject? {
        if let virtualObjectRoot = node as? VirtualObject {
            return virtualObjectRoot
        }
        
        if node.parent != nil {
            return isNodePartOfVirtualObject(node.parent!)
        }
        
        return nil
    }
    
    
}

// MARK: - Protocols for Virtual Objects

protocol ReactsToScale {
    func reactToScale()
}

extension SCNNode {
    
    func reactsToScale() -> ReactsToScale? {
        if let canReact = self as? ReactsToScale {
            return canReact
        }
        
        if parent != nil {
            return parent!.reactsToScale()
        }
        
        return nil
    }
}

extension VirtualObject {
    
    ///
    /// - Returns: The supplied boundingBox (min,max) or boundingSphere (center) point, "corrected" so that it's expressed in terms of the ARKit/SceneKit axes (i.e. Z is up,
    ///            and scale is meters)
    ///
    func correctedBoundingPoint(_ corner: SCNVector3) -> SCNVector3 {
        if self.object.isModelYAxisUp {
            return corner.scaled(by: self.scale)
        } else {
            return SCNVector3(corner.x, corner.z, corner.y).scaled(by: self.scale)
        }
    }
    
}

