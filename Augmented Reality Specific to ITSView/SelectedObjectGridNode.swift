//
//  SelectedObjectGridNode.swift
//  ITS View
//
//  Created by Paul Manson on 30/05/18.
//  Copyright © 2018 com.equinox. All rights reserved.
//

import Foundation
import SceneKit
import PalaceSoftwareCore
import PalaceSoftwareSpatial

///
/// Given a VirtualObject, a SelectedObjectGridNode is a planar SCNNode that's sized to reflect the VirtualObject's bounding box, and positioned to sit directly beneath it.
///
class SelectedObjectGridNode : SCNNode {
    
    //
    // The following creates a SelectedObjectGridNode with a rectangular footprint. Replaced by the circular footprint ...
    //
    init(for object: VirtualObject) {
        let kCheckerboardPatternSize = 0.2 as Float //  20cm square
        
        super.init()
        
        //
        //  Create a plane with the same size as the virtual object's bounding box, projected to the ground.
        //
        let boundingBox = object.boundingBox
        
        //
        //  Use the PlaneRectangle class to derive the orientation of the boundingBox. Note that we need to ensure that the PlaneRectangle is created using ARKit/SceneKit axes,
        //  so the object's Y axis and scale must be taken into account BEFORE we construct the PlaneRectangle.
        //
        let correctedMin = object.correctedBoundingPoint(boundingBox.min)
        let correctedMax = object.correctedBoundingPoint(boundingBox.max)
        
        let rectangle = PlaneRectangle(min: correctedMin, max: correctedMax)
        
        //
        //  Expand the Plane by 20cm on all sides, to ensure that it's clearly visible around the base of the Object.
        //
        let expandedWidth  = rectangle.width  + (kCheckerboardPatternSize * 2.0)
        let expandedHeight = rectangle.height + (kCheckerboardPatternSize * 2.0)
        
        let plane = SCNPlane(width: CGFloat(expandedWidth), height: CGFloat(expandedHeight))
        
        let material = SCNMaterial()
        material.diffuse.contents = Image(named: "CheckerboardGrid")        //
        material.isDoubleSided    = true
        //
        //  The scale gives the number of times the image is repeated ARKit givest the width and height in meters, in this case we want to repeat the pattern each 20cm = 0.2m,
        //  so we divide the width/height to find the number of patterns we then round this so that we always have a clean repeat and not a truncated one
        //
        let scaleX = (Float(plane.width)  / kCheckerboardPatternSize).rounded()
        let scaleY = (Float(plane.height) / kCheckerboardPatternSize).rounded()
        //
        //  We then apply the scaling
        //
        material.diffuse.contentsTransform = SCNMatrix4MakeScale(scaleX, scaleY, 0)
        //
        //  Set repeat mode in both direction otherwise the pattern is stretched!
        //
        material.diffuse.wrapS = .repeat
        material.diffuse.wrapT = .repeat
        //
        //  Apply material
        //
        plane.materials = [material]

        /**
        //
        //  Position the Plane at the rectangle's center (we'll rotate it to the correct orientation later), positioned so it's at the bottom of the object.
        //
        //  IMPORTANT: The Rectangle's center is relative to the "origin" of the Bounding Box, so we need to apply this centering offset to the current position of the object,
        //             to get our grid into the correct position.
        //
        self.position = SCNVector3(object.position.x + rectangle.center.x, object.position.y, object.position.z - rectangle.center.z)
        **/
        
        //
        //  Position the Plane at the rectangle's center (we'll rotate it to the correct orientation later), positioned so it's at the bottom of the object.
        //
        //  NOTE: We previously had to offset the Plane to account for the Bounding Box not being intrinsically centered at (0,0,0). But because we adjust the .pivot
        //        property of each 3D Virtual Object just after loading it, we no longer need to offset the Plane.
        //
        self.position = object.position
        
        //
        //  Add this as a child of ourselves.
        //
        let planeNode = SCNNode(geometry: plane)
        planeNode.eulerAngles.x = .pi / 2                   //  Rotate the Plane around the x axis to make it a Horizontal Plane.
        
        //
        //  Rotate the Plane around the y axis to make it fit the boundingBox's orientation. Take any existing rotation of the virtualObject into account.
        //
        planeNode.eulerAngles.y = rectangle.orientation + object.eulerAngles.y
        
        self.addChildNode(planeNode)
    }
    
    /*
    //
    // The following creates a SelectedObjectGridNode with a rectangular footprint. Replaced by the circular footprint that encloses the base of the boundingBox ...
    //
    init(for object: VirtualObject) {
        super.init()
        
        //
        //  Create a plane with the same size as the virtual object's bounding box, projected to the ground.
        //
        let boundingBox = object.boundingBox
        
        //
        //  Use the PlaneRectangle class to derive the center and size of the boundingBox. Note that we need to ensure that the PlaneRectangle is created using ARKit/SceneKit axes,
        //  so the object's Y axis and scale must be taken into account BEFORE we construct the PlaneRectangle.
        //
        let correctedMin = correctedBoundingPoint(boundingBox.min, for: object)
        let correctedMax = correctedBoundingPoint(boundingBox.max, for: object)
        
        let rectangle = PlaneRectangle(min: correctedMin, max: correctedMax)
        
        let center      = rectangle.center
        let radius      = rectangle.boundingCircleRadius
        
        //
        //  Construct a flat disc with a radius that is [ 20% larger (maximum 1m larger) than ] the same as the bounding circle
        //
        /*
        let kExpansion     = 0.2 as Float
        let kMaxExpansion  = 1.0 as Float
        let expandRadiusBy = radius * kExpansion
        let expandedRadius = radius + (expandRadiusBy > kMaxExpansion ? kMaxExpansion : expandRadiusBy)
        */
        
        let kScaleFactor = 1.0 as Float
        let bezierPath = UIBezierPath()
        bezierPath.lineWidth = 0.1
        bezierPath.addArc(withCenter: CGPoint.zero, radius: CGFloat(radius * kScaleFactor), startAngle: 0.0, endAngle: 2.0 * .pi, clockwise: true)
        bezierPath.close()
            
        let circleShape = SCNShape(path: bezierPath, extrusionDepth: 0.0)
        
        //
        //  The subdivisionLevel property drives the "roundedness" of the circle. A value of 2 seems to deliver good results on "medium-sized" circles, which our scale factor
        //  will tend to deliver.
        //
        circleShape.subdivisionLevel = 3
        
        /**
        //
        //  Texture the disc with a checkerboard pattern
        //
        let material = SCNMaterial()
        material.diffuse.contents = Image(named: "CheckerboardGrid")
        material.isDoubleSided    = true
        //
        //  The scale gives the number of times the image is repeated ARKit givest the width and height in meters, in this case we want to repeat the pattern each 20cm = 0.2m,
        //  so we divide the width/height to find the number of patterns we then round this so that we always have a clean repeat and not a truncated one
        //
        let scale = ((radius * 2.0 * kScaleFactor) / 0.2).rounded()
        
        //
        //  We then apply the scaling
        //
        material.diffuse.contentsTransform = SCNMatrix4MakeScale(scale, scale, 0)
        
        //
        //  Set repeat mode in both direction otherwise the pattern is stretched!
        //
        material.diffuse.wrapS = .repeat
        material.diffuse.wrapT = .repeat
        
        //
        //  Apply material
        //
        circleShape.materials = [material]
        **/
        
        let node = SCNNode()
        node.geometry = circleShape
        node.geometry?.firstMaterial?.diffuse.contents  = Color.red
        node.geometry?.firstMaterial?.isDoubleSided     = true
            
        //
        //  Now shrink the circle by the same scale factor that we initially expanded it.
        //
        let inverseScaleFactor = 1.0 / kScaleFactor
        node.scale = SCNVector3(inverseScaleFactor, inverseScaleFactor, inverseScaleFactor)
            
        //
        //  Add a rotation of the Shape by 90 degrees around the X Axis (so it lies 2-dimensionally in the X-Z axis)
        //
        node.rotation = SCNVector4(1, 0, 0, Float(.pi / 2.0))
            
        //
        //  Add this node as a child of ourselves, and position us.
        //
        self.addChildNode(node)
        
        /*
        self.position = SCNVector3(position.x, position.y + Float(super.kFlatPointFeatureHeight), position.z)
        */
        
        /*
        let disc = SCNCylinder(radius: CGFloat(expandedRadius), height: 0.0)

        //
        //  Texture the disc with a checkerboard pattern
        //
        let material = SCNMaterial()
        material.diffuse.contents = Image(named: "CheckerboardGrid")
        material.isDoubleSided    = true
        //
        //  The scale gives the number of times the image is repeated ARKit givest the width and height in meters, in this case we want to repeat the pattern each 20cm = 0.2m,
        //  so we divide the width/height to find the number of patterns we then round this so that we always have a clean repeat and not a truncated one
        //
        let scale = (Float(disc.radius) / 0.2).rounded()

        //
        //  We then apply the scaling
        //
        material.diffuse.contentsTransform = SCNMatrix4MakeScale(scale, scale, 0)
        
        //
        //  Set repeat mode in both direction otherwise the pattern is stretched!
        //
        material.diffuse.wrapS = .repeat
        material.diffuse.wrapT = .repeat
        
        //
        //  Apply material
        //
        disc.materials = [material]
        **/
        
        //
        //  Position the Disc at the center of the boundingBox.
        //
        //  IMPORTANT: The boundingBox center is a delta from (0,0,0) so we need to apply that in reverse to object's position to get the correct position for our disc.
        //
        self.position = SCNVector3(object.position.x - center.x, object.position.y, object.position.z - center.z)
        
        Log.debug{ "*** Object's    pos = \(object.position.description)" }
        Log.debug{ "*** BoundingBox min = \(boundingBox.min.description) and max = \(boundingBox.max.description)" }
        Log.debug{ "*** Object    scale = \(object.scale)" }
        Log.debug{ "*** PlaneRectangle:" }
        Log.debug{ "***           width = \(rectangle.width)" }
        Log.debug{ "***          height = \(rectangle.height)" }
        Log.debug{ "***          center = \(rectangle.center.description)" }
        Log.debug{ "***     orientation = \(rectangle.orientation)" }
        Log.debug{ "***  boundingRadius = \(rectangle.boundingCircleRadius)" }
        Log.debug{ "*** Grid   position = \(self.position)" }
        Log.debug{ "*** Grid      scale = \(self.scale)" }
        let ourBoundingBox = self.boundingBox
        Log.debug{ "*** GridBoundingBox = min: \(ourBoundingBox.min.description) and max: \(ourBoundingBox.max.description)" }
        let ourBoundingSphere = self.boundingSphere
        Log.debug{ "*** GridBoundingSphere center is \(ourBoundingSphere.center.description) and radius: \(ourBoundingSphere.radius)" }
        /*
        //
        //  Add this as a child of ourselves.
        //
        let discNode = SCNNode(geometry: disc)
        discNode.eulerAngles.x = .pi / 2                   //  Rotate the Plane around the x axis to make it a Horizontal Plane.
        self.addChildNode(discNode)
        **/
    }
    */
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init() {
        super.init()
    }
    

}

extension SCNVector3 {
    
    func scaled(by scaleVector: SCNVector3) -> SCNVector3 {
        return SCNVector3(self.x * scaleVector.x, self.y * scaleVector.y, self.z * scaleVector.z)
    }
}


/*
//
//  A filled square lying flat on the ground.
//
class FilledSquarePointNode: ARModelNode {
    
    init(position: SCNVector3, color: Color, opacity: CGFloat, size width: Distance) {
        super.init()
        
        let bezierPath = UIBezierPath()
        bezierPath.lineWidth = 0.1
        
        let offsetInMeters = CGFloat(width.meters / 2.0)
        
        bezierPath.move   (to: CGPoint(x: -offsetInMeters, y: -offsetInMeters))
        bezierPath.addLine(to: CGPoint(x:  offsetInMeters, y: -offsetInMeters))
        bezierPath.addLine(to: CGPoint(x:  offsetInMeters, y:  offsetInMeters))
        bezierPath.addLine(to: CGPoint(x: -offsetInMeters, y:  offsetInMeters))
        bezierPath.close()
        
        let squareShape = SCNShape(path: bezierPath, extrusionDepth: super.kFlatPointFeatureThickness)
        
        let node = SCNNode()
        node.geometry = squareShape
        node.geometry?.firstMaterial?.diffuse.contents  = color.withAlphaComponent(opacity)
        node.geometry?.firstMaterial?.isDoubleSided     = true
        
        //
        //  Add a rotation of the Shape by 90 degrees around the X Axis (so it lies 2-dimensionally in the X-Z axis)
        //
        node.rotation = SCNVector4(1, 0, 0, Float(.pi / 2.0))
        
        //
        //  Add this node as a child of ourselves, and position us.
        //
        self.addChildNode(node)
        self.position = SCNVector3(position.x, position.y + Float(super.kFlatPointFeatureHeight), position.z)
    }
    
    override init() {
        super.init()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

//
//  A point node is a flat tube lying on the ground.
//
class PointNodeUsingTube: ARModelNode {
    
    init(position: SCNVector3, color: Color, opacity: CGFloat, diameter: Distance) {
        super.init()
        
        self.position = position
        
        //
        //  Create our tube as a flat circle with a small inner diameter (20cm max).
        //
        let innerRadius = diameter.meters > 0.5 ? 0.05 : 0.05
        let cylinder = SCNTube(innerRadius: CGFloat(innerRadius), outerRadius: CGFloat(diameter.meters / 2.0), height: super.kFlatPointFeatureThickness)
        cylinder.firstMaterial?.diffuse.contents = color.withAlphaComponent(opacity)
        
        //
        //  Add this as a child of ourselves.
        //
        self.addChildNode(SCNNode(geometry: cylinder))
    }
    
    override init() {
        super.init()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
*/

