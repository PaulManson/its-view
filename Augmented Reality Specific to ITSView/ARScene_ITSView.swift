//
//  ARScene.swift
//  ITS View
//
//  Created by Paul Manson on 6/08/17.
//  Copyright © 2017 e.quinox KG. All rights reserved.
//
//  This class manages an ARKit scene for the ITS View App. This is DRAMATICALLY cut down compared to the equivalent class for our other (GEMINI, ITS Geo) Apps.
//

import Foundation
import ARKit
import PalaceSoftwareSpatial

class ARScene {
    
    private static let kDefaultCameraHeight = Distance(meters: 1.6)
    private        let kUniqueKey           = String(describing: ARScene.self)
    
    //
    //  ------------------------------------------------    P r i v a t e    P r o p e r t i e s    ------------------------------------------------
    //
    
    //
    //  The scene for which we are managing AR model nodes.
    //
    private var sceneView : ARSCNView
    
    //
    //  Our sceneNode is the node that's added to the sceneView's rootNode, and all other nodes we add are added to the sceneNode. This enables us to instantly remove them
    //  (by removing the sceneNode), as required.
    //
    private var sceneNode : SCNNode
    
    //
    //  The camera's height above the ground (set initially to 1.6m, and subsequently updated whenever we recognize a horizontal plane beneath us. Setting this height will
    //  trigger a change to the .shift property, which will in turn adjust the vertical level of the model.
    //
    private(set) var cameraHeight : Distance = ARScene.kDefaultCameraHeight {
        didSet {
            //
        }
    }
    
    //
    //  Returns the transform of the AR camera.
    //
    var currentCameraTransform : simd_float4x4? {
        get {
            guard let pointOfView = self.sceneView.pointOfView else {
                return  nil
            }
            
            return pointOfView.simdTransform
        }
    }
    
    //
    //  Returns the position of the AR camera in world coordinates.
    //
    var currentCameraWorldPosition : SCNVector3? {
        get {
            guard let pointOfView = self.sceneView.pointOfView else {
                return  nil
            }
            
            return pointOfView.worldPosition
            /*
            let worldPosition = pointOfView.simdWorldPosition
            
            return SCNVector3(worldPosition.x, worldPosition.y, worldPosition.z)
            */
        }
    }
    
    //
    //  Returns the current attitude of the AR camera.
    //
    //  The pitch value will be 0.0 if the camera is horizontal around the x axis.
    //  The roll  value will be 0.0 if the camera is horizontal around the z axis
    //  The yaw   value is corrected using the model's rotation property, so will be an absolute Bearing value, provided the model has been accurately calibrated.
    //
    var currentCameraAttitude : Attitude? {
        get {
            guard let pointOfView = self.sceneView.pointOfView else {
                return  nil
            }
    
            let eulerAngles = pointOfView.eulerAngles
            
            let pitch = Angle(radians: Double(eulerAngles.x) + (Double.pi / 2.0),     minimumValue: -Double.pi, maximumValue: Double.pi, normalizeValue: false)
            let yaw   = Angle(radians: Double(eulerAngles.y) /* - self.rotation.radians */, minimumValue: -Double.pi, maximumValue: Double.pi, normalizeValue: false)
            let roll  = Angle(radians: Double(eulerAngles.z),                         minimumValue: -Double.pi, maximumValue: Double.pi, normalizeValue: false)
            
            let delta : String
            if let lastYaw = self._lastYaw {
                let deltaDegrees = yaw.degrees - lastYaw.degrees
                delta = deltaDegrees.format(".0")
            } else {
                delta = "?"
            }
            
            Log.debug{ "Yaw radians = \(eulerAngles.y),   degrees = \(yaw.degrees),   delta = \(delta) degrees" }
            
            self._lastYaw = yaw
            
            return Attitude(pitch: pitch, roll: roll, yaw: yaw)
        }
    }
    private var _lastYaw : Angle?
    
    //
    //  ------------------------------------------------    P r i v a t e    M e t h o d s    ------------------------------------------------
    //
    
    
    //
    //  ------------------------------------------------    P u b l i c    T y p e s    ------------------------------------------------
    //
    
    //
    //  ------------------------------------------------    P u b l i c    P r o p e r t i e s    ------------------------------------------------
    //
    
    //
    //  Returns the current (X,Y,Z) position of the camera in AR space, within the scene.
    //
    var currentScenePosition : SCNVector3? {
        get {
            guard let pointOfView = self.sceneView.pointOfView else {
                return  nil
            }
            
            return pointOfView.position
        }
    }
    
    //
    //  Returns the scenePosition of the point on the ground directly beneath the camera (i.e. the point that's .cameraHeight below the current scenePosition).
    //
    var currentPositionOnTheGroundDirectlyBeneathTheCamera : SCNVector3? {
        get {
            guard let cameraPosition = self.currentScenePosition else { return nil }
            
            return SCNVector3(x: cameraPosition.x, y: cameraPosition.y - Float(self.cameraHeight.meters), z: cameraPosition.z)
        }
    }
    
    //
    //  ------------------------------------------------    P u b l i c    M e t h o d s    ------------------------------------------------
    //
    
    //
    //  Create a Scene
    //
    init(in sceneView: ARSCNView, session: ARSession) {
        self.sceneView     = sceneView
        self.sceneNode     = SCNNode()
        self.sceneView.scene.rootNode.addChildNode(self.sceneNode)
    }

    deinit {
        
    }
    
    //
    //  Add a new 3D Object to the Scene.
    //
    func addObjectToScene(object: Object3D) {
        /**
        self.model.add(feature: feature)
        
        let newNodes = self.model.nodes(for: feature)
        for node in newNodes {
            self.addNodeToModel(node, feature: feature, isAlsoAnARAnchor: feature.geometry.featureDefinition.isUsedForCalibration)
        }
        
        if var existingNodesForFeatureDefinition = self.nodes[feature.geometry.featureDefinition] {
            existingNodesForFeatureDefinition[feature] = newNodes
            self.nodes[feature.geometry.featureDefinition] = existingNodesForFeatureDefinition
        } else {
            self.nodes[feature.geometry.featureDefinition] = [feature : newNodes]
        }
        ***/
    }
    
    //
    //  Remove an existing 3D Object from the Scene.
    //
    func removeObjectFromScene(object: Object3D) {
        /**
        self.model.remove(feature: feature)
        
        if var existingNodesForFeatureDefinition = self.nodes[feature.geometry.featureDefinition] {
            if let existingNodesForFeature = existingNodesForFeatureDefinition[feature] {
                for node in existingNodesForFeature {
                    node.removeFromParentNode()
                }
                
                //
                //  If this FeatureClass has any associated Anchor(s), we also remove them (TODO).
                //
                /***
                if feature.geometry.featureDefinition.isUsedForCalibration {
                    self.anchorManager?.removeAnchors(for: feature)
                }
                ***/
                
                existingNodesForFeatureDefinition[feature]     = nil
                self.nodes[feature.geometry.featureDefinition] = existingNodesForFeatureDefinition
            } else {
                Log.error{ "Cannot remove Feature from Model because no nodes exist for this Feature!" }
            }
        } else {
            Log.error{ "Cannot remove Feature from Model because no Features exist for this FeatureDefinition!" }
        }
        **/
    }
    
    /**
    //
    //  Update the visibility for all Model objects belonging to the specified FeatureClass
    //
    func updateVisibility(for featureDefinition: FeatureDefinition) {
        if let nodesForFeatureDefinition = self.nodes[featureDefinition] {
            for (_, nodes) in nodesForFeatureDefinition {
                for node in nodes {
                    node.isHidden = !featureDefinition.isVisible
                }
            }
        }
    }
    **/
    
    //
    //  Setting the cameraHeight triggers a change in the y element of the 3D shift applied to the calibrationNode's position.
    //
    func adjustCameraHeightAboveGroundLevel(cameraHeight: Distance) {
        self.cameraHeight = cameraHeight
    }

}

extension SCNVector3 : CustomStringConvertible {
    
    public static func +(lhs: SCNVector3, rhs: SCNVector3) -> SCNVector3 {
        return SCNVector3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z)
    }
    
    public static func -(lhs: SCNVector3, rhs: SCNVector3) -> SCNVector3 {
        return SCNVector3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z)
    }
    
    public var description : String {
        return "(\(Double(x).format(".2")), \(Double(y).format(".2")), \(Double(z).format(".2")))"
    }

    public var inverse : SCNVector3 {
        return SCNVector3(-self.x, -self.y, -self.z)
    }
    
    public static func *(lhs: SCNVector3, rhs: Float) -> SCNVector3 {
        return SCNVector3(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs)
    }

    public static func crossProduct(_ lhs: SCNVector3, _ rhs: SCNVector3) -> SCNVector3 {
        return SCNVector3(lhs.y * rhs.z - lhs.z * rhs.y, lhs.z * rhs.x - lhs.x * rhs.z, lhs.x * rhs.y - lhs.y * rhs.x)
    }

    public static func dotProduct(_ lhs: SCNVector3, _ rhs: SCNVector3) -> Float {
        return (lhs.x * rhs.x) + (lhs.y * rhs.y) + (lhs.z * rhs.z)
    }

}

extension Location : CustomStringConvertible {
    
    public var description : String {
        get {
            let altitudeString = self.altitude == nil ? "?" : self.altitude!.meters.format(".2")
            return "lat: \(self.latitude.asStringInDegreesMinutesAndSeconds()), lon = \(self.longitude.asStringInDegreesMinutesAndSeconds()), alt = \(altitudeString))"
        }
    }
}

