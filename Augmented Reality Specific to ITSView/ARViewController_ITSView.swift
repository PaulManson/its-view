//
//  ARViewController.swift
//  VisualMapper
//
//  Created by Paul Manson on 28/08/17.
//  Copyright © 2017 e.quinox KG. All rights reserved. 
//

import ARKit
import Foundation
import SceneKit
import UIKit
import Photos
import CoreMotion
import AudioToolbox
import PalaceSoftwareCore
import PalaceSoftwareSpatial
import ReplayKit

class ARViewController: UIViewController, ProgressHUDDelegate, UIPopoverPresentationControllerDelegate, UIGestureRecognizerDelegate, EasyTipViewDelegate, Select3DObjectDelegate {

    @IBOutlet weak var sceneView      : ARSCNView!
    
    @IBOutlet weak var messagePanel   : UIView!
    @IBOutlet weak var statusMessage  : UILabel!
    
    @IBOutlet weak var buttonBarView  : ButtonBarView!
    
    @IBOutlet weak var btnVideo       : AugmentedRealityButton!
    @IBOutlet weak var btnAddObject   : AugmentedRealityButton!
    @IBOutlet weak var btnSnapshot    : AugmentedRealityButton!
    
    @IBOutlet weak var btnBack        : UIButton!
    
    @IBOutlet weak var containerViewForLabel      : UIView!
    @IBOutlet weak var lblGetIntoAugmentedReality : UILabel!
    
    //
    //  The .configureForDataCollection() method MUST be called after the ARViewController has been instantiated, but before calling viewDidLoad().
    //  This method provides the project object to which we would attach any Snapshots. It also supplies us with a completion closure to call upon exit, if any Snapshots have been taken.
    //  This method allows our caller to put up a HUD while the task is being synchronized to the server.
    //
    private var project    : Place!
    private var callerName : String = ""
    
    typealias DataCollectionCompletionCallback = (/* isCallerRequiredToSynchronize: */ Bool, /* isCallerRequestedToExportData: */ Bool) -> ()
    private var dataCollectionCompletion : DataCollectionCompletionCallback? = nil
    
    //
    //  We increment the snapshotNumber each time the user takes a photo, and use this to name each of those Snapshots (which are saved to the "ITS View" Album).
    //
    private var snapshotNumber : Int = 0
    
    //
    //  If we take any snapshots or record and save any features, we want our caller to synchronize with the Perseids Cloud server upon exit from the AR View.
    //  Keep track of that here ...
    //
    var isCallerRequiredToSynchronize : Bool = false
    
    //
    //  The ARState state machine drives the visibility and enabled status of the individual UI elements as we progress through each state in turn.
    //
    enum ARState {
        case waitingToInitializeARKit
        case welcomingTheUser
        case welcomeExitButton
        case welcomeVideoButton
        case welcomeAddObjectButton
        case welcomeSnapshotButton
        case working
        case finished
    }
    
    private var stateLock = SynchedLock()
    
    var state : ARState = .waitingToInitializeARKit {
        didSet {
            //
            //  Adjust the UI elements to reflect this new state. Ensure that we don't commence a new state until the previous one has fully taken effect ...
            //
            synched(self.stateLock) {
                Async.onMainThread {
                    self.updateUI(for: self.state)
                }
            }
        }
    }
    
    // MARK: - ARKit Config Properties
    
    var screenCenter : CGPoint?
    
    //
    //  The Augmented Reality Scene
    //
    var scene : ARScene!

    //
    //  The session was originally a let constant; had to be changed to a var in order to invalide and replace it!
    //
    var session = ARSession()
    
    private let standardConfiguration: ARWorldTrackingConfiguration = {
        let configuration = ARWorldTrackingConfiguration()
        
        if #available(iOS 11.3, *) {
            configuration.planeDetection = Settings.isDetectingVerticalPlanes ? [.horizontal, .vertical] : [.horizontal]
        } else {
            configuration.planeDetection = .horizontal
        }

        //
        //  The following setting is incredibly important! The .gravityAndHeading setting ensures that while our initial position is still going to have an error,
        //  all the data we insert into the AR world will be north-aligned. However that alignment is only approximate, depending as it does upon the iPhone's compass. We
        //  initially switched between .gravityAndHeading and .gravity when the user manually wanted to adjust the orientation of their data. But this resulted in an
        //  instantaneous rotation of about 90 degrees (anticlockwise), for some reason.
        //
        //  So we now use .gravity always with ARKit, and we monitor the device's magnetic orientation ourselves, setting and adjusting the model's rotation manually.
        //
        configuration.worldAlignment = .gravity // .gravityAndHeading
        return configuration
    }()
    
    var currentConfiguration : ARWorldTrackingConfiguration!
    
    var textManager: StatusMessageManager!
    
    // MARK: - Queues
    
    let serialQueue = DispatchQueue(label: "com.equinox.VisualMapper.serialSceneKitQueue")
    
    // MARK: - View Controller Life Cycle
    
    //
    //  We now manage the camera's orientation ourselves, keeping ARKit in .gravity(only) mode throughout.
    //  This avoids the sudden (and inexplicable) jump to the left by 90 degrees which seems to occur (regardless of our physical orientation)
    //  when switching from .gravityAndHeading to .gravity(only).
    //
    let motionKit = MotionKit()
    var latestHeading : Bearing?                = nil
    var orientation   : UIInterfaceOrientation? = nil
    
    //
    //  When we've configured the iPad's interface orientation, we make a note of it and only re-configure when actually required.
    //
    var iPadUIConfiguredForOrientation : UIInterfaceOrientation? = nil
    
    internal var planeDetectionTimer   : Timer? = nil
    private  var positionOfLatestPlaneDetectedAtCrosshairs : float3? = nil  //  If isPlaneDetected then this is the height of the most-recently-detected Plane.
    
    private var planeDetectedAtHeight : Float? {
        get {
            return self.positionOfLatestPlaneDetectedAtCrosshairs?.y
        }
    }
    
    private  var isPlaneDetected : Bool {
        get {
            return self.planeDetectedAtHeight != nil
        }
    }
    
    //
    //  We maintain a record of the current Tracking State, so it can be polled elsewhere in the ARViewController, and so we can respond to changes in the Tracking State here
    //  in this class. This avoids some App-specific code squelching into the (otherwise Common) ARViewController+ARSCNViewDelegate.swift module.
    //
    internal var currentARCameraTrackingState : ARCamera.TrackingState? {
        didSet {
            //
            //  If we are in the .waitingForARKitInitialization and the ARKit state moves to .normal, we trigger the next state which is .startingStepOne
            //
            if let previousValue = oldValue {
                switch previousValue {
                case .normal:
                    break
                default:
                    //
                    //  The oldValue was NOT .normal
                    //
                    if let newValue = currentARCameraTrackingState {
                        switch newValue {
                        case .normal:
                            //
                            //  The newValue IS .normal. So if we were .waitingToInitializeARKit then it's time to move to .startingStepOne, UNLESS we haven't yet welcomed the user to
                            //  Augmented Reality, in which case we go through the welcome sequence.
                            //
                            if self.state == .waitingToInitializeARKit {
                                if Settings.isUserAlreadyWelcomedToAugmentedReality {
                                    self.state = .working
                                } else {
                                    self.state = .welcomingTheUser
                                }
                            }
                        default:
                            break
                        }
                    }
                }
            }
        }
    }
    
    //
    //  The virtualObjectManager manages VirtualObjects, which are instances of Object3D templates within AR Space.
    //
    var virtualObjectManager: VirtualObjectManager!
    
    //
    //  We put up a separate HUD during the Snapshot process
    //
    internal var snapshotHUD : ProgressHUD? = nil

    //
    //  The planesManager manages Horizontal and Vertical Planes on behalf of the AR session. At any point in time we may have many Planes recorded, some of which are
    //  being tracked and others of which maybe are not. At any point in time there is only ONE Plane of each type which may be locked (selected and displayed as a grid).
    //
    var planesManager: ARPlanesManager? = nil

    /**
    //
    //  Planes and Focus Squares
    //
    var planes = [ARPlaneAnchor: PlaneNode]()
    **/
    
    var focusSquare: FocusSquare?
    internal var isDisplayingFocusSquare : Bool = false          //  We only start displaying the FocusSquare when we get past the initial AR Welcome UI.

    //
    //  The animation we display when the user is first running the App and doesn't yet know how to move their iOS device to detect planes and start Mapping.
    //
    var getIntoARAnimation : GetIntoARAnimation?
    internal var isDisplayingGetIntoARAnimation : Bool {
        get {
            return getIntoARAnimation != nil
        }
    }
    
    //
    //  While recording video, we maintain an isRecordingVideo state flag. Toggling this will toggle visiblity of the ticking timer label and will start/stop the task which
    //  updates that label every second.
    //
    private var isRecordingVideo = false {
        didSet {
            Async.onMainThread {
                if self.isRecordingVideo {
                    self.btnVideo.isAnimating = true
                    self.startVideoTimer()
                } else {
                    self.btnVideo.isAnimating = false
                    self.stopVideoTimer()
                }
            }
        }
    }
    private var isExitingAfterPreviewingVideo = false
    
    //
    //  As soon as we are granted access to the user's PhotoLibrary, we start watching their "video" album(s).
    //  Any changes which occur while we are running get moved to the "ITS View" folder.
    //
    private var videosInVideoAlbumFetchResults : [PHFetchResult<PHAsset>] = []
    
    //
    //  When we are recording video, we display a ticking timer label just above the Video button.
    //
    internal var lblVideoTimer         : UILabel? = nil
    private var videoTickTimer         : Timer?   = nil
    private var secondsOfVideoRecorded : Int      = 0
    
    //
    //  When loading a large 3D Model, we display a spinning 3D "Please Wait" indicator.
    //
    internal var pleaseWaitNode : PleaseWaitNode? = nil
    
    //
    //  ---------------------    C a l i b r a t i o n    ---------------------
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.isExitingAfterPreviewingVideo = false
        
        self.currentConfiguration = self.standardConfiguration
        
        self.setupScene()
        
        Async.onMainThread {
            self.orientation = Device.interfaceOrientation
        }
        
        //
        //  Ensure we have regularly-updated headings from the compass/magnetometer.
        //
        if motionKit.isDeviceMotionAvailable {
            motionKit.getDeviceMotionObjectUsingTrueNorthReferenceFrame(interval: 1.0/10.0) { (deviceMotion: CMDeviceMotion) in
                //
                //  Update heading
                //
                if let orientation = self.orientation, let headingRadians = deviceMotion.headingCorrectedForTiltAndOrientation(orientation) {
                    self.latestHeading = headingRadians
                }
            }
        }
        
        //
        //  Add a tap gesture recognizer to the ARView. This is used to select an existing 3D Object.
        //
        self.createTapGestureRecognizer()
        
        //
        //  Our SwipeGestureRecognizer is actually a 2-axis PanGestureRecognizer which tells us when the user wants to pan the selected 3D Object (horizontal swipe) or
        //  when they want to zoom it (zoom out is swipe up, zoom in is swipe down).
        //
        self.createSwipeGestureRecognizer()

        //
        //  The RotationGestureRecognizer allows the user to rotate the selected 3D Object around its internal origin.
        //
        self.createRotationGestureRecognizer()

        //
        //  Configure the EasyTip "welcome" sequence in readiness ...
        //
        self.configureEasyTip()
        
        //
        //  After a moment for ARKit to initialize, create and present the AR Scene.
        //
        Async.main(after: 0.3) {
            self.prepareToCreateARScene()
        }
        
        self.isCallerRequiredToSynchronize = false
    }
    
    deinit {
        self.motionKit.stopDeviceMotionUpdates()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //
        //  Prevent the screen from being dimmed after a while.
        //
        UIApplication.shared.isIdleTimerDisabled = true
        
        if ARWorldTrackingConfiguration.isSupported {
            //
            //  Start the ARSession.
            //
            resetTracking()
        } else {
            //
            //  This device does not support 6DOF world tracking. But we should NEVER get here, as this App is limited (by its Info.plist) to iOS devices which support "arkit".
            //  Hence there is no need to localize these messages.
            //
            let sessionErrorMsg = "This app requires world tracking. World tracking is only available on iOS devices with A9 processor or newer. " +
            "Please quit the application."
            displayErrorMessage(title: "Unsupported platform", message: sessionErrorMsg, allowRestart: false)
        }
        
        //
        //  Start a task which checks frequently whether there is a plane detected at the screen center (i.e. beneat the central crosshairs, if displayed). When such a plane
        //  is detected, we use the vertical distance to this plan to determine our cameraHeight. This task also handles crosshairs visibility when the crosshairs are displayed.
        //
        self.startPlaneDetectionTask()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        session.pause()
        
        //
        //  Stop the plane detection task, which will be restarted if we again call viewDidAppear().
        //
        self.stopPlaneDetectionTask()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //
        //  Update our record of the current interface orientation
        //
        if Thread.isMainThread {
            self.orientation = Device.interfaceOrientation
        }
        
        self.updateScreenCenter()
        
        //
        //  On iPad, we don't want the buttonBar to span the width of the screen. Instead, we arrange the buttons in a cluster around the iPad's Home button.
        //
        if Device.isiPad {
            self.repositionButtonBar()
        }
    }
    
    //
    //  As a class function for the entire App, provide the supported and preferred Interface Orientations
    //
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            return RotatingUIViewController.supportedInterfaceOrientationsForApplication()
        }
    }
    
    //
    //  ------------------------------------------------    S t a t e - B a s e d    U I   M e t h o d s    ------------------------------------------------
    //
    
    //
    //  Update the various UI elements to reflect the current state.
    //
    private func updateUI(for state: ARState) {
        switch state {
        case .waitingToInitializeARKit:
            //
            //  All Buttons start off disabled (and in many cases, invisible)
            //
            self.btnBack.isEnabled = false
            self.updateSnapshotButton(isEnabled: false, isHiddenIfNotEnabled: false)
            self.updateVideoButton(isEnabled: false, isHiddenIfNotEnabled: false)
            self.updateAddEndFeatureButton(isEnabled: false, isHiddenIfNotEnabled: false)
            
        case .welcomingTheUser, .welcomeExitButton, .welcomeSnapshotButton, .welcomeAddObjectButton, .welcomeVideoButton:
            //
            //  All of the buttons are visible; none is enabled.
            //
            self.btnBack.isEnabled = false
            self.updateVideoButton(isEnabled: false, isHiddenIfNotEnabled: false)
            self.updateSnapshotButton(isEnabled: false, isHiddenIfNotEnabled: false)
            self.updateAddEndFeatureButton(isEnabled: false, isHiddenIfNotEnabled: false)
            
            //
            //  Present the welcoming "Easy Tip" corresponding to the new state. Dismissal of the popup Tip will trigger a step to the next state.
            //
            self.displayEasyTip(for: state)
            
        case .working:
            //
            //  If we get this far, we must have walked the user through the "Welcome to Augmented Reality" sequence of EasyTips already.
            //
            Settings.isUserAlreadyWelcomedToAugmentedReality = true

            self.btnBack.isEnabled       = true
            self.updateVideoButton(isEnabled: true)
            self.updateSnapshotButton(isEnabled: true)
            self.updateAddEndFeatureButton(isEnabled: true)
            
            if PHPhotoLibrary.authorizationStatus() != .notDetermined {
                //
                //  Add the "Get Into AR" Animation, which displays until we detect the first horizontal Plane (and for at least 1.5 seconds, to avoid flicker)
                //
                self.setupGetIntoARAnimation()
            } else {
                //
                //  The last thing we do - if not already granted - is to request access to the user's Photo library. We use this access to save Snapshots and also to move recorded
                //  Videos to our "ITS View" folder.
                //
                self.seekPhotoLibraryAccessIfNotAlreadyGranted(ifOrWhenAccessIsGranted: {
                    //
                    //  Early in the AR setup process, we request access to the user's Photo Library, if this hasn't already been granted on a previous occasion.
                    //
                    //  If granted, we register to be notified whenever a Video is saved to the Photo Library WHILE WE ARE RUNNING. If this happens, it will be due to our recording using
                    //  ReplayKit, and we will want to move that new Video into our "ITS View" folder.
                    //
                    self.registerObserverForPhotoLibrary()
                    
                    //
                    //  Add the "Get Into AR" Animation, which displays until we detect the first horizontal Plane (and for at least 1.5 seconds, to avoid flicker)
                    //
                    self.setupGetIntoARAnimation()
                })
            }

        case .finished:
            //
            //  Time to leave ...
            //
            self.exit()
        }
    }
    
    //
    //  Display a Popup Alert explaining how this Step of the ITS Data Collection process works. When the user taps "OK", change to the specified nextState.
    //
    private func showStepMessage(title: String, message: String, nextState: ARState) {
        let okAction = Alert.Action(actionTitle: LM.localizedString("button_ok", comment: "The standard OK for dialog boxes")) {
            self.state = nextState
        }
        
        Alert.alertWithActions(title, message: message, iconName: nil, actions: [okAction], lastOptionIsCancel: false, presentedOn: self)
    }
    
    //
    //  ------------------------------------------------    E n d    O f    S t a t e - B a s e d    U I   M e t h o d s    ------------------------------------------------
    //
    
    func setupScene() {
        //
        //  Synchronize updates via the `serialQueue`.
        //
        virtualObjectManager = VirtualObjectManager(updateQueue: serialQueue)
        virtualObjectManager.delegate = self
        
        setupUIControls()
        
        //
        //  Set up scene view
        //
        sceneView.setup()
        sceneView.delegate = self
        sceneView.session  = session
        
        session.delegate   = self
        
        //
        //  Create the planesManager to manage horizontal and vertical planes.
        //  This takes the AR Session as a parameter, along with ourselves as its delegate.
        //
        self.planesManager = ARPlanesManager(session: self.session, delegate: self)

        sceneView.scene.enableEnvironmentMapWithIntensity(25, queue: serialQueue)
        
        setupFocusSquare()
        
        Async.onMainThread {
            self.updateScreenCenter()
        }
    }
    
    func setupUIControls() {
        //
        //  Localize the button labels.
        //
        self.btnVideo.label     = LM.localizedString("button_video",       comment: "Record a video of the screen.")
        self.btnAddObject.label = LM.localizedString("button_add_feature", comment: "Add (record) a Feature.")
        self.btnSnapshot.label  = LM.localizedString("button_snapshot",    comment: "Take a snapshot (photo of the screen).")
        
        //
        //  Assign the "Tap" handlers to each of the ARButtons
        //
        self.btnVideo.buttonWasTappedCallback = { (_) in
            if self.isRecordingVideo {
                self.stopRecordingVideo(thenExit: false)
            } else {
                self.startRecordingVideo()
            }
        }
        
        self.btnAddObject.buttonWasTappedCallback = { (addObjectButton) in
            self.btnAddPressed(addObjectButton)
        }
        
        self.btnAddObject.buttonWasTappedAndHeldCallback = { (addObjectButton) in
            self.selectObjectToAdd(button: addObjectButton)
        }
        
        self.btnSnapshot.buttonWasTappedCallback = { (snapshotButton) in
            self.btnSnapshotPressed(snapshotButton)
        }
        
        //
        //  Set appearance of message output panel
        //
        messagePanel.layer.cornerRadius = 3.0
        messagePanel.clipsToBounds      = true
        messagePanel.isHidden           = true
        statusMessage.text              = ""
        
        //
        //  Configure the BackButton, which is always enabled.
        //
        self.btnBack.isEnabled = true
        
        textManager = StatusMessageManager(viewController: self)
    }
    
    
    // MARK: - Gesture Recognizers
    
    //
    //  Create a Vertical and Horizontal Swipe Gesture Recognizer. This is actually implemented as a PanGestureRecognizer, because SwipeGestureRecognizers are discrete
    //  and we need a continuous gesture to respond to. At any point, this will move the selected VirtualObject either horizontally or vertically.
    //
    //  For Horizontal Swipes, the amount of the pan will be a linear percentage of the horizontal screen width.
    //  For Vertical   Swipes, the amount of the pan will be a log-scale proportion of the distance from the object to the camera, or from the object to "the horizon".
    //
    private func createSwipeGestureRecognizer() {
        let swipeGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(ARViewController.swipeDetected(_:)))
            
        swipeGestureRecognizer.delegate               = self
        swipeGestureRecognizer.minimumNumberOfTouches = 1
        swipeGestureRecognizer.maximumNumberOfTouches = 1
            
        Async.main{
            self.sceneView.addGestureRecognizer(swipeGestureRecognizer)
        }
    }
        
    @objc func swipeDetected(_ swipeGestureRecognizer: UIPanGestureRecognizer) {
            
        switch swipeGestureRecognizer.state {
        case .began:
            guard virtualObjectManager.isSelectedObject else {
                self.playFailureSound()
                swipeGestureRecognizer.state = .cancelled
                return
            }
            
        case .cancelled, .failed:
            break
            
        case .possible:
            break
            
        case .changed:
            guard let cameraPosition = self.scene.currentPositionOnTheGroundDirectlyBeneathTheCamera else {
                self.playFailureSound()
                swipeGestureRecognizer.state = .cancelled
                return
            }

            let translation = swipeGestureRecognizer.translation(in: self.sceneView)
            
            let isMovementVertical = abs(translation.y) > abs(translation.x)
            let percentOfScene     = isMovementVertical ? (translation.y / self.sceneView.bounds.height) : (translation.x / self.sceneView.bounds.width)
            self.virtualObjectManager.moveSelectedObject(percentTranslation: Float(percentOfScene), isMovementVertical: isMovementVertical, cameraPosition: cameraPosition)
            
        case .ended:
            break
        }
    }
    
    //
    //  Create a Rotation Gesture Recognizer, with which the user can rotate the selected 3D Object.
    //
    private func createRotationGestureRecognizer() {
        let rotationGestureRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(ARViewController.rotationDetected(_:)))
        
        rotationGestureRecognizer.delegate = self
        
        Async.main{
            self.sceneView.addGestureRecognizer(rotationGestureRecognizer)
        }
    }
    
    @objc func rotationDetected(_ rotationGestureRecognizer: UIRotationGestureRecognizer) {
        
        switch rotationGestureRecognizer.state {
        case .began:
            guard virtualObjectManager.isSelectedObject else {
                self.playFailureSound()
                rotationGestureRecognizer.state = .cancelled
                return
            }
            
        case .cancelled, .failed:
            break
            
        case .possible:
            break
            
        case .changed:
            self.virtualObjectManager.rotateSelectedObject(rotation: Float(rotationGestureRecognizer.rotation), isRotationEnding: false)
            
        case .ended:
            self.virtualObjectManager.rotateSelectedObject(rotation: Float(rotationGestureRecognizer.rotation), isRotationEnding: true)
            break
        }
    }
    
    /**
    private var startedSwipeGestureAt : CGPoint? = nil
    
    // var currentGesture: Gesture?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard virtualObjectManager.isSelectedObject else {
            guard let tappedPoint = touches.first?.location(in: self.sceneView) else {
                self.playFailureSound()
                return
            }

            if let newSelectedObject = virtualObjectManager.virtualObject(at: tappedPoint, in: self.sceneView) {
                virtualObjectManager.select(newSelectedObject)
            } else {
                self.playFailureSound()
            }
            
            return
        }
        self.startedSwipeGestureAt = touches.first?.location(in: self.sceneView)
        
        /*
        virtualObjectManager.reactToTouchesBegan(touches, with: event, in: self.sceneView)
        
        displayVirtualObjectTransform()
        */
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        /*
        virtualObjectManager.reactToTouchesMoved(touches, with: event)

        displayVirtualObjectTransform()
        */
        guard let startLocation = self.startedSwipeGestureAt else { return }
        guard let newLocation   = touches.first?.location(in: self.sceneView) else {
            Log.debug{ "NO NEW LOCATION! WHY??????" }
            return
        }
        
        let verticalMovement   = newLocation.y - startLocation.y
        let horizontalMovement = newLocation.x - startLocation.x
        
        if abs(verticalMovement) > abs(horizontalMovement) {
            let percentOfScene = Int((verticalMovement / self.sceneView.bounds.height) * 100)
        
            Log.debug{ "MOVED \(verticalMovement) VERTICALLY which is \(percentOfScene)% of the scene" }
        } else {
            let percentOfScene = Int((horizontalMovement / self.sceneView.bounds.width) * 100)
            
            Log.debug{ "MOVED \(horizontalMovement) HORIZONTALLY, which is \(percentOfScene)% of the scene" }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        // virtualObjectManager.reactToTouchesEnded(touches, with: event)
        
        if self.startedSwipeGestureAt == nil {
            Alert.simpleAlertWithOK("ERROR!", message: "SELECT AN OBJECT FIRST, THEN SWIPE UP/DOWN TO MOVE THEM BACK/FORWARD!", iconName: nil, presentedOn: self, completion: nil)
        }
        self.startedSwipeGestureAt = nil
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        // virtualObjectManager.reactToTouchesCancelled(touches, with: event)
        self.startedSwipeGestureAt = nil
    }
    **/
    
    /**
    func displayVirtualObjectTransform() {
        
        guard let object = virtualObjectManager.currentSelectedObject?.object, let cameraTransform = session.currentFrame?.camera.transform else {
            return
        }
        
        // Output the current translation, rotation & scale of the virtual object as text.
        
        let cameraPos = SCNVector3.positionFromTransform(cameraTransform)
        let vectorToCamera = cameraPos - object.position
        
        let distanceToUser = vectorToCamera.length()
        
        var angleDegrees = Int(((object.eulerAngles.y) * 180) / Float.pi) % 360
        if angleDegrees < 0 {
            angleDegrees += 360
        }
        
        let distance = String(format: "%.2f", distanceToUser)
        let scale = String(format: "%.2f", object.scale.x)
        textManager.showMessage("Dist: \(distance)m - Rot: \(angleDegrees)° - Scale: \(scale)x")
    }
    
    func moveVirtualObjectToPosition(_ pos: SCNVector3?, _ instantly: Bool, _ filterPosition: Bool) {
        
        guard let newPosition = pos else {
            textManager.showMessage("CANNOT PLACE OBJECT\nTry moving left or right.")
            //
            //  Reset the content selection in the menu only if the content has not yet been initially placed.
            //
            if !virtualObjectManager.isSelectedObject {
                resetVirtualObject()
            }
            return
        }
        
        if instantly {
            setNewVirtualObjectPosition(newPosition)
        } else {
            updateVirtualObjectPosition(newPosition, filterPosition)
        }
    }
    */
    
    func configureForDataCollection(project: Place, callerName: String?, completionCallback: @escaping DataCollectionCompletionCallback) {
        self.project                  = project
        self.callerName               = callerName ?? ""
        self.dataCollectionCompletion = completionCallback
    }
    
    //
    //  ---------------------    A R S c e n e    M a n a g e m e n t    ---------------------
    //
    
    //
    //  Prepare to display an ARScene.
    //
    private func prepareToCreateARScene() {
        
        createAugmentedRealityScene()
        
        //
        //  If ARKit has completed initialization, we go to .working, unless this is the first time the user has visited the Augmented Reality view. In that case,
        //  we walk them through a Welcome sequence of EasyTips before going to .working
        //
        if let currentTrackingState = self.currentARCameraTrackingState {
            switch currentTrackingState {
            case .normal:
                if Settings.isUserAlreadyWelcomedToAugmentedReality {
                    self.state = .working
                } else {
                    self.state = .welcomingTheUser
                }
            case .notAvailable, .limited(_):
                self.state = .waitingToInitializeARKit
            }
        } else {
            self.state = .waitingToInitializeARKit
        }
    }
    
    //
    //  Create the AR Scene with ZERO model objects (much simpler than GEMINI or ITS_GEO APP). Still feed the scene the iOS device's current heading.
    //
    func createAugmentedRealityScene() {
        //
        //  The call to .resetTracking() resets the current camera location to (0,0,0) which is important, as that's the basis for our calculations of relative coordinates
        //  for the OSM objects we are about to create in the ARScene.
        //
        self.resetTracking()
        
        //
        //  Now create the scene, providing just the session
        //
        self.scene = ARScene(in: self.sceneView, session: self.session)
    }
    
    //
    //  ---------------------    E a s y T i p V i e w D e l e g a t e     ---------------------
    //
    
    //
    //  Whether dismissed by timer or by a tap from the user, we want to step on to the next EasyTip view.
    //
    func easyTipViewDidDismiss(_ tipView : EasyTipView) {
        switch self.state {
        case .welcomingTheUser:
            self.state = .welcomeExitButton
            
        case .welcomeExitButton:
            self.state = .welcomeVideoButton
            
        case .welcomeVideoButton:
            self.state = .welcomeAddObjectButton
            
        case .welcomeAddObjectButton:
            self.state = .welcomeSnapshotButton
            
        case .welcomeSnapshotButton:
            self.state = .working
            
        default:
            //
            //  This should be impossible. However, if it ever were to happen we'd want to go to .working
            //
            self.state = .working
        }
    }
    
    private func configureEasyTip() {
        var preferences = EasyTipView.Preferences()
        
        preferences.drawing.font            = UIFont(name: "Futura-Medium", size: 18)!
        preferences.drawing.cornerRadius    = 10.0
        preferences.drawing.arrowHeight     = 10.0
        preferences.drawing.arrowWidth      = 20.0
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = App.signatureColor
        
        preferences.positioning.maxWidth    = 300.0
        
        EasyTipView.globalPreferences = preferences
    }
    
    private func displayEasyTip(for state: ARState) {        
        switch self.state {
        case .welcomingTheUser:
            showEasyTip(view: self.messagePanel,  withinSuperview: self.view, tip: "itsview_welcome_to_ar_introduction")
            
        case .welcomeExitButton:
            showEasyTip(view: self.btnBack,       withinSuperview: self.view, tip: "itsview_welcome_to_ar_exit_button")

        case .welcomeAddObjectButton:
            showEasyTip(view: self.btnAddObject,  withinSuperview: self.view, tip: "itsview_welcome_to_ar_add_object_button")

        case .welcomeVideoButton:
            showEasyTip(view: self.btnVideo,      withinSuperview: self.view, tip: "itsview_welcome_to_ar_video_button")

        case .welcomeSnapshotButton:
            showEasyTip(view: self.btnSnapshot,   withinSuperview: self.view, tip: "itsview_welcome_to_ar_snapshot_button")

        default:
            //
            //  This should be impossible. However, if it ever were to happen we'd probably want to go to .working
            //
            self.state = .working
            return
        }
    }
    
    private func showEasyTip(view: UIView, withinSuperview superview: UIView? = nil, tip tipToken: String) {
        let localizedTipText  = LM.localizedString(tipToken, comment: "").withApplicationNameInjected
        let tapHereToContinue = LM.localizedString("welcome_tap_here_to_continue", comment: "")
        
        let tipText = "\(localizedTipText)\n\n\n\(tapHereToContinue)"
        
        EasyTipView.show(animated: true, forView: view, withinSuperview: superview, text: tipText, preferences: EasyTipView.globalPreferences, delegate: self)
    }
    
    //
    //  A number of factors determine whether individual buttons on the AR View are enabled, or not.
    //
    func updateARViewButtons() {
        //
        //  For the ITS Geo Solutions App, the Add/End Feature and Snapshot buttons are always enabled.
        //
        self.updateAddEndFeatureButton(isEnabled: true, isHiddenIfNotEnabled: false)
        self.updateSnapshotButton     (isEnabled: true, isHiddenIfNotEnabled: false)
        self.updateVideoButton        (isEnabled: true, isHiddenIfNotEnabled: false)
    }
    
    internal func exit() {
        //
        //  Ensure that we no longer get notifications if a Video has been recorded ...
        //
        PHPhotoLibrary.shared().register(self)
        
        //
        //  Always call the completion closure, asking our caller to (optionally) synchronize if appropriate and export data (if there is/was any).
        //
        if let completion = self.dataCollectionCompletion {
            completion(self.isCallerRequiredToSynchronize, self.isCallerRequiredToSynchronize)
        }
        
        //
        //  Reset ARView
        //
        self.project                       = nil
        self.planesManager                 = nil
        self.dataCollectionCompletion      = nil
        self.isCallerRequiredToSynchronize = false

        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSnapshotPressed(_ sender: Any) {
        //
        //  When an object is selected, the Snapshot button changes to a "Trash" button.
        //
        if self.virtualObjectManager.isSelectedObject {
            self.virtualObjectManager.deleteSelectedObject()
            self.updateARViewButtons()
            return
        }

        //
        //  Check firstly if this functionality is supported for the user's account status
        //
        if !AccountManager.capabilityIsAvailable(.takeASnapshot) {
            AccountManager.suggestActionToEnableCapability(.takeASnapshot, onViewController: self)
            return
        }
        
        takeSnapshot()
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        //
        //  If the user is recording a video, we ask if they want to discard or preview/save this first. We can proceed from discard to exit, but we can't automatically proceed from
        //  preview/save to exit.
        //
        if self.isRecordingVideo {
            self.stopRecordingVideo(thenExit: true)
        } else {
            self.exitIfTheUserConfirms()
        }
    }
    
    private func exitIfTheUserConfirms() {
        //
        //  If not, we still ask the user to confirm their wish to exit the AR View. We don't want them doing so accidentally, as they will lose their calibration.
        //
        let title   = LM.localizedString("title_please_confirm",             comment: "Title for a question asking the user to confirm an action (e.g. a deletion).")
        let message = LM.localizedString("text_exit_augmented_reality_view", comment: "Ask the user to confirm that they wish to exit the Augmented Reality view (to ensure they don't do so accidentally, losing their calibration, etc).")
        
        let exitAction = Alert.Action(actionTitle: LM.localizedString("menu_exit", comment: "Menu item to exit the current form or context."), isDestructive: true) {
            self.exit()
        }
        
        Alert.alertWithActions(title, message: message, iconName: nil, actions: [exitAction], lastOptionIsCancel: true, presentedOn: self)
    }

    //
    //  If we are already placing an object, discard it. If not, allow the user to select a 3D object and place it into AR space
    //
    @IBAction func btnAddPressed(_ sender: UIButton) {
        if self.virtualObjectManager.isSelectedObject {
            self.virtualObjectManager.deselectSelectedObject()
        } else {
            //
            //  We block the "Add" button if there's no plane detected in front of the camera, putting a message on the screen if the user taps "Add" when that's the case.
            //
            if self.isPlaneDetected {
                self.selectObjectToAdd(button: sender)
            } else {
                self.noPlaneDetectedError()
            }
        }
        
        self.updateARViewButtons()
    }

    private func noPlaneDetectedError() {
        self.playFailureSound()
        Async.onMainThread {
            self.textManager.showMessage(LM.localizedString("text_no_plane_unable_to_measure", comment: "Explain that the user cannot measure to the center of the Augmented Reality view because ARKit has not detected a Plane."))
        }
    }
    
    private func startRecordingVideo() {
        let recorder = RPScreenRecorder.shared()
        
        if recorder.isAvailable {
            recorder.isMicrophoneEnabled = true
            recorder.startRecording { (error) in
                if let errorMessage = error {
                    Async.onMainThread {
                        let errorTitle = LM.localizedString("title_error", comment: "Title for a message informing the user that an operation has failed.")
                        Alert.simpleAlertWithOK(errorTitle, message: "Video Screen Recording could not be started on this iOS Device!\n\nError = \(errorMessage.localizedDescription)")
                    }
                } else {
                    //
                    //  Recording has started ...
                    //
                    Async.onMainThread {
                        self.isRecordingVideo = true
                        self.updateARViewButtons()
                    }
                }
            }
        } else {
            Async.onMainThread {
                let errorTitle = LM.localizedString("title_error", comment: "Title for a message informing the user that an operation has failed.")
                Alert.simpleAlertWithOK(errorTitle, message: "Video Screen Recording is not available on this iOS Device!")
            }
        }
    }
    
    private func stopRecordingVideo(thenExit: Bool) {
        guard self.isRecordingVideo else {
            if thenExit {
                self.exitIfTheUserConfirms()
            }
            return
        }
        
        let recorder = RPScreenRecorder.shared()
        
        recorder.stopRecording { (previewController, error) in
            if let previewController = previewController {
                Async.onMainThread {
                    self.discardOrPreviewAndSaveVideo(using: previewController, thenExit: thenExit)
                }
            } else {
                // Handle error
                Log.error{ "FAILURE ON STOP RECORDING! WHAT CAN CAUSE THIS????" }
            }
            Async.onMainThread {
                self.isRecordingVideo = false
                self.updateARViewButtons()
            }
        }
    }
    
    private func discardOrPreviewAndSaveVideo(using previewController: RPPreviewViewController, thenExit: Bool) {
        let title   = LM.localizedString("button_stop_recording_video", comment: "Title for the button which allows the user to stop recording a video clip.")
        let message = LM.localizedString("text_discard_or_preview_and_save_video", comment: "Ask the user whether they wish to discard the video they have just recorded, or preview and optionally save it.")
        
        let discardAction = Alert.Action(actionTitle: LM.localizedString("menu_discard_video", comment: "Discard the Video that's just been recorded."), isDestructive: true, actionCompletion: {
            RPScreenRecorder.shared().discardRecording(handler: {
                Async.onMainThread {
                    //
                    //  Executed once recording has successfully been discarded
                    //
                    if thenExit {
                        self.exitIfTheUserConfirms()
                    }
                }
            })
        })
        
        let previewAction = Alert.Action(actionTitle: LM.localizedString("menu_preview_and_save_video", comment: "Preview (and optionally save) the Video that's just been recorded."), actionCompletion: {
            Async.onMainThread {
                //
                //  It seems that we get runtime errors if we just present the previewController. These mention that a UIPopoverPresentationController should have a non-nil
                //  sourceView or barButtonItem set before presentation. So this is a consequence of the adaptivePresentationStyle ...
                //
                previewController.modalPresentationStyle                  = .popover
                previewController.popoverPresentationController?.delegate = self

                previewController.previewControllerDelegate = self
                self.present(previewController, animated: true, completion: nil)
                
                previewController.popoverPresentationController?.sourceView = self.btnVideo
                previewController.popoverPresentationController?.sourceRect = self.btnVideo.bounds
                
                //
                //  Set the isExitingAfterPreviewingVideo flag, so we can continue to (try to) exit once the Preview/Save view controller exits.
                //
                self.isExitingAfterPreviewingVideo = thenExit
            }
        })
        
        Alert.alertWithActions(title, message: message, iconName: nil, actions: [discardAction, previewAction], lastOptionIsCancel: false, presentedOn: self)
    }
    
    private func selectObjectToAdd(button: UIButton) {
        let selectObjectViewController = Select3DObjectViewController(size: Select3DObjectViewController.popoverSize)
        selectObjectViewController.delegate                                = self
        selectObjectViewController.modalPresentationStyle                  = .popover
        selectObjectViewController.popoverPresentationController?.delegate = self
        
        self.present(selectObjectViewController, animated: true, completion: nil)
        
        selectObjectViewController.popoverPresentationController?.sourceView = button
        selectObjectViewController.popoverPresentationController?.sourceRect = button.bounds
    }
    
    internal func stopVideoTimer() {
        if self.videoTickTimer != nil {
            self.videoTickTimer?.invalidate()
            self.videoTickTimer = nil

            //
            //  Remove the video timer label from just above the "Video" button.
            //
            self.removeVideoTimerLabel()
            self.secondsOfVideoRecorded = 0
        }
    }
    
    internal func startVideoTimer() {
        stopVideoTimer()
        
        //
        //  Add a video timer label just above the "Video" button and start the timer that will update that label every second ...
        //
        self.secondsOfVideoRecorded = 0
        self.addVideoTimerLabel()
        
        self.videoTickTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ARViewController.videoTimerTick(_:)), userInfo: nil, repeats: true)
    }
    
    @objc func videoTimerTick(_ theTimer: Timer) {
        self.secondsOfVideoRecorded += 1
        
        Async.onMainThread {
            self.updateVideoTimerLabel(elapsedTime: TimeInterval(self.secondsOfVideoRecorded))
        }
    }
    
    //
    //  ---------------------    S e l e c t 3 D O b j e c t D e l e g a t e     ---------------------
    //
    func userDidSelect(object: Object3D) {
        
        guard let cameraTransform = session.currentFrame?.camera.transform else {
            return
        }
        
        guard let lastPlanePosition = self.positionOfLatestPlaneDetectedAtCrosshairs else {
            self.noPlaneDetectedError()
            return
        }
        
        //
        //  If there's an existing Plane, position the new 3D Object at that location (pushing it back far enough that the leading edge of the bounding box is
        //  positioned at the crosshairs Plane location); if not, position it 2m away in the current camera direction.
        //
        self.virtualObjectManager.create(newObject: object, atOrBehind: lastPlanePosition, camera: SCNMatrix4(cameraTransform), in: self.sceneView)
    }
    
    //
    //  ---------------------    M i s c e l l a n e o u s    M e t h o d s     ---------------------
    //
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //
        //  All popover segues should be popovers even on iPhone.
        //
        if let popoverController = segue.destination.popoverPresentationController, let button = sender as? UIButton {
            popoverController.delegate   = self
            popoverController.sourceRect = button.bounds
        }
    }
    
    //
    //  MUST call on the main thread!
    //
    internal func updateScreenCenter() {
        self.screenCenter = self.sceneView.bounds.mid
    }
    
    func updateSnapshotButton(isEnabled: Bool, isHiddenIfNotEnabled: Bool = true) {
        let isObjectSelected         = self.virtualObjectManager.isSelectedObject
        self.btnSnapshot.label       = LM.localizedString(isObjectSelected ? "button_delete_object" : "button_snapshot", comment: "")
        let btnImageName             = isObjectSelected ? "Trash" : "Snapshot"
        self.btnSnapshot.buttonImage = Image(named: btnImageName)
        
        self.btnSnapshot.isHidden    = !isEnabled && isHiddenIfNotEnabled
        self.btnSnapshot.isEnabled   = isEnabled
    }
    
    func updateVideoButton(isEnabled: Bool, isHiddenIfNotEnabled: Bool = true) {
        self.btnVideo.label          = LM.localizedString(self.isRecordingVideo ? "button_stop_recording_video" : "button_record_video", comment: "")
        self.btnVideo.buttonImage    = Image(named: "RecordVideo")
        self.btnVideo.animationImage = Image(named: "RecordingVideo")       //  Set up for animation.
        
        self.btnVideo.isHidden    = !isEnabled && isHiddenIfNotEnabled
        self.btnVideo.isEnabled   = isEnabled
    }
    
    func updateAddEndFeatureButton(isEnabled: Bool, isHiddenIfNotEnabled: Bool = true) {
        let isObjectSelected          = self.virtualObjectManager.isSelectedObject
        btnAddObject.label            = LM.localizedString(isObjectSelected ? "button_confirm_object" : "button_add_object", comment: "")
        let btnImageName              = isObjectSelected ?  "ARTick" : "AddFeature"
        self.btnAddObject.buttonImage = Image(named: btnImageName)
        
        self.btnAddObject.isHidden  = !isEnabled && isHiddenIfNotEnabled
        self.btnAddObject.isEnabled = isEnabled
    }
    
    //
    //  ----------------------   P l a n e    D e t e c t i o n   ----------------------
    //
    //  We frequently check to see whether there is a horizontal plane at the center of the ARView (beneath the crosshairs, when those are displayed). If so, this plane is used
    //  to set our cameraHeight (and therefore the height of all the model features).
    //
    //  If there is a plane detected, the crosshairs are displayed in full visual intensity; if not, they are dimmed to indicate that the Measure button won't work if pressed.
    //
    //  Note that we don't actually disable measurement. Rather, we beep and inform the user if they press it when there's no Plane intersection detected.
    //
    
    internal func stopPlaneDetectionTask() {
        if self.planeDetectionTimer != nil {
            self.planeDetectionTimer?.invalidate()
            self.planeDetectionTimer = nil
        }
    }
    
    internal func startPlaneDetectionTask() {
        stopPlaneDetectionTask()
        self.planeDetectionTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(ARViewController.planeDetectionTimerTick(_:)), userInfo: nil, repeats: true)
    }
    
    @objc func planeDetectionTimerTick(_ theTimer: Timer) {
        guard let screenCenter = self.screenCenter else { return }
        
        guard self.virtualObjectManager != nil && self.sceneView != nil && self.scene != nil else { return }
        
        let (positionBeneathCrosshairs, _, didHitAPlane) = self.virtualObjectManager.worldPositionFromScreenPosition(screenCenter, in: self.sceneView, objectPos: nil, infinitePlane: true)
        
        if didHitAPlane {
            //
            //  The first time we hit a Plane, if the Get Into AR Animation is ready to dismiss, we do that, and we start displaying the focus square instead.
            //
            if self.isDisplayingGetIntoARAnimation {
                self.ceaseGetIntoARAnimationIfReadyToDoSo()
            }
            
            //
            //  Ensure the cameraHeight reflects the distance to the plane beneath the crosshairs.
            //
            if let crosshairsPosition = positionBeneathCrosshairs {
                let newCameraHeight = Distance(meters: Double( -crosshairsPosition.y))
                
                if self.scene.cameraHeight != newCameraHeight {
                    self.scene.adjustCameraHeightAboveGroundLevel(cameraHeight: newCameraHeight)
                }
            }
        }

        /*
        //
        //  We keep track of the absolute height of the most-recently-detected Plane. This is used for positioning of any new objects.
        //
        self.planeDetectedAtHeight = didHitAPlane ? positionBeneathCrosshairs?.y : nil
        */
        
        //
        //  We keep track of the position of the crossHairs whenever they hit a detected Plane. This is used for positioning of any new objects.
        //
        self.positionOfLatestPlaneDetectedAtCrosshairs = didHitAPlane ? positionBeneathCrosshairs : nil
    }
    
    // MARK: - Planes
    
    /**
    func addPlane(node: SCNNode, anchor: ARPlaneAnchor) {
        
        let plane = PlaneNode(anchor)
        planes[anchor] = plane
        node.addChildNode(plane)
        
        textManager.cancelScheduledMessage(forType: .planeEstimation)
    }
    
    func updatePlane(anchor: ARPlaneAnchor, node: SCNNode) {
        if let plane = planes[anchor] {
            plane.update(anchor)
            // updateCameraHeight(for: anchor, node: node, isNewAnchor: false)
        } else {
            self.addPlane(node: node, anchor: anchor)
        }
    }
    
    func removePlane(anchor: ARPlaneAnchor) {
        if let plane = planes.removeValue(forKey: anchor) {
            plane.removeFromParentNode()
        }
    }
    **/
    
    func resetTracking() {
        session.run(currentConfiguration, options: [.resetTracking, .removeExistingAnchors])
    }
    
    // MARK: - Focus Square
    
    func setupFocusSquare() {
        serialQueue.async {
            self.focusSquare?.isHidden = true
            self.focusSquare?.removeFromParentNode()
            self.focusSquare = FocusSquare()
            self.sceneView.scene.rootNode.addChildNode(self.focusSquare!)
        }
    }
    
    func updateFocusSquare(isDisplayingFocusSquare: Bool) {
        guard let screenCenter = screenCenter else { return }
        
        DispatchQueue.main.async {
            /*
            var objectVisible = false
            
            for object in self.virtualObjectManager.virtualObjectsArray {
                if self.sceneView.isNode(object, insideFrustumOf: self.sceneView.pointOfView!) {
                    objectVisible = true
                    break
                }
            }

            if objectVisible {
                self.focusSquare?.hide()
            } else {
                self.focusSquare?.unhide()
            }
            */
            
            guard isDisplayingFocusSquare else {
                self.focusSquare?.hide()
                return
            }

            self.focusSquare?.unhide()
            
            let (worldPos, planeAnchor, _) = self.virtualObjectManager.worldPositionFromScreenPosition(screenCenter, in: self.sceneView, objectPos: self.focusSquare?.simdPosition)
            if let worldPos = worldPos {
                self.serialQueue.async {
                    self.focusSquare?.update(for: worldPos, planeAnchor: planeAnchor, camera: self.session.currentFrame?.camera)
                }
                self.textManager.cancelScheduledMessage(forType: .focusSquare)
            }
        }
    }
    
    // MARK: - Error handling
    
    func displayErrorMessage(title: String, message: String, allowRestart: Bool = false) {
        //
        //  Blur the background.
        //
        textManager.blurBackground()
        
        if allowRestart {
            //
            //  Present an alert informing about the error that has occurred.
            //
            let restartAction = UIAlertAction(title: "Reset", style: .default) { _ in
                self.textManager.unblurBackground()
                self.restartExperience()
            }
            textManager.showAlert(title: title, message: message, actions: [restartAction])
        } else {
            let okAction = UIAlertAction(title: "OK", style: .default) { _ in
                self.textManager.unblurBackground()
            }
            
            textManager.showAlert(title: title, message: message, actions: [okAction])
        }
    }
    
    func restartExperience() {
        
        DispatchQueue.main.async {
            self.textManager.cancelAllScheduledMessages()
            self.textManager.dismissPresentedAlert()
            
            self.virtualObjectManager.removeAllVirtualObjects()
            self.focusSquare?.isHidden = true
            
            self.resetTracking()
            
            //
            //  Show the focus square after a short delay to ensure all plane anchors have been deleted.
            //
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.setupFocusSquare()
            })
        }
    }
    
    //
    //  -----------------------  A R V i e w    T a p   G e s t u r e    R e c o g n i z e r  -----------------------
    //
    
    //
    //  Add a tap gesture recognizer to the ARView, so the user can select a 3D Object. If there is already a 3D Object selected, beep and warn the user that they
    //  need to confirm this first.
    //
    func createTapGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ARViewController.handleTapToSelectGesture(_:)))
        
        tapGestureRecognizer.numberOfTouchesRequired = 1
        tapGestureRecognizer.numberOfTapsRequired    = 1
        tapGestureRecognizer.delegate                = self
        
        Async.main{ //  Explicitly ensure that ALL operations that might result in UI are on the main thread
            self.sceneView.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    
    @objc func handleTapToSelectGesture(_ tapGestureRecognizer: UITapGestureRecognizer) {
        if let currentSelectedObject = virtualObjectManager.selectedObject {
            self.playFailureSound()
            let titleError = LM.localizedString("title_error", comment: "Title for a message informing the user that an operation has failed.")
            var message    = LM.localizedString("text_please_confirm_selected_object_before_selecting_another", comment: "Ask the user to confirm the placement of the current selected object, using the 'Confirm 3D Object' button, before tapping on another object. @1 is replaced by the name of the current selected object, while @2 is replaced with the localized title of the 'Confirm 3D Object' button.")
            message.replace("@1", withString: currentSelectedObject.object.name)
            message.replace("@2", withString: LM.localizedString("button_confirm_object", comment: ""))
            Alert.simpleAlertWithOK(titleError, message: message, iconName: nil, presentedOn: self, completion: nil)
        } else {
            let tapLocation = tapGestureRecognizer.location(in: self.sceneView)
            
            if let newSelectedObject = virtualObjectManager.virtualObject(at: tapLocation, in: self.sceneView) {
                virtualObjectManager.select(newSelectedObject, in: self.sceneView)
            } else {
                self.playFailureSound()
                
                let titleTip = LM.localizedString("title_tip", comment: "Title for a message that provides the user with a useful suggestion or hint (a 'Tip').")
                var message  = LM.localizedString("text_explain_object_selection_and_positioning_gestures", comment: "The user has tapped on the Augmented Reality view, but not on a 3D Object. Explain to them how to select a 3D Object, and the gestures they should use to reposition or rotate a selected object.")
                message.replace("@1", withString: LM.localizedString("button_confirm_object", comment: ""))
                Alert.simpleAlertWithOK(titleTip, message: message, iconName: nil, presentedOn: self, completion: nil)
            }
        }
    }
    
    //
    //  ---------------------    S t a t i c H U D    f o r     S n a p s h o t s    ---------------------
    //
    func showSnapshotHUD() {
        updateSnapshotButton(isEnabled: false)
        
        let snapshotHUD              = ProgressHUD(view: self.view)
        snapshotHUD.minSize          = CGSize(width: 200.0, height: 200.0)
        snapshotHUD.delegate         = self
        snapshotHUD.mode             = .customView
        snapshotHUD.customView       = snapshotIconView()
        
        snapshotHUD.labelText        = LM.localizedString("text_snapshot_saved", comment: "Message displayed when a Snapshot has been taken of the Augmented Reality view, and saved with the current Task.")
        snapshotHUD.detailsLabelText = nil
        snapshotHUD.graceTime        = 0.0
        snapshotHUD.minShowTime      = 2.0
        snapshotHUD.dismissible      = false
        
        self.view.addSubview(snapshotHUD)
        
        self.snapshotHUD = snapshotHUD
        
        Async.onMainThread{
            self.snapshotHUD?.show(animated: true)
        }
    }
    
    //
    //  Create an imageView suitable for adding to the snapshotHUD as a customView, to indicate completion of the Snapshot.
    //
    func snapshotIconView() -> View {
        let customView   = View()
        
        let image        = Image(named: "Snapshot")!
        let imageRect    = CGRect(origin: CGPoint.zero, size: image.size)
        let imageView    = ImageView(frame: imageRect)
        imageView.image  = image
        
        customView.frame = imageRect
        customView.addSubview(imageView)
        
        return customView
    }
    
    func HUDWasTapped(_ HUD: ProgressHUD) {
    }
    
    func HUDWasHidden(_ HUD: ProgressHUD) {
        if let snapshotHUD = self.snapshotHUD, snapshotHUD == HUD {
            //
            //  The static Snapshot HUD has done. We can re-enable the "Snapshot" button.
            //
            Async.main{
                self.updateSnapshotButton(isEnabled: true, isHiddenIfNotEnabled: false)
            }
            self.snapshotHUD = nil
            return
        }
    }
    
    func takeSnapshot() {
        
        //
        //  Immediately take the snapshot and make a sound
        //
        AudioServicesPlaySystemSoundWithCompletion(SystemSoundID(1108), nil)
        
        let snapshotImage = self.sceneView.snapshot()
        
        self.showSnapshotHUD()
        
        let snapshot = watermarked(snapshot: snapshotImage)
        
        Log.debug{ "Took snapshot: size = \(snapshot.size)" }
        
        //
        //  Push the processing and saving of the snapshot to the background queue. The "Snapshot" button remains disabled until the image has been saved, which avoids any
        //  threading/race condition issues.
        //
        Async.background {
            //
            //  Now save the photo to the /Tmp folder and move it into the /Media folder in the process of saving it with the current selectedTask.
            //
            //  In the process we add EXIF data containing the watermark content.
            //
            let newSnapshotURL = FileManager.tmpFolderURL.appendingPathComponent(SwiftUUID.uniqueFileName(withExtension: "JPG"))
            
            do {
                guard let snapshotWithEXIF = snapshot.addEXIF(container: self.snapshotEXIF) else {
                    Log.error{ "Failed to add the EXIF data to the JPEG representation of our Snapshot!" }
                    return
                }
                try snapshotWithEXIF.write(to: newSnapshotURL, options: [.atomic])
            } catch {
                Log.error{ "Failed to write our Snapshot to \(newSnapshotURL.lastPathComponent)!" }
                return
            }
            
            //
            //  Add this Snapshot to the current Place
            //
            var mediaForProject = Places2NoteDB.mediaForPlace(self.project)
            
            guard let newSnapshot = PlaceMedia(movingMediaFromURL: newSnapshotURL, forPlaceWithID: self.project.GUID) else {
                Log.error{ "Failed to move Snapshot PlaceMedia from /Tmp to the /Media folder!" }
                return
            }
            
            mediaForProject.append(newSnapshot)
            
            Places2NoteDB.updatePlace(self.project, mediaForPlace: mediaForProject, updateModifiedTime: true)
            
            self.isCallerRequiredToSynchronize = true
            
            //
            //  Also add the snapshot to the user's Photos library, if we have been (or are subsequently) granted permission.
            //
            self.seekPhotoLibraryAccessIfNotAlreadyGranted(ifOrWhenAccessIsGranted: {
                self.saveSnapshotToPhotoLibrary(snapshot)
                
                //
                //  In case we just got granted access, if we're not already observing changes to the user's Video album(s), do so now.
                //
                if self.videosInVideoAlbumFetchResults.count == 0 {
                    self.registerObserverForPhotoLibrary()
                }
            })
            
            //
            //  We're done, so we can tell the "Snapshot" HUD to disappear after an appropriate delay ...
            //
            Async.main{
                self.snapshotHUD?.hide(animated: true, afterDelay: 1.5)
            }
        }
    }
    
    //
    //  Check that we have been granted access to the user's Photo Library and - if so, or when access is granted - call the supplied closure. If access has been denied, inform
    //  the user.
    //
    private func seekPhotoLibraryAccessIfNotAlreadyGranted(ifOrWhenAccessIsGranted callClosure: @escaping () -> ()) {
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            callClosure()
        case .restricted, .denied:
            Async.onMainThread {
                let title   = LM.localizedString("title_permission_denied", comment: "Title for a form explaining that Places2Note requires the user's permission to perform an action, but the user has previously denied this permission.")
                let message = LM.localizedString("text_photo_library_access_denied", comment: "Remind the user that they have previously denied us access to their Photo Library. As a result, we cannot mirror Snapshots to the 'ITS View' folder, and we cannot move any recorded Videos to that folder either.").withApplicationNameInjected
                
                self.textManager.showAlert(title: title, message: message)
                Alert.simpleAlertWithOK(title, message: message)
            }
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (authorizationStatus) in
                if authorizationStatus == .authorized {
                    callClosure()
                }
            })
        }
    }
    
    //
    //  We have access to the user's PhotoLibrary. Register as an observer for changes to their Video folder(s).
    //
    private func registerObserverForPhotoLibrary() {
        PHPhotoLibrary.shared().register(self)
        
        self.videosInVideoAlbumFetchResults = PHPhotoLibrary.videosInVideoAlbumsFetchResults()
    }
    
    private func saveSnapshotToPhotoLibrary(_ snapshot: Image) {
        PHPhotoLibrary.saveImage(image: snapshot, albumName: App.applicationName, completion: { (photoAsset) in
            if photoAsset != nil {
                Log.debug{ "PHPhotoLibrary.saveImage() returned success." }
            } else {
                Log.error{ "PHPhotoLibrary.saveImage() returned a Nil asset!" }
            }
        })
    }
    
    private func snapshotName(projectName: String, snapshotNumber: Int) -> String {
        let snapshotNumberAsString = snapshotNumber.format("4")
        return "\(projectName) - \(snapshotNumberAsString)"
    }
    
    //
    //  Add a "metadata" watermark to the supplied Image. This watermark specifies:
    //
    //  1. That the image was "Surveyed with VisualMapper(R)"
    //  2. The location (lat/long) at which the Image was taken
    //  3. The compass bearing of the camera when the Image was taken
    //  4. The time at which the Image was taken.
    //
    func watermarked(snapshot: Image) -> Image {
        var watermark = LM.localizedString("text_surveyed_with_x", comment: "Text added to Augmented Reality 'snapshots' to identify their provenance.").withApplicationLegalNameInjected
        
        if let currentLocation = AppLocationManager.latestGPSLocation {
            watermark += "   \(currentLocation.latitude.asStringInDegreesMinutesAndSeconds()) \(currentLocation.longitude.asStringInDegreesMinutesAndSeconds())."
        }
        
        if let currentHeading = self.latestHeading {
            watermark += "   \(currentHeading.asString(format: Angle.Formats.degreesDDDddd, precision: 0))."
        }
        
        watermark += "   \(Date.now.toString(dateStyle: DateFormatter.Style.short, timeStyle: DateFormatter.Style.medium, doesRelativeDateFormatting: false, timeZone: TimeZone.autoupdatingCurrent, locale: LocalizationManager.systemLocale))"
        
        return snapshot.image(withWatermark: watermark, textColor: Color.white.withAlphaComponent(0.5), locatedAt: .bottom)
    }
    
    var snapshotEXIF : EXIFContainer {
        let container = EXIFContainer()
        
        let provenanceMessage = LM.localizedString("text_surveyed_with_x", comment: "Text added to Augmented Reality 'snapshots' to identify their provenance.").withApplicationLegalNameInjected
        
        container.add(description: provenanceMessage)
        container.add(userComment: provenanceMessage)
        
        if let currentHeading = self.latestHeading {
            container.add(heading: Direction(radians: currentHeading.radians))
        }
        
        return container
    }
    
    //
    //  --------------------------------------------------------    M e a s u r e m e n t     --------------------------------------------------------
    //
    
    //
    //  Return the (internal) SCNVector3 position of the supplied point in the AR Scene, optionally requiring that a Plane be hit.
    //
    //  IMPORTANT: THIS METHOD SHOULD ONLY BE CALLED BY CLIENTS WHICH UNDERSTAND THE CONSEQUENCES! USUALLY A MEASUREMENT THAT DOES NOT HIT A PLANE WILL BE INACCURATE.
    //
    private func positionOfMeasurement(to screenPosition: CGPoint, isPlaneRequired: Bool, objectPos: float3? = nil) -> SCNVector3? {
        let (scenePositionFloat3, _ /* anchor */, didHitAPlane) = self.virtualObjectManager.worldPositionFromScreenPosition(screenPosition, in: self.sceneView, objectPos: objectPos, infinitePlane: true)
        
        if isPlaneRequired && !didHitAPlane {
            return nil
        }
        
        guard let scenePosition = scenePositionFloat3 else {
            Log.error{ "The hit test returned a nil scenePosition! Perhaps there was no plane detected? DidHitAPlane = \(didHitAPlane)" }
            return nil
        }
        
        return SCNVector3(scenePosition)
        
    }
    
    //
    //  Return the (internal) SCNVector3 position of the measurement crosshairs in the center of the AR Scene, regardless of whether or not a Plane was hit.
    //
    //  IMPORTANT: THIS METHOD SHOULD ONLY BE CALLED BY CLIENTS WHICH UNDERSTAND THE CONSEQUENCES! USUALLY A MEASUREMENT THAT DOES NOT HIT A PLANE WILL BE
    //  INACCURATE. MOST CLIENTS SHOULD USE THE .positionOfMeasurementCrossHairs PROPERTY (WHICH MANDATES THAT A PLANE MUST HAVE BEEN HIT) INSTEAD!
    //
    private func positionOfMeasurementCrossHairs(isPlaneRequired: Bool) -> SCNVector3? {
        guard let screenCenter = self.screenCenter else {
            Log.error{ "NO SCREEN CENTER!" }                //  Should be impossible.
            return nil
        }
        
        return positionOfMeasurement(to: screenCenter, isPlaneRequired: isPlaneRequired)
    }
    
    //
    //  Return the (internal) SCNVector3 position of the measurement crosshairs in the center of the AR Scene.
    //
    //  If the ray from the AR "camera" to that point in the center of the AR View doesn't strike a Plane, the measurement is VERY likely to be inaccurate and
    //  in that case we reject the measurement and return nil.
    //
    private var positionOfMeasurementCrossHairs : SCNVector3? {
        get {
            return self.positionOfMeasurementCrossHairs(isPlaneRequired: true)
        }
    }

    //
    //  -----------------------------------    F a i l u r e     S o u n d    -------------------------
    //
    func playFailureSound() {
        AudioServicesPlayAlertSound(SystemSoundID(1053))     //  SIMToolkitNegativeACK
    }
    
    // MARK: - Get Into AR Animation
    
    func setupGetIntoARAnimation() {
        serialQueue.async {
            self.getIntoARAnimation?.isHidden = true
            self.getIntoARAnimation?.removeFromParentNode()
            self.getIntoARAnimation = GetIntoARAnimation()
            self.getIntoARAnimation?.position      = SCNVector3Make(0, -0.5, -5.0)
            self.getIntoARAnimation?.eulerAngles.x = .pi / 20
            self.sceneView.pointOfView?.addChildNode(self.getIntoARAnimation!)
            
            self.getIntoARAnimation?.startAnimating(label: self.lblGetIntoAugmentedReality, container: self.containerViewForLabel)
        }
    }
    
    func updateGetIntoARAnimation() {
        Async.main {
            self.getIntoARAnimation?.updateAnimation()
        }
    }
    
    private func ceaseGetIntoARAnimation() {
        Async.main {
            self.getIntoARAnimation?.stopAnimating {
                self.getIntoARAnimation      = nil
                self.textManager.cancelScheduledMessage(forType: .planeEstimation)
                self.isDisplayingFocusSquare = true
            }
        }
    }
    
    func ceaseGetIntoARAnimationIfReadyToDoSo() {
        guard let getIntoARAnimation = self.getIntoARAnimation else { return }
        
        if getIntoARAnimation.isReadyToHideAnimation {
            self.ceaseGetIntoARAnimation()
        }
    }
}

extension ARViewController : RPScreenRecorderDelegate {
    
    //
    //  If an error occurs during video recording, warn the user and optionally give them a chance to save the Video.
    //
    public func screenRecorder(_ screenRecorder: RPScreenRecorder, didStopRecordingWith previewViewController: RPPreviewViewController?, error: Error?) {
        Async.onMainThread {
            let errorTitle = LM.localizedString("title_error", comment: "Title for a message informing the user that an operation has failed.")
            
            let okAction = Alert.Action(actionTitle: LM.localizedString("button_ok", comment: "The standard OK for dialog boxes")) {
                if let previewController = previewViewController {
                    self.discardOrPreviewAndSaveVideo(using: previewController, thenExit: false)
                }
            }
            
            Alert.alertWithActions(errorTitle, message: error?.localizedDescription, iconName: nil, actions: [okAction], lastOptionIsCancel: false, presentedOn: self)
            
            self.isRecordingVideo = false
            self.updateARViewButtons()
        }
    }
}

extension ARViewController : RPPreviewViewControllerDelegate {
    
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        Async.onMainThread {
            previewController.dismiss(animated: true, completion: {
                if self.isExitingAfterPreviewingVideo {
                    self.isExitingAfterPreviewingVideo = false
                    self.exitIfTheUserConfirms()
                }
            })
            // self.dismiss(animated: true, completion: nil)
        }
    }
}

extension ARViewController : PHPhotoLibraryChangeObserver {
    
    // This callback is invoked on an arbitrary serial queue. If you need this to be handled on a specific queue, you should redispatch appropriately
    public func photoLibraryDidChange(_ changeInstance: PHChange) {
        Async.onMainThread {
            //
            //  Ignore anything that happens when we are not actually running ...
            //
            guard UIApplication.shared.applicationState == .active else {
                return
            }
            
            //
            //  OK, someone (who by definition must be us!) made a change to the Photos library. We care about this only if it's a new Video (which we will then move to our "ITS View"
            //  folder.
            //
            guard self.videosInVideoAlbumFetchResults.count > 0 else { return }
            
            var updatedFetchResults : [PHFetchResult<PHAsset>] = []
            
            for fetchResults in self.videosInVideoAlbumFetchResults {
                //
                //  Check for changes to the list of assets (insertions, deletions, moves, or updates). We care only about insertions.
                //
                if let changes = changeInstance.changeDetails(for: fetchResults) {
                    //
                    //  Keep the new fetch result for future use.
                    //
                    updatedFetchResults.append(changes.fetchResultAfterChanges)
                    
                    if changes.hasIncrementalChanges {
                        //
                        //  Are there are incremental diffs? We care only about insertions ...
                        //
                        /*
                        if let removed = changes.removedIndexes where removed.count > 0 {
                            xxx
                        }
                        */
                        if let inserted = changes.insertedIndexes, inserted.count > 0 {
                            let insertedVideos = changes.fetchResultAfterChanges.objects(at: inserted)
                            
                            //
                            //  Add all these new videos (typically there will only be one) to the "ITS View" album, creating it in the process if necessary.
                            //
                            PHPhotoLibrary.add(assets: insertedVideos, toAlbumNamed: App.applicationName, completion: { (addedVideos) in
                                guard let addedVideos = addedVideos else {
                                    Log.error{ "FAILED TO ADD \(insertedVideos.count) VIDEOS TO THE \(App.applicationName) ALBUM!" }
                                    return
                                }
                                if addedVideos.count != insertedVideos.count {
                                    Log.error{ "AT LEAST ONE OF THE \(insertedVideos.count) VIDEOS WHICH HAD BEEN INSERTED WAS NOT COPIED TO THE \(App.applicationName) ALBUM!" }
                                }
                            })
                        }
                    }
                } else {
                    //
                    //  Nothing changed, but keep a record of what we already had in case it changes in future.
                    //
                    updatedFetchResults.append(fetchResults)
                }
            }
            
            //
            //  Update the saved videoFetchResults for future reference ...
            //
            self.videosInVideoAlbumFetchResults = updatedFetchResults
        }
    }
    
}

extension CGPoint {
    
    //
    //  Given a bearing in radians and a distance, returns the point at that distance and bearing from this point.
    //
    public func point(atBearing bearing: CGFloat, andDistance distance: CGFloat) -> CGPoint {
        return CGPoint(x: self.x + (distance * sin(bearing)), y: self.y + (distance * cos(bearing)))
    }
    
    //
    //  Create a CGPoint from a SCNVector3. But unlike the standard constructor, this one presumes the incoming SCNVector3 is a 2D location with the .z property being what we'd
    //  normally consider to be the "y" dimension. The .y property (the height, in 3D) is ignored.
    //
    init(vector3D: SCNVector3) {
        self.init(x: CGFloat(vector3D.x), y: CGFloat(vector3D.z))
    }
    
}

extension SCNVector3 {
    
    func distance(to: SCNVector3) -> Float {
        let xd = to.x - self.x
        let yd = to.y - self.y
        let zd = to.z - self.z
        
        return abs(Float(sqrt(xd * xd + yd * yd + zd * zd)))
    }
    
    func distance2D(to: SCNVector3) -> Float {
        let xd = to.x - self.x
        let zd = to.z - self.z
        
        return abs(Float(sqrt(xd * xd + zd * zd)))
    }
    
    //
    //  Given a sceneLocation, where x represents the west-east axis and z represents the north-south axis, return the corresponding 2D polar coordinates (R, Phi) where R is the
    //  distance from the origin and Phi is the angle from the origin.
    //
    var polarCoordinates : (/* rDistance: */ Float, /* phiRadians: */ Float) {
        get {
            let x = self.x
            let y = self.z
            
            let rDistance  = sqrt((x * x) + (y * y))
            let phiRadians = atan2(y,x)
            
            return (rDistance, phiRadians)
        }
    }
    
    //
    //  Given a pair of 2D polar coordinates (R,Phi), plus a height (y axis), return the corresponding vector.
    //
    init(rDistance: Float, phiRadians: Float, yHeight: Float) {
        self.init(rDistance * cos(phiRadians), rDistance * sin(phiRadians), yHeight)
    }
    
}

extension CGPoint {
    
    func angle(to: CGPoint) -> Angle {
        let origin         = CGPoint(x: to.x - self.x, y: to.y - self.y)
        //
        //  Need to subtract 90 degrees because objects are already aligned to the X axis.
        //
        let bearingRadians = atan2(origin.x, origin.y) - (.pi / 2.0)
        
        return Angle(radians: Double(bearingRadians), minimumValue: 0.0, maximumValue: 2.0 * .pi, normalizeValue: true)
    }
    
}

//
//  -----------------------  P l a n e s M a n a g e r D e l e g a t e  -----------------------
//
extension ARViewController: PlanesManagerDelegate {
    
    func didUnlockPlane(_ lockedPlane: ARPlaneAnchor) {
        self.textManager.showMessage(LM.localizedString("title_plane_unlocked", comment: "Message displayed in Augmented Reality when the user has tapped on the screen to deselect and 'unlock' the current locked Plane."), autoHide: true)
    }
    
    func didLockPlane(_ lockedPlane: ARPlaneAnchor) {
        self.textManager.showMessage(LM.localizedString("title_plane_locked", comment: "Message displayed in Augmented Reality when the user has tapped on the screen to select and 'lock' a specific Plane."), autoHide: false)
        
        HapticManager.hapticFeedback(level: .medium)
    }
    
}

extension ARPlaneAnchor {
    
    ///
    /// - Returns:  the nominal scenePosition of this **ARPlaneAnchor**, being the "translation" portion of its transform.
    ///
    /// NOTE: It is generally far better to use this to obtain (e.g. the height of a horizontal Plane), rather than the .center property which isn't all that useful.
    ///
    var scenePosition : SCNVector3 {
        get {
            return SCNVector3(self.transform.translation)
        }
    }
    
}

