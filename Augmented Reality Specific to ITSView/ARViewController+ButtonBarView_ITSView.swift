//
//  ARViewController+ButtonBarView.swift
//  ITS View
//
//  Created by Paul Manson on 28/08/17.
//  Copyright © 2017 e.quinox KG. All rights reserved.
//
//  Methods to rearrange the ButtonBar (containing the AddObject/Video/Snapshot buttons) when running on an iPad.
//

import UIKit
import PalaceSoftwareCore

extension ARViewController {
    //
    //  Reset the constraints for the buttonBar, based on our new UIOrientation.
    //
    internal func repositionButtonBar() {
        guard let newOrientation = self.orientation else { return }

        //
        //  Return immediately if we've already configured the iPad UI for this newOrientation.
        //
        if let previousConfiguredOrientation = self.iPadUIConfiguredForOrientation, previousConfiguredOrientation == orientation {
            return
        }
        
        switch newOrientation {
        case .portrait, .portraitUpsideDown, .unknown:
            self.buttonsForPortraitOrientation()
        case .landscapeLeft:
            self.buttonsForLandscapeOrientation(isLeft: true)
        case .landscapeRight:
            self.buttonsForLandscapeOrientation(isLeft: false)
        }
        
        self.iPadUIConfiguredForOrientation = newOrientation
    }
    
    ///
    /// Add a label showing elapsed video recording time, positioned just above the Video button. Must be called on the main thread.
    ///
    internal func addVideoTimerLabel() {
        let videoTimerLabel = UILabel(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 80.0, height: 36.0)))
        videoTimerLabel.textColor     = Color.white
        videoTimerLabel.font          = Font.systemFont(ofSize: 13.0)
        videoTimerLabel.textAlignment = .center
        videoTimerLabel.text          = "00:00:00"
        videoTimerLabel.isHidden      = false
        
        self.buttonBarView?.addSubview(videoTimerLabel)
        
        self.lblVideoTimer = videoTimerLabel
        self.lblVideoTimer?.translatesAutoresizingMaskIntoConstraints = false
        
        self.lblVideoTimer?.bottomAnchor.constraint (equalTo: self.btnVideo.topAnchor,        constant:  4.0).isActive = true
        self.lblVideoTimer?.centerXAnchor.constraint(equalTo: self.btnVideo.centerXAnchor                   ).isActive = true
        self.lblVideoTimer?.heightAnchor.constraint (equalToConstant:                                   36.0).isActive = true
        self.lblVideoTimer?.widthAnchor.constraint  (greaterThanOrEqualToConstant:                      80.0).isActive = true
    }

    ///
    /// Remove the video timer label. Must be called on the main thread.
    ///
    internal func removeVideoTimerLabel() {
        self.lblVideoTimer?.isHidden = true
        self.lblVideoTimer?.removeFromSuperview()
        if let videoTimerLabelConstraints = self.lblVideoTimer?.constraints {
            NSLayoutConstraint.deactivate(videoTimerLabelConstraints)
            self.lblVideoTimer?.removeConstraints(videoTimerLabelConstraints)
        }

        self.lblVideoTimer = nil
    }
    
    ///
    /// Update the video timer label. Must be called on the main thread.
    ///
    internal func updateVideoTimerLabel(elapsedTime: TimeInterval) {
        guard self.lblVideoTimer != nil else { return }
        
        self.lblVideoTimer?.text = Date.today.dateAtStartOfDay().addingTimeInterval(elapsedTime).toStringHHMMSS()
    }

    private func buttonsForPortraitOrientation() {
        self.removeExistingConstraints()

        self.addButtonInternalConstraints(isPortraitMode: true)
        
        //
        //  Add all the constraints needed for the buttonBarView in portrait mode.
        //
        let guide : UILayoutGuide
        if #available(iOS 11.0, *) {
            guide = self.view.safeAreaLayoutGuide
        } else {
            guide = self.view.layoutMarginsGuide
        }
        self.buttonBarView.leadingAnchor.constraint (equalTo: guide.leadingAnchor                            ).isActive = true
        self.buttonBarView.trailingAnchor.constraint(equalTo: guide.trailingAnchor                           ).isActive = true
        self.buttonBarView.bottomAnchor.constraint  (equalTo: guide.bottomAnchor                             ).isActive = true
        self.buttonBarView.heightAnchor.constraint  (equalToConstant: 64                                     ).isActive = true
        
        //
        //  X
        //
        self.btnVideo.leadingAnchor.constraint      (equalTo: self.buttonBarView.leadingAnchor, constant:   4).isActive = true
        self.btnAddObject.centerXAnchor.constraint  (equalTo: self.buttonBarView.centerXAnchor               ).isActive = true
        self.btnSnapshot.trailingAnchor.constraint  (equalTo: self.buttonBarView.trailingAnchor, constant: -4).isActive = true

        //
        //  Y
        //
        self.btnAddObject.topAnchor.constraint     (equalTo: self.buttonBarView.topAnchor                    ).isActive = true
        self.btnAddObject.centerYAnchor.constraint (equalTo: self.btnVideo.centerYAnchor                     ).isActive = true
        self.btnAddObject.centerYAnchor.constraint (equalTo: self.btnSnapshot.centerYAnchor                  ).isActive = true

        self.addTimerLabelConstraints()
    }
    
    private func buttonsForLandscapeOrientation(isLeft: Bool) {
        self.removeExistingConstraints()
        
        self.addButtonInternalConstraints(isPortraitMode: false)
        
        //
        //  Add all the constraints needed for the buttonBarView in landscape mode.
        //
        let guide : UILayoutGuide
        if #available(iOS 11.0, *) {
            guide = self.view.safeAreaLayoutGuide
        } else {
            guide = self.view.layoutMarginsGuide
        }
        
        if isLeft {
            self.buttonBarView.leadingAnchor.constraint (equalTo: guide.leadingAnchor                         ).isActive = true
        } else {
            self.buttonBarView.trailingAnchor.constraint (equalTo: guide.trailingAnchor                       ).isActive = true
        }
        self.buttonBarView.centerYAnchor.constraint (equalTo: self.view.centerYAnchor                         ).isActive = true
        self.buttonBarView.widthAnchor.constraint   (equalToConstant:  80                                     ).isActive = true
        self.buttonBarView.heightAnchor.constraint  (equalToConstant: 400                                     ).isActive = true
        
        //
        //  X
        //
        self.btnVideo.centerXAnchor.constraint      (equalTo: self.buttonBarView.centerXAnchor                ).isActive = true
        self.btnAddObject.centerXAnchor.constraint  (equalTo: self.buttonBarView.centerXAnchor                ).isActive = true
        self.btnSnapshot.centerXAnchor.constraint   (equalTo: self.buttonBarView.centerXAnchor                ).isActive = true
        
        //
        //  Y
        //
        self.btnVideo.topAnchor.constraint          (equalTo: self.buttonBarView.topAnchor,     constant:   36).isActive = true
        self.btnAddObject.centerYAnchor.constraint  (equalTo: self.buttonBarView.centerYAnchor, constant:   18).isActive = true
        self.btnSnapshot.bottomAnchor.constraint    (equalTo: self.buttonBarView.bottomAnchor,  constant:    0).isActive = true
        
        self.addTimerLabelConstraints()
    }
    
    private func addButtonInternalConstraints(isPortraitMode: Bool) {
        self.btnSnapshot.addInternalConstraints()
        self.btnAddObject.addInternalConstraints()
        self.btnVideo.addInternalConstraints()
        
        self.btnSnapshot.isArrangedAlongTheBottomOfTheScreen  = isPortraitMode
        self.btnAddObject.isArrangedAlongTheBottomOfTheScreen = isPortraitMode
        self.btnVideo.isArrangedAlongTheBottomOfTheScreen     = isPortraitMode
    }
    
    private func addTimerLabelConstraints() {
        self.lblVideoTimer?.heightAnchor.constraint (equalToConstant:                               36.0).isActive = true
        self.lblVideoTimer?.widthAnchor.constraint  (greaterThanOrEqualToConstant:                  80.0).isActive = true
        self.lblVideoTimer?.bottomAnchor.constraint (equalTo: self.btnVideo.topAnchor,    constant:  4.0).isActive = true
        self.lblVideoTimer?.centerXAnchor.constraint(equalTo: self.btnVideo.centerXAnchor               ).isActive = true
    }

    private func removeExistingConstraints() {
        
        self.buttonBarView.translatesAutoresizingMaskIntoConstraints  = false
        
        self.buttonBarView.removeAllContraintsFromView(removeParentConstraints: true, removeChildConstraints: true)
        
        NSLayoutConstraint.deactivate(self.btnAddObject.constraints)
        NSLayoutConstraint.deactivate(self.btnVideo.constraints)
        NSLayoutConstraint.deactivate(self.btnSnapshot.constraints)
        
        if let videoTimerLabelConstraints = self.lblVideoTimer?.constraints {
            NSLayoutConstraint.deactivate(videoTimerLabelConstraints)
        }

    }

}

class ButtonBarView : UIView {
    
}

extension UIView {
    
    func removeAllContraintsFromView(removeParentConstraints: Bool, removeChildConstraints: Bool) {
        if removeParentConstraints {
            //
            //  Remove constraints between view and its parent.
            //
            let superView = self.superview
            self.removeFromSuperview()
            superView?.addSubview(self)
        }
        
        if removeChildConstraints {
            //
            //  Remove constraints between this view and its children.
            //
            NSLayoutConstraint.deactivate(self.constraints)
        }
    }

}

class VerticalTopAlignedLabel: UILabel {
    
    override func drawText(in rect:CGRect) {
        guard let labelText = text else {  return super.drawText(in: rect) }
        
        let attributedText = NSAttributedString(string: labelText, attributes: [NSAttributedString.Key.font: font])
        var newRect = rect
        newRect.size.height = attributedText.boundingRect(with: rect.size, options: .usesLineFragmentOrigin, context: nil).size.height
        
        if numberOfLines != 0 {
            newRect.size.height = min(newRect.size.height, CGFloat(numberOfLines) * font.lineHeight)
        }
        
        super.drawText(in: newRect)
    }
    
}

