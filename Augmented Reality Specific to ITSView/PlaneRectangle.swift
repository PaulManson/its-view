//
//  PlaneRectangle.swift
//
//  Based on a class of the same name in ARKitRectangleDetection
//
//  Created by Melissa Ludowise on 8/5/17.
//  Source: https://github.com/mludowise/ARKitRectangleDetection
//  Copyright © 2017 Mel Ludowise. All rights reserved.
//
//  Simplified and adapted for ITS View by Paul Manson.
//

import Foundation
import ARKit
import Vision
import PalaceSoftwareSpatial

///
/// A PlaneRectangle represents a 2D Rectangular Plane
///
class PlaneRectangle: NSObject {
    
    private var min : SCNVector3
    private var max : SCNVector3

    ///
    ///  Creates a 2D Rectangular Plane given the lower-left and upper-right positions (i.e. the bounding box). The 3rd dimension (i.e. Height differences) are ignored.
    ///
    /// - Important!:   The min and max values MUST be in terms of ARKit axes (i.e. if the model is rotated, these must also be rotated; if the model is scaled, so must these be).
    ///
    init(min: SCNVector3, max: SCNVector3) {
        self.min = SCNVector3(min.x, 0.0, min.z)
        self.max = SCNVector3(max.x, 0.0, max.z)
    }
    
    private var topLeft : SCNVector3 {
        get {
            return self.min
        }
    }
    
    private var topRight : SCNVector3 {
        get {
            return SCNVector3(self.max.x, self.max.y, self.min.z)
        }
    }
    
    private var bottomLeft : SCNVector3 {
        get {
            return SCNVector3(self.min.x, self.min.y, self.max.z)
        }
    }
    
    private var bottomRight : SCNVector3 {
        get {
            return self.max
        }
    }
    
    ///
    /// - Returns: The rectangle's width, scaled.
    ///
    var width: Float {
        get {
            return topRight.distance(from: topLeft)
        }
    }
    
    ///
    /// - Returns: The rectangle's height, scaled.
    ///
    var height: Float {
        get {
            return topLeft.distance(from: bottomLeft)
        }
    }

    ///
    /// - Returns: the midpoint from opposite corners of rectangle
    ///
    var center: SCNVector3 {
        get {
            return topRight.midPoint(from: bottomLeft)
        }
    }
    
    ///
    /// - Returns: The orientation of the rectangle based on how much the rectangle is rotated around the y axis
    ///
    var orientation: Float {
        get {
            let distX = topRight.x - topLeft.x
            let distZ = topRight.z - topLeft.z
            return -atan(distZ / distX)
        }
    }
}

extension PlaneRectangle {
    
    ///
    /// - Returns: The radius of the circle centered at the center of this PlaneRectangle and which just contains it.
    ///
    var boundingCircleRadius : Float {
        get {
            let center = self.center
            
            let corners = [self.topLeft, self.topRight, self.bottomRight, self.bottomLeft]
            
            let maxRadius = corners.reduce(0.0) { (soFar, corner) -> Float in
                let distanceFromCorner = center.distance(from: corner)
                
                return (distanceFromCorner > soFar) ? distanceFromCorner : soFar
            }
            
            return maxRadius
        }
    }
    
}

extension SCNVector3 {
    
    func distance(from vector: SCNVector3) -> Float {
        let deltaX = self.x - vector.x
        let deltaY = self.y - vector.y
        let deltaZ = self.z - vector.z
        
        return sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)
    }
    
    func midPoint(from vector: SCNVector3) -> SCNVector3 {
        let midX = (self.x + vector.x) / 2
        let midY = (self.y + vector.y) / 2
        let midZ = (self.z + vector.z) / 2
        return SCNVector3(midX, midY, midZ)
    }
    
    /*
    //
    //  From Apple's demo App
    //
    static func positionFromTransform(_ transform: matrix_float4x4) -> SCNVector3 {
        return SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
    }
    */
}

