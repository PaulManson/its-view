//
//  ARViewController+VirtualObjectManagerDelegate.swift
//
//  Copyright(c) e.quinox kG, 2018.
//

import UIKit
import SceneKit
import PalaceSoftwareCore
import PalaceSoftwareSpatial

///
/// VirtualObjectManager delegate methods for the ITS View Application's AugmentedRealityViewController. These methods actually respond visibly in the ARView to events which are
/// implemented by the VirtualObjectManager (and which are, in turn, triggered by the user's interaction with ARKit).
///
extension ARViewController: VirtualObjectManagerDelegate {
    
    // MARK: - VirtualObjectManager delegate callbacks
    
    func virtualObjectManager(_ manager: VirtualObjectManager, willLoad object: VirtualObject) {
        DispatchQueue.main.async {
            //
            //  Show a progress indicator just in front of the camera while the object loads
            //
            if let existingPleaseWaitNode = self.pleaseWaitNode {
                existingPleaseWaitNode.removeFromParentNode()
                self.pleaseWaitNode = nil
            }

            let pleaseWaitNode = PleaseWaitNode()
            pleaseWaitNode.position = SCNVector3(0, 0, -3) //  1.5m away ...
            
            self.sceneView.scene.rootNode.addChildNode(pleaseWaitNode)
            
            self.pleaseWaitNode = pleaseWaitNode
            
            /**
            //
            //  Show progress indicator
            //
            self.spinner = UIActivityIndicatorView()
            self.spinner!.center      = self.sceneView.center // self.addObjectButton.center
            self.spinner!.bounds.size = CGSize(width: 48.0, height: 48.0)  // CGSize(width: self.addObjectButton.bounds.width - 5, height: self.addObjectButton.bounds.height - 5)
          //self.addObjectButton.setImage(#imageLiteral(resourceName: "buttonring"), for: [])
            self.sceneView.addSubview(self.spinner!)
            self.spinner!.startAnimating()
            
            self.isLoadingObject = true
            **/
        }
    }
    
    func virtualObjectManager(_ manager: VirtualObjectManager, didLoad object: VirtualObject) {
        DispatchQueue.main.async {
            if let existingPleaseWaitNode = self.pleaseWaitNode {
                existingPleaseWaitNode.removeFromParentNode()
                self.pleaseWaitNode = nil
            }
            
            /**
            self.isLoadingObject = false
            
            //
            //  Remove progress indicator
            //
            self.spinner?.removeFromSuperview()
            //self.addObjectButton.setImage(#imageLiteral(resourceName: "add"), for: [])
            //self.addObjectButton.setImage(#imageLiteral(resourceName: "addPressed"), for: [.highlighted])
            **/
        }
    }
    
    func virtualObjectManager(_ manager: VirtualObjectManager, couldNotPlace object: VirtualObject) {
        textManager.showMessage("CANNOT PLACE OBJECT\nTry moving left or right.")
    }
    
    // MARK: - VirtualObjectSelectionViewControllerDelegate
    
    /*
    func virtualObjectSelectionViewController(_: VirtualObjectSelectionViewController, didSelectObjectAt index: Int) {
        guard let cameraTransform = session.currentFrame?.camera.transform else {
            return
        }
        
        let definition = VirtualObjectManager.availableObjects[index]
        let object = VirtualObject(definition: definition)
        let position = focusSquare?.lastPosition ?? float3(0)
        virtualObjectManager.loadVirtualObject(object, to: position, cameraTransform: cameraTransform)
        if object.parent == nil {
            serialQueue.async {
                self.sceneView.scene.rootNode.addChildNode(object)
            }
        }
    }
    
    func virtualObjectSelectionViewController(_: VirtualObjectSelectionViewController, didDeselectObjectAt index: Int) {
        virtualObjectManager.removeVirtualObject(at: index)
    }
    */
    
    func virtualObjectManager(_ manager: VirtualObjectManager, didMove object: VirtualObject, to objectPosition: SCNVector3) {
        Async.main{
            /*
            Alert.simpleAlertWithOK("New Object", message: "cameraWorldPos = \(cameraWorldPos).\nObject pos = \(objectPosition).\nCamera to Object = \(distanceFromCamera)", iconName: nil, presentedOn: self, completion: nil)
            */
        }
    }
    
    func virtualObjectManager(_ manager: VirtualObjectManager, didDelete object: VirtualObject) {
        Async.main {
            object.isHidden = true
            object.removeFromParentNode()
            Log.debug{ "DELETED OBJECT \(object.object.name)" }
            
            self.updateARViewButtons()
        }
    }
    
    func virtualObjectManager(_ manager: VirtualObjectManager, didCreate object: VirtualObject) {
        Async.main {
            self.sceneView.scene.rootNode.addChildNode(object)
            
            Log.debug{ "CREATED OBJECT \(object.object.name)" }
            
            self.updateARViewButtons()
        }
    }
    
    func virtualObjectManager(_ manager: VirtualObjectManager, didSelect object: VirtualObject) {
        Async.main {
            Log.debug{ "SELECTED OBJECT \(object.object.name)" }

            self.updateARViewButtons()
        }
    }
    
    func virtualObjectManager(_ manager: VirtualObjectManager, didDeselect object: VirtualObject) {
        Async.main {
            Log.debug{ "DE-SELECTED OBJECT \(object.object.name) which is at \(object.position.description)" }
            
            self.updateARViewButtons()
        }
    }
    
    func virtualObjectManager(_ manager: VirtualObjectManager, didMoveObjectOntoNearbyPlane object: VirtualObject, adjustingHeightBy distance: Distance) {
        let distanceToTheMillimeter = distance.asString(inDistanceUnits: .meters, format: ".3")
        Log.debug{ "At \(Date.now) moved object named \(object.object.name) onto nearby plane, adjusting its height by \(distanceToTheMillimeter)" }
    }
}
