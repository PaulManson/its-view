//
//  PhotoLibrary.swift
//  ITS View
//
//  Source: https://gist.github.com/ricardopereira/636ccd0a3c8a327c43d42e7cbca4d041
//

import Foundation
import Photos
import PalaceSoftwareCore

extension PHPhotoLibrary {
    
    typealias PhotoAsset = PHAsset
    typealias PhotoAlbum = PHAssetCollection
    
    static func saveImage(image: UIImage, albumName: String, completion: @escaping (PHAsset?)->()) {
        if let album = self.findAlbum(albumName: albumName) {
            saveImage(image: image, album: album, completion: completion)
            return
        }
        
        createAlbum(albumName: albumName) { album in
            if let album = album {
                self.saveImage(image: image, album: album, completion: completion)
            } else {
                assert(false, "Album is nil")
            }
        }
    }
    
    static func add(assets: [PHAsset], toAlbumNamed albumName: String, completion: @escaping ([PHAsset]?)->()) {
        if let existingAlbum = findAlbum(albumName: albumName) {
            add(assets: assets, to: existingAlbum, completion: completion)
        } else {
            createAlbum(albumName: albumName) { (album) in
                if let newAlbum = album {
                    add(assets: assets, to: newAlbum, completion: completion)
                } else {
                    Log.error{ "FAILED TO ADD AN ASSET (PHOTO/VIDEO) TO THE ALBUM NAMED \(albumName) ... FAILED TO CREATE THE ALBUM!" }
                    completion(nil)
                }
            }
        }
    }
    
    static func add(assets: [PHAsset], to album: PhotoAlbum, completion: @escaping ([PHAsset]?)->()) {
        PHPhotoLibrary.shared().performChanges({
            //
            //  Request editing the album
            //
            guard let albumChangeRequest = PHAssetCollectionChangeRequest(for: album) else {
                assert(false, "Album change request failed")
                completion(nil)
                return
            }
            
            albumChangeRequest.addAssets(assets as NSArray)
        }, completionHandler: { success, error in
            if success {
                Log.error{ "PHPhotoLibrary.performChanges() added \(assets.count) video(s) to the album: \(album)" }
                completion(assets)
            } else {
                Log.error{ "PHPhotoLibrary.performChanges() resulted in an error = \(String(describing: error?.localizedDescription))" }
                completion(nil)
            }
        })
    }
    
    static private func saveImage(image: UIImage, album: PhotoAlbum, completion: @escaping (PHAsset?)->()) {
        var placeholder: PHObjectPlaceholder?
        
        PHPhotoLibrary.shared().performChanges({
            //
            //  Request creating an asset from the image
            //
            let createAssetRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            
            //
            //  Request editing the album
            //
            guard let albumChangeRequest = PHAssetCollectionChangeRequest(for: album) else {
                assert(false, "Album change request failed")
                return
            }
            
            //
            //  Get a placeholder for the new asset and add it to the album editing request
            //
            guard let photoPlaceholder = createAssetRequest.placeholderForCreatedAsset else {
                assert(false, "Placeholder is nil")
                return
            }
            
            placeholder = photoPlaceholder
            albumChangeRequest.addAssets([photoPlaceholder] as NSArray)
        }, completionHandler: { success, error in
            guard let placeholder = placeholder else {
                assert(false, "Placeholder is nil")
                completion(nil)
                return
            }
            
            if success {
                if let firstAsset = PHAsset.fetchAssets(withLocalIdentifiers: [placeholder.localIdentifier], options: nil).firstObject {
                    completion(firstAsset)
                } else {
                    Log.error{ "PHAsset.fetchAssets(withLocalIdentifiers: \(placeholder.localIdentifier)) didn't return any Assets!" }
                    completion(nil)
                }
            } else {
                Log.error{ "PHPhotoLibrary.performChanges() resulted in an error = \(String(describing: error?.localizedDescription))" }
                completion(nil)
            }
        })
    }
    
    static func findAlbum(albumName: String) -> PhotoAlbum? {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
        
        let fetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: fetchOptions)
        
        guard let photoAlbum = fetchResult.firstObject else {
            return nil
        }
        return photoAlbum
    }
    
    static func videoAlbumFetchResults() -> PHFetchResult<PHAssetCollection> {
        return PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.smartAlbum, subtype: PHAssetCollectionSubtype.smartAlbumVideos, options: nil)
    }
    
    static func videosInVideoAlbumsFetchResults() -> [PHFetchResult<PHAsset>] {
        var allVideos : [PHFetchResult<PHAsset>] = []
        
        let fetchResult = videoAlbumFetchResults()
        
        for videoAlbumIndex in 0 ..< fetchResult.count {
            let videosInAlbum = PHAsset.fetchAssets(in: fetchResult.object(at: videoAlbumIndex), options: nil)
            
            allVideos.append(videosInAlbum)
        }
        
        return allVideos
    }
    
    static func videoAlbums() -> [PhotoAlbum] {
        let fetchResult = videoAlbumFetchResults()
        
        var result : [PhotoAlbum] = []
        
        for albumIndex in 0 ..< fetchResult.count {
            result.append(fetchResult.object(at: albumIndex))
        }

        return result
    }
    
    static func createAlbum(albumName: String, completion: @escaping (PhotoAlbum?)->()) {
        var albumPlaceholder: PHObjectPlaceholder?
        
        PHPhotoLibrary.shared().performChanges({
            //
            //  Request creating an album with parameter name
            //
            let createAlbumRequest = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: albumName)
            
            //
            //  Get a placeholder for the new album
            //
            albumPlaceholder = createAlbumRequest.placeholderForCreatedAssetCollection
        }, completionHandler: { success, error in
            guard let placeholder = albumPlaceholder else {
                assert(false, "Album placeholder is nil")
                completion(nil)
                return
            }
            
            let fetchResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [placeholder.localIdentifier], options: nil)
            guard let album = fetchResult.firstObject else {
                assert(false, "FetchResult has no PHAssetCollection")
                completion(nil)
                return
            }
            
            if success {
                completion(album)
            } else {
                Log.error{ "PHPhotoLibrary.performChanges() resulted in an error = \(String(describing: error?.localizedDescription))" }
                completion(nil)
            }
        })
    }
    
    static func loadThumbnailFromLocalIdentifier(localIdentifier: String, completion: @escaping (UIImage?)->()) {
        let assets = PHAsset.fetchAssets(withLocalIdentifiers: [localIdentifier], options: nil)
        
        if let firstAsset = assets.firstObject  {
            loadThumbnailFromAsset(asset: firstAsset, completion: completion)
        } else {
            completion(nil)
        }
    }
    
    static func loadThumbnailFromAsset(asset: PhotoAsset, completion: @escaping (UIImage?)->()) {
        let thumbnailSize = CGSize(width: 100.0, height: 100.0)
        PHImageManager.default().requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFit, options: PHImageRequestOptions(), resultHandler: { result, info in
            completion(result)
        })
    }
    
}
