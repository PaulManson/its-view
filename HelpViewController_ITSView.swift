//
//  HelpViewController.swift
//  ITS View
//
//  Created by Paul Manson on 28/05/15.
//  Copyright (c) 2015 e.quinox KG. All rights reserved.
//
//  The home of all on-line help and support functionality.
//

import UIKit
import Eureka
import PalaceSoftwareCore


class HelpViewController: FormViewController {
    
    private let kSupportEmail = App.supportEmailAddress
    private let kTutorialsURL = "https://www.youtube.com/channel/UC6NiL4qvh-eo2WwGdb1gGwg"
    
    private let kTutorialsTitle = LM.localizedString("title_tutorials", comment: "Title for the 'Tutorials' link in the 'Support' menu.")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //
        //  Create and add the custom IU elements that weren't handled in IB.
        //
        self.navigationItem.leftBarButtonItem = SidebarButton(target: self, selector: #selector(HelpViewController.sidebarButtonPressed(_:)))
        
        let doneTitle = LM.localizedString("button_done", comment: "Standard Done button. Accepts the fields entered on a form. Equivalent to OK.")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: doneTitle, style: UIBarButtonItem.Style.plain, target: self, action: #selector(HelpViewController.btnDonePressed(_:)))
        
        //
        //  Create and add the custom IU elements that weren't handled in IB.
        //
        let titleEmailSupport      = LM.localizedString("title_email_support",            comment: "Title for the 'Email Support' link in the 'Support' menu.")
        
        //
        //  Create the "Support" form
        //
        form +++ Section("")
            
            <<< ButtonRow() {
                $0.title = kTutorialsTitle
            }.onCellSelection({ (_, _) in
                self.tutorials()
            })

            <<< ButtonRow() {
                $0.title = titleEmailSupport
            }.onCellSelection({ (_, _) in
                self.emailSupport()
            })
        
        if App.isErikOrPaul {
            //
            //  The following "Diagnostics" section should be disabled in production versions of the App. This exists solely so that customers can send their Log file to
            //  support if we're trying to debug a problem.
            //
            form +++ Section(header: "Diagnostics", footer: "During the \(App.applicationName) field-testing phase, users can send the App's diagnostic Log file to the \(App.organizationName) support team via the 'Send Log File to Support' button.")
                
                <<< ButtonRow() {
                    $0.title = "Send Log File to Support"
                    }.onCellSelection { cell, row in
                        guard let logFile = Log.logFileURL else {
                            Log.error{ "Unable to get the LogFileName!" }
                            return
                        }
                        
                        self.emailSupport(attachments: [logFile])
            }
        }
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        _ = SafeAsynchronousEventManager.checkForEvents(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func btnDonePressed(_ sender: AnyObject) {
        showMap()
    }
    
    @objc func sidebarButtonPressed(_ sender:UIBarButtonItem) {
        toggleSideMenuView()
    }
    
    private func showMap() {
        if MapViewController.singletonMapViewController == nil {
            Log.severe{ "PlacesList.showPlaceOnMap() tried to call MapViewController but it doesn't (yet) exist!" }
            return
        }
        
        let destViewController = MapViewController.singletonMapViewController!
        
        sideMenuController()?.setContentViewController(destViewController)
        sideMenuController()?.sideMenu?.toggleMenu()
        
        CustomToolbarManager.resetDefaultButtons()
    }
    
    private func tutorials() {
        openWebPage(kTutorialsTitle, webURL: kTutorialsURL)
    }
    
    private var emailer : Emailer!
    
    private func emailSupport(attachments: [URL] = []) {
        //
        //  Per documentation, we cycle the Emailer client each time we use it.
        //
        if emailer != nil {
            emailer = nil
        }
        emailer                     = Emailer(onViewController: self)
        let supportEmailAddress     = EmailAddress(string: kSupportEmail)!
        let placeholderEmailSubject = LM.localizedString("title_support_email_subject", comment: "The default subject line for an email sent to the technical support team at Places2Note.")
        let placeholderEmailBody    = LM.localizedString("text_support_email_template", comment: "Template for an email to the technical support team at Places2Note. @1 is replaced by the App's name.").withApplicationNameInjected
        emailer.sendEmail(to: [supportEmailAddress], subject: placeholderEmailSubject, body: placeholderEmailBody, attachments: attachments)
    }
    
    private func openWebPage(_ title: String, webURL: String) {
        guard AppReachability.checkNetworkReachabilityAndWarnIfNotReachable() else { return }
        
        let webDocumentToOpen = WebDocumentToOpen(prompt: title, isLocal: false, documentName: webURL, documentExtension: "")
        
        if let webDocumentViewController = Storyboard.mainStoryboard().instantiateViewController(withIdentifier: "WebDocumentViewController") as? WebDocumentViewController {
            webDocumentViewController.webDocument = webDocumentToOpen
            self.navigationController?.pushViewController(webDocumentViewController, animated: true)
        } else {
            Log.severe{ "HelpViewController().openWebPage() could not instantiate a WebDocumentViewController!" }
        }
    }
    
}

